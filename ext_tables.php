<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

global $_EXTKEY;
global $TYPO3_CONF_VARS;

$configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ftm']);

/**
 * Registers a Backend Module
 */
if(!isset($configuration) || $configuration['disableBackendModule']!='1') {
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'CodingMs.'.$_EXTKEY,
		'tools',  // Make module a submodule of 'tools'
		'ftm',    // Submodule key
		'999',    // Position
		array(
			'Overview' => 'index',
			'Settings' => 'edit,save',
			'Services' => 'index',
			'Theme' => 'newTheme,createTheme,newFile,newFileAjax,deleteFile,saveSnippet,editFile,saveFile,rewriteThemeFiles',
		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.svg',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_ftm.xml',
		)
	);
}



/***************
 * Extend configuration
 */
// Add Rootline fields for default meta-tags
$TYPO3_CONF_VARS['FE']['addRootLineFields']
	= 'abstract,keywords,description,author,author_email,backend_layout_next_level,' . $TYPO3_CONF_VARS['FE']['addRootLineFields'];
$TYPO3_CONF_VARS['FE']['addRootLineFields']
	= implode(',', array_unique(explode(',', $TYPO3_CONF_VARS['FE']['addRootLineFields'])));
//
// Additional file types
$additionalFileTypes = 'html,css,less,sass,scss,ts,setupts,constantsts,pagets,userts,typoscript,yaml,rst,';
$TYPO3_CONF_VARS['BE']['fileExtensions']['webspace']['allow']
	= implode(',', array_unique(explode(',', $additionalFileTypes . $TYPO3_CONF_VARS['BE']['fileExtensions']['webspace']['allow'])));
$TYPO3_CONF_VARS['SYS']['textfile_ext']
	= implode(',', array_unique(explode(',', $additionalFileTypes . $TYPO3_CONF_VARS['SYS']['textfile_ext'])));
//
// make static files field bigger
$TCA['sys_template']['columns']['include_static_file']['config']['size'] = 20;
$TCA['sys_template']['columns']['include_static_file']['config']['maxitems'] = 999;
//
// we need more than 20 tables to be edited by the backend users...
$TCA['be_groups']['columns']['tables_modify']['config']['maxitems'] = 99;
$TCA['be_groups']['columns']['tables_select']['config']['maxitems'] = 99;
//
// Table configuration arrays
$tables = array(
    'tx_ftm_domain_model_theme' => array(
        'csh' => 'EXT:ftm/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_theme.xlf'
    ),
    'tx_ftm_domain_model_themelink' => array(
        'csh' => 'EXT:ftm/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themelink.xlf'
    ),
    'tx_ftm_domain_model_themekeyword' => array(
        'csh' => 'EXT:ftm/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themekeyword.xlf'
    ),
    'tx_ftm_domain_model_themeextension' => array(
        'csh' => 'EXT:ftm/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themeextension.xlf'
    ),
    'tx_ftm_domain_model_themelicense' => array(
        'csh' => 'EXT:ftm/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themelicense.xlf'
    ),
    'tx_ftm_domain_model_themelicenseresource' => array(
        'csh' => 'EXT:ftm/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themelicenseresource.xlf'
    ),
    'tx_ftm_domain_model_themeauthor' => array(
        'csh' => 'EXT:openimmo/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themeauthor.xlf'
    ),
    'tx_ftm_domain_model_themescreenshot' => array(
        'csh' => 'EXT:openimmo/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themescreenshot.xlf'
    ),
    'tx_ftm_domain_model_themeconstants' => array(
        'csh' => 'EXT:openimmo/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themeconstants.xlf'
    ),
    'tx_ftm_domain_model_themeconstantscategory' => array(
        'csh' => 'EXT:openimmo/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_themeconstantscategory.xlf'
    ),
    'tx_ftm_domain_model_log' => array(
        'csh' => 'EXT:openimmo/Resources/Private/Language/locallang_csh_tx_ftm_domain_model_log.xlf'
    )
);
foreach ($tables as $table => $data) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr($table, $data['csh']);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages($table);
}
