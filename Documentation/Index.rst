..  Editor configuration
	...................................................
	* utf-8 with BOM as encoding
	* tab indent with 4 characters for code snippet.
	* optional: soft carriage return preferred.

.. Includes roles, substitutions, ...
.. include:: _IncludedDirectives.rst

========================================================
Fluid-Template-Manager |extension_version| Documentation
========================================================


Technical Details
=================

:Extension-Name: |extension_name|
:Extension-Key: |extension_key|
:Version: |extension_version|
:TYPO3-Version: |version_typo3|
:Language: |language|
:Author: |author|
:Last change: |time|


.. toctree::
	:maxdepth: 5
	:glob:

	*
	Tutorials/Index
	Services/Index
	Settings/Index



