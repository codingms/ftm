=======
RealUrl
=======

Clear RealURL-Cache
===================
Clears the following Database-Tables:

* tx_realurl_chashcache
* tx_realurl_errorlog
* tx_realurl_pathcache
* tx_realurl_urldecodecache
* tx_realurl_urlencodecache


Delete RealURL Auto-Configuration
=================================
Deletes the ``typo3conf/ext/realurl_autoconf.php``.