=======
RealUrl
=======

RealURL-Cache leeren
====================
Leert den Cache der folgenden RealURL-Tabellen in der Datenbank:

* tx_realurl_chashcache
* tx_realurl_errorlog
* tx_realurl_pathcache
* tx_realurl_urldecodecache
* tx_realurl_urlencodecache


RealURL Auto-Configuration löschen
==================================
Löscht die Datei ``typo3conf/ext/realurl_autoconf.php``.