=====================================================
Installation von Theme-Bootstrap und FTM in TYPO3 6.2
=====================================================

.. warning::
	**Hinweis:**

	Dieses Tutorial wurde in einem frisch installierten TYPO3 6.2.10 durchgeführt.



1. Theme-Bootstrap inkl. alle Abhängigkeiten installieren
=========================================================

Wechseln Sie in den *Extensions Manager* und wählen Sie die Ansicht *Get Extensions*. Hier suchen Sie nun die *theme_bootstrap*-Erweiterung und installieren diese.


.. figure:: ./001-ExtensionManager.png
	:alt: Theme-Bootstrap mit dem Extension-Manager installieren.


Mit der Installation von *theme_bootstrap* werden einige weitere Erweiterungen benötigt, diese sind:

=================== ====================================================================================================
Erweiterung         Beschreibung
=================== ====================================================================================================
static_info_tables  Wird benötigt damit der Sprachmenü-ViewHelper, der von der THEMES-Erweiterung mitgeliefert wird, automatisiert seine möglichen Menüpunkte beziehen kann.
yaml_parser         Die Konfiguration von Themes basiert auf einer YAML-Datei. Viele die schon einmal mit TYPO3-Neos oder Flow gearbeitet haben werden diese kennen. Da TYPO3-CMS diese von Haus aus leider nicht lesen kann, wird diese Erweiterung benötigt.
themes              Dies ist die THEMES-Erweiterung. Diese stellt ein Backend-Modul zum Auswählen und Konfigurieren von Themes bereit.
belayout_tsprovider Diese Erweiterung ermöglicht es Backend-Layouts auf Basis von Seiten-TypoScript zu erstellen. Dies ist vor allem dann nützlich, wenn man Backend-Layouts über ein Theme in einem TYPO3 bereitstellen möchte.
gridelements        GridElements bietet die einfache Möglichkeit strukturierende Inhaltselemente für das Back- und Frontend bereitzustellen.
themes_gridelements Liefert vordefinierte GridLayouts wie z.B. Spaltenelemente, Accoridon, Carousel oder auch Tabs.
dyncss              Diese Erweiterung ist das Basis-Modul für dynamisches CSS. Diese benötigt einen Adapter im bevorzugten CSS-Dialekt, wie z.B. LESS, SASS, o.ä..
dyncss_less         Da Theme-Bootstrap auf LESS aufsetzt wird der Dyncss-LESS Adapter benötigt.
=================== ====================================================================================================

Bestätigen Sie also bei der Installation den Hinweis zu den Dependencies.


.. figure:: ./002-ExtensionManagerDependendies.png
	:alt: Theme-Bootstrap Dependencies bestätigen.


Nach der Installation wechseln Sie in die *Manage Extensions* Ansicht und überprüfen die installierten Erweiterungen. 


.. figure:: ./003-ExtensionManagerInstalledExtensions.png
	:alt: Installierte Erweiterungen prüfen.


.. warning::
	**Warnung:**

	Leider kommt es hin und wieder vor das nicht alle Erweiterungen, die in den *dependencies* der Theme-Bootstrap Erweiterung eingetragen sind, automatisch mit installiert werden. So wurde auch in unserem Fall leider die *dyncss* und auch *dyncss_less* Erweiterung ausgelassen. Installieren Sie diese bei Bedarf einfach wie gewohnt nach.


2. Theme-Bootstrap Einrichtung
==============================

* Seite erstellen
* Root-Template darauf erstellen und statische Templates einbinden
* Ins THEMES-Modul wechseln und das Theme-Bootstrap auswählen
* Theme-Bootstrap basis Variablen (pfade anpassen)
* Backend-Layout auswählen

-> Ergebnis: Theme-Bootstrap einsatzbereit

* jQuery bereitstellen

3. Fluid-Template-Manager installieren und einrichten
=====================================================

* Ext installieren
* Extension konfigurieren/möglichkeiten

4. Eigenes Theme erstellen mit dem FTM
======================================

* Ins Modul wechseln
* Neues Theme-Erstellen-Button
* Vorgang erklären
* Erklären welche Dateien erstellt werden

5. Theme mit dem FTM konfigurieren
==================================

* Bearbeitungs-Formular erklären
* Hinweis das mit dem Speichern Dateien aktualisiert werden

6. Erste Dateien mit dem FTM erstellen
======================================

* Typ/Sub-Typ Logik erklären



Zusätzlich zu den bereits installierten Erweiterungen bnötigen Sie noch jQuery und die Bootstrap-JavaScript Bibliothek. Für die einfache Einbindung empfehlen wir die *t3jquery*-Erweiterung. Installieren Sie diese ebenfalls schon