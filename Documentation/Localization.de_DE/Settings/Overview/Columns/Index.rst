=======
Spalten
=======

Enable/Disable columns for overview list:

* Type: Main type of a file (TypoScript, Fluid, ...)
* Sub-Type: Sub type of a file
* Relative path: The relative path of a file
* Name: The filename
* File-Extension: The Extension of a file


* Size: Displays the file size