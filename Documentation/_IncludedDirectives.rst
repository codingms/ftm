﻿..  Content substitution
	...................................................
	Hint: following expression |my_substition_value| will be replaced when rendering doc.

.. |author| replace:: Thomas Deuling <typo3@coding.ms>
.. |extension_key| replace:: ftm
.. |extension_version| replace:: 2.0.0
.. |extension_name| replace:: Fluid-Template-Manager
.. |version_typo3| replace:: TYPO3 6.2.4 - 6.2.x
.. |time| date:: %m-%d-%Y %H:%M
.. |language|  replace:: english

..  Custom roles
	...................................................
	After declaring a role like this: ".. role:: custom", the document may use the new role like :custom:`interpreted text`. 
	Basically, this will wrap the content with a CSS class to be styled in a special way when document get rendered.
	More information: http://docutils.sourceforge.net/docs/ref/rst/roles.html

.. role:: code
.. role:: typoscript
.. role:: typoscript(code)
.. role:: ts(typoscript)
.. role:: php(code)


..  ANREDE IMMER: Sie/Ihnen
..  Naming: Fluid-Template-Manager (FTM)
..  http://sphinx-doc.org/contents.html
