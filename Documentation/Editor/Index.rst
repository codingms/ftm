





Editor-Shortcuts
================

* ``CTRL + H`` Search and replace
* ``CTRL + S`` Save file
* ``CTRL + SPACE`` trigger auto-completion or snippet
* ``CTRL + D`` Remove current line