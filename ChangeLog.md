## FTM Change-Log


*   [BUGFIX] Fixing frontend user and group extension.
*   [TASK] Optimizing backend module icon.

### 2018-01-13  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing parent classes of ViewHelpers.
*   [BUGFIX] Fixing ViewHelpers namespace.
*   [BUGFIX] Fixing RenderExternal ViewHelper.
*   [TASK] Optimizing PHP documentation.



## 2017-12-12  Release of version 4.1.0

### 2017-12-12  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Rewriting theme configuration files must be now triggered explicitly by an action button.
*   [BUGFIX] Fixing FrontendUser model image.
*   [BUGFIX] Fixing edit empty files.



## 2017-12-08  Release of version 4.0.0

### 2017-12-08  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Refactoring for TYPO3 8.7

### 2017-12-06  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Removing unnecessary files

### 2017-09-01  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Removing parsedown feature - please use EXT:parsedown_extra
*   [TASK] Removing usage of Extension vhs



### 2017-07-12  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing render context issue in RenderExternal-ViewHelper



## 2016-09-19  Release of version 3.0.5

### 2016-11-18  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Adding scss file support

### 2016-11-12  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing generated ext_emconf.php for generated themes

### 2016-10-26  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing mail utility issue

### 2016-10-04  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Adding a TypoScript setting for read only theme packages



## 2016-09-19  Release of version 3.0.4

### 2016-09-18  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing hook declaration
*   [TASK] Inserting authors into ext_emconf.php after updating meta yaml

## 2016-09-14  Release of version 3.0.3

### 2016-09-14  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Modifying dependencies

### 2016-06-03  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Working on file list

### 2016-04-30  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Allow SCSS as an editable file

## 2016-03-21  Release of version 3.0.0
## 2016-03-21  Release of version 3.0.2

### 2016-03-21  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Removing theme_bootstrap dependency
*   [TASK] Optimizing wording

### 2016-02-23  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] TCA/UpdateRow is now able to display documentation text

### 2016-02-05  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Service for backup the full database
*   [FEATURE] Service for clearing TER-Cache
*   [BUGFIX] Fixing edit Theme-YAML for TYPO3 7.6.x

### 2016-01-24  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Unroll save buttons

### 2015-10-26  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Adding a FileReference Model for creating file references

### 2015-10-25  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Adding a backport of the format.case viewhelper of TYPO3 7.x

### 2015-05-22  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Adding configuration for editing language files

### 2015-05-20  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Extending Editor configuration (useSoftTabs, tabSize, showInvisibles)

### 2015-04-14  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Configuring Editor for showing invisible characters
*   [TASK] Enabling *.tmpl, *.setupts, *.constantsts in extension scope

### 2015-03-26  Thomas Deuling  <typo3@coding.ms>

*   [BUGFIX] Fixing persistence all problem, by creating new Theme-Records

### 2015-03-24  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Removing tt_address components

### 2015-03-18  Thomas Deuling  <typo3@coding.ms>

*   [FEATURE] Integrating an own editor
*   [FEATURE] Providing a snippet feature for editor

### 2015-03-16  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Adding rst to editable files
*   [BUGFIX] Merging Settings for Theme-Actions
*   [BUGFIX] Rename Settings field for saving Snippets
*   [FEATURE] Fix file permission for theme files

## 2015-03-16  Release of Fluid-Template-Manager 2.0.0

### 2015-03-16  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Adding Documentation Creating-Directories

### 2015-03-09  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Refactor translations and (error) messaging
*   [TASK] Optimizing Error handling while saving a snippet
*   [TASK] Protect user settings via .htaccess
*   [TASK] Move PluginCloud key, name and password into module settings, in order to achieve user based access data

### 2015-03-08  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Refactor translations

### 2015-03-07  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Refactor translations
*   [TASK] Code cleanup
*   [BUGFIX] Refactor and bugfix Update-Check-Service
*   [FEATURE] Reset FTM-Settings service

### 2015-03-06  Thomas Deuling  <typo3@coding.ms>

*   [TASK] Refactor translations
*   [TASK] Implement E-Mail spam protection for Markdown plugin
*   [TASK] Implement a plugin for rendering and displaying Markdown files

