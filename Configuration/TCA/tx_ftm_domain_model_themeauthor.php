<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$return = array(
	'ctrl' => array(
        'title'	=> 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_author.xlf:tx_ftm_domain_model_themeauthor',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,

        'adminOnly' => 1,
        'rootLevel' => 1,
        'hideTable' => 1,

        'versioningWS' => 2,
        'versioning_followPages' => TRUE,

        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'name,email,company,company_logo,company_website,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ftm') . 'Configuration/TCA/ThemeAuthor.php',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ftm') . 'Resources/Public/Icons/tx_ftm_domain_model_themeauthor.gif'
    ),
	'interface' => array(
		'showRecordFieldList' => 'hidden, name, email, company, company_logo, company_website',
	),
	'types' => array(
		'1' => array('showitem' => 'name, email, company, company_logo, company_website'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_author.xlf:tx_ftm_domain_model_themeauthor.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_author.xlf:tx_ftm_domain_model_themeauthor.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'company' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_author.xlf:tx_ftm_domain_model_themeauthor.company',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'company_logo' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_author.xlf:tx_ftm_domain_model_themeauthor.company_logo',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'company_website' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_author.xlf:tx_ftm_domain_model_themeauthor.company_website',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'theme' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

return $return;
