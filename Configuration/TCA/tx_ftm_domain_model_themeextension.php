<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$return = array(
	'ctrl' => array(
        'title'	=> 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_extension.xlf:tx_ftm_domain_model_themeextension',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,

        'adminOnly' => 1,
        'rootLevel' => 1,
        'hideTable' => 1,

        'versioningWS' => 2,
        'versioning_followPages' => TRUE,

        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'name,version,description,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ftm') . 'Configuration/TCA/ThemeExtension.php',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ftm') . 'Resources/Public/Icons/tx_ftm_domain_model_themeextension.gif'
    ),
	'interface' => array(
		'showRecordFieldList' => 'hidden, name, version, description',
	),
	'types' => array(
		'1' => array('showitem' => 'name, version, description'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_extension.xlf:tx_ftm_domain_model_themeextension.name',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'CodingMs\Ftm\Backend\ExtensionDirectorySelector->renderField',
				'eval' => 'trim,required'
			),
		),
		'version' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_extension.xlf:tx_ftm_domain_model_themeextension.version',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_extension.xlf:tx_ftm_domain_model_themeextension.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),

		'theme' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

return $return;
