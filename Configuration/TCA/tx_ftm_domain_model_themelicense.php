<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$return = array(
	'ctrl' => array(
        'title'	=> 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_license.xlf:tx_ftm_domain_model_themelicense',
        'label' => 'license',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,

        'adminOnly' => 1,
        'rootLevel' => 1,
        'hideTable' => 1,

        'versioningWS' => 2,
        'versioning_followPages' => TRUE,

        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'license,license_uri,description,resources,',
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ftm') . 'Configuration/TCA/ThemeLicense.php',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ftm') . 'Resources/Public/Icons/tx_ftm_domain_model_themelicense.gif'
    ),
	'interface' => array(
		'showRecordFieldList' => 'hidden, license, license_uri, description, resources',
	),
	'types' => array(
		'1' => array('showitem' => 'license, license_uri, description, resources'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'license' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_license.xlf:tx_ftm_domain_model_themelicense.license',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'license_uri' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_license.xlf:tx_ftm_domain_model_themelicense.license_uri',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_license.xlf:tx_ftm_domain_model_themelicense.description',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'resources' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme_license.xlf:tx_ftm_domain_model_themelicense.resources',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themelicenseresource',
				'foreign_field' => 'themelicense',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),

		'theme' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

return $return;
