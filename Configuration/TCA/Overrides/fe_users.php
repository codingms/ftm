<?php

// Google-Maps Felder hinzufuegen
$tmp_ftm_columns = array(


	'tx_ftm_account_premium_start' => array(
		'exclude' => 1,
		'label' => 'Premium-Account Start-DateTime',
		'config' => array(
			'type' => 'input',
			'size' => 13,
			'max' => 20,
			'eval' => 'datetime',
			'checkbox' => 0,
			'default' => 0,
			'range' => array(
				'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
			),
		),
	),
	'tx_ftm_account_premium_end' => array(
		'exclude' => 1,
		'label' => 'Premium-Account End-DateTime',
		'config' => array(
			'type' => 'input',
			'size' => 13,
			'max' => 20,
			'eval' => 'datetime',
			'checkbox' => 0,
			'default' => 0,
			'range' => array(
				'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
			),
		),
	),
	'tx_ftm_account_premium_credits' => array(
		'exclude' => 1,
		'label' => 'Premium-Account Credits',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
			'default' => '0',
		),
	),

	'tx_ftm_privacy' => array(
		'exclude' => 1,
		'label' => 'Privacy',
		'config' => array(
			'type' => 'check',
		),
	),
	'tx_ftm_legal' => array(
		'exclude' => 1,
		'label' => 'Legal',
		'config' => array(
			'type' => 'check',
		),
	),
	'tx_ftm_newsletter' => array(
		'exclude' => 1,
		'label' => 'Newsletter',
		'config' => array(
			'type' => 'check',
		),
	),
	'tx_ftm_birthday' => array(
		'exclude' => 1,
		'label' => 'Geburtstag',
		'config' => array(
			'type' => 'input',
			'size' => 13,
			'max' => 20,
			'eval' => 'datetime',
			'checkbox' => 0,
			'default' => 0,
			'range' => array(
				'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
			),
		),
	),
	'tx_ftm_year_of_birth' => array(
		'exclude' => 1,
		'label' => 'Jahrgang',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
			'default' => '0',
		),
	),

	'tx_ftm_attribute1' => array(
		'exclude' => 1,
		'label' => 'Attribute1',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
		),
	),
	'tx_ftm_attribute2' => array(
		'exclude' => 1,
		'label' => 'Attribute2',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
		),
	),
	'tx_ftm_attribute3' => array(
		'exclude' => 1,
		'label' => 'Attribute3',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
		),
	),
	'tx_ftm_attribute4' => array(
		'exclude' => 1,
		'label' => 'Attribute4',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
		),
	),
	'tx_ftm_attribute5' => array(
		'exclude' => 1,
		'label' => 'Attribute5',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
		),
	),
	'tx_ftm_attribute6' => array(
		'exclude' => 1,
		'label' => 'Attribute6',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim',
		),
	),
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tmp_ftm_columns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_account_premium_start",'','after:tx_extbase_type');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_account_premium_end",'','after:tx_ftm_account_premium_start');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_account_premium_credits",'','after:tx_ftm_account_premium_end');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_privacy",'','after:tx_ftm_account_premium_credits');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_legal",'','after:tx_ftm_privacy');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_newsletter",'','after:tx_ftm_legal');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_birthday",'','after:tx_ftm_newsletter');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_year_of_birth",'','after:tx_ftm_birthday');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_attribute1",'','after:tx_ftm_year_of_birth');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_attribute2",'','after:tx_ftm_attribute1');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_attribute3",'','after:tx_ftm_attribute2');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_attribute4",'','after:tx_ftm_attribute3');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_attribute5",'','after:tx_ftm_attribute4');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("fe_users", "tx_ftm_attribute6",'','after:tx_ftm_attribute5');



/*
$TCA['fe_users']['columns'][$TCA['fe_users']['ctrl']['type']]['config']['items'][] = array(
	'FTM - FrontendUser', //'LLL:EXT:coding_ms_base/Resources/Private/Language/locallang_db.xml:fe_users.tx_extbase_type.Tx_CodingMsBase_FrontendUser',
	'CodingMs\Ftm\Domain\Model\FrontendUser'
);
$TCA['fe_groups']['columns'][$TCA['fe_groups']['ctrl']['type']]['config']['items'][] = array(
	'FTM - FrontendUser-Group', //'LLL:EXT:coding_ms_base/Resources/Private/Language/locallang_db.xml:fe_users.tx_extbase_type.Tx_CodingMsBase_FrontendUser',
	'CodingMs\Ftm\Domain\Model\FrontendUserGroup'
);


$TCA['tx_ftm_domain_model_frontenduser'] = $TCA['fe_users'];
$TCA['tx_ftm_domain_model_frontenduser']['crtl']['type'] = 'tx_extbase_type';
*/

// Add record type to select box!
$GLOBALS['TCA']['fe_users']['columns']['tx_extbase_type']['config']['items'][] = array(
	'FTM - FrontendUser',
	'CodingMs\Ftm\Domain\Model\FrontendUser'
);
