<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$return = array(
	'ctrl' => array(
        'title'	=> 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,

        'adminOnly' => 1,
        'rootLevel' => 1,
        'hideTable' => 1,

        'versioningWS' => 2,
        'versioning_followPages' => TRUE,

        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ftm') . 'Configuration/TCA/Theme.php',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('ftm') . 'Resources/Public/Icons/tx_ftm_domain_model_theme.gif'
    ),
	'interface' => array(
		'showRecordFieldList' => 'hidden, name, title, version, description, parent_theme, parent_theme_version, file_hash, documentation, demo_location, keywords, supported_extensions, licenses, authors, screenshots, constants',
	),
	'types' => array(
		'1' => array('showitem' => 'name, title, version, description, parent_theme, parent_theme_version, file_hash, documentation, demo_location, keywords,
		--div--;LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.tab_extensions, supported_extensions,
		--div--;LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.tab_licenses, licenses,
		--div--;LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.tab_authors, authors,
		--div--;LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.tab_screenshots, screenshots,
		--div--;LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.tab_constants, constants'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'readOnly' =>1,
				'eval' => 'trim,required'
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'version' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.version',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'description' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'parent_theme' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.parent_theme',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'parent_theme_version' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.parent_theme_version',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'file_hash' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.file_hash',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'readOnly' =>1,
				'eval' => 'trim'
			),
		),
		'documentation' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.documentation',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themelink',
				'foreign_field' => 'theme6',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'demo_location' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.demo_location',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themelink',
				'foreign_field' => 'theme',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'keywords' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.keywords',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themekeyword',
				'foreign_field' => 'theme',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'supported_extensions' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.supported_extensions',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themeextension',
				'foreign_field' => 'theme',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'licenses' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.licenses',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themelicense',
				'foreign_field' => 'theme',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'authors' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.authors',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themeauthor',
				'foreign_field' => 'theme',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'screenshots' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.screenshots',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themescreenshot',
				'foreign_field' => 'theme',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),

		),
		'constants' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:ftm/Resources/Private/Language/locallang_db.theme.xlf:tx_ftm_domain_model_theme.constants',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_ftm_domain_model_themeconstants',
				'minitems' => 0,
				'maxitems' => 1,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),

	),
);

return $return;
