/**
 * Ace editor
 */
define(['ace/ace', 'jquery', 'ace/ext/language_tools', 'ace/config', 'ace/snippets'], function (ace, jQuery, languageTools, config, snippets) {

	this.editor = ace.edit('editor');

	this.saveCommand = function(editor) {
		var content = editor.getSession().getValue();
		var uriSave = jQuery('#editor-form-save').attr('action');
		jQuery.post(uriSave, {
				'tx_ftm_tools_ftmftm[content]': content,
				'tx_ftm_tools_ftmftm[relPath]': jQuery('#editor-relPath').val(),
				'tx_ftm_tools_ftmftm[name]': jQuery('#editor-fileName').val(),
				'tx_ftm_tools_ftmftm[theme]': jQuery('#editor-selectedTheme').val()
			},
			function(response) {
				if(typeof(response.error) !== 'undefined') {
					alert(response.error);
					jQuery('.ace_content').css('background-color', '#660000');
					setTimeout(function() {
						jQuery('.ace_content').css('background-color', 'transparent')
					}, 300);
				}
				else {
					jQuery('.ace_content').css('background-color', '#006600');
					setTimeout(function() {
						jQuery('.ace_content').css('background-color', 'transparent')
					}, 300);
				}
			}
		);
	};

	this.initialize = function() {
		var ace = this;
		this.editor.setTheme(jQuery('#editor-editorTheme').val());
		this.editor.getSession().setMode(jQuery('#editor-scriptMode').val());
		this.editor.getSession().setValue(jQuery('#content').val());
		// Using soft tabs? (that means spaces instead of a tab)
		editor.getSession().setTabSize(jQuery('#editor-editorTabsSize').val());
		editor.getSession().setUseSoftTabs(jQuery('#editor-editorTabsUseSoftWraps').val());
		// Show invisible characters?!
		editor.setShowInvisibles(jQuery('#editor-editorShowInvisibles').val());
		// Auto completer
		editor.setOptions({
			enableBasicAutocompletion: true,
			enableSnippets: true
		});
		// Prepare snippets
		var snippetManager = snippets.snippetManager;
		var snippetFile = jQuery('#editor-snippetFile').val();
		var snippetScope = jQuery('#editor-snippetScope').val();
		config.loadModule(snippetFile, function(m) {
			if (m) {
				if(typeof(m.snippetText) !== 'undefined') {
					m.snippets = snippetManager.parseSnippetFile(m.snippetText);
				}
				else {
					m.snippets = snippetManager.parseSnippetFile('');
				}
				m.snippets.push({
					content: '${1:test}.this is custom snippet text!',
					tabTrigger: 'CustomSnippet'
				});
				snippetManager.register(m.snippets, m.scope);
			}
		});
		//
		// Add save command
		this.editor.commands.addCommand({
			name: 'save',
			bindKey: {
				win: 'Ctrl-S',
				mac: 'Command-S',
				sender: 'editor|cli'
			},
			exec: this.saveCommand
		});

		jQuery('.btn.editor-save').click(function() {
			var content = ace.editor.getSession().getValue();
			var uriSave = jQuery('#editor-form-save').attr('action');
			jQuery.post(uriSave, {
					'tx_ftm_tools_ftmftm[content]': content,
					'tx_ftm_tools_ftmftm[relPath]': jQuery('#editor-relPath').val(),
					'tx_ftm_tools_ftmftm[name]': jQuery('#editor-fileName').val(),
					'tx_ftm_tools_ftmftm[theme]': jQuery('#editor-selectedTheme').val()
				},
				function(response) {
					if(typeof(response.error) !== 'undefined') {
						alert(response.error);
						jQuery('.ace_content').css('background-color', '#660000');
						setTimeout(function() {
							jQuery('.ace_content').css('background-color', 'transparent')
						}, 300);
					}
					else {
						jQuery('.ace_content').css('background-color', '#006600');
						setTimeout(function() {
							jQuery('.ace_content').css('background-color', 'transparent')
						}, 300);
					}
				}
			);
			return false;
		});

		jQuery('.btn.editor-save-and-close').click(function() {
			var content = ace.editor.getSession().getValue();
			var uriSave = jQuery('#editor-form-save').attr('action');
			jQuery.post(uriSave, {
					'tx_ftm_tools_ftmftm[content]': content,
					'tx_ftm_tools_ftmftm[relPath]': jQuery('#editor-relPath').val(),
					'tx_ftm_tools_ftmftm[name]': jQuery('#editor-fileName').val(),
					'tx_ftm_tools_ftmftm[theme]': jQuery('#editor-selectedTheme').val()
				},
				function(response) {
					if(typeof(response.error) !== 'undefined') {
						alert(response.error);
						jQuery('.ace_content').css('background-color', '#660000');
						setTimeout(function() {
							jQuery('.ace_content').css('background-color', 'transparent');
						}, 300);
					}
					else {
						jQuery('.ace_content').css('background-color', '#006600');
						setTimeout(function() {
							jQuery('.ace_content').css('background-color', 'transparent');
							location.href = jQuery('.btn.editor-close').attr('href');
						}, 300);
					}
				}
			);
			return false;
		});

	};

	return this.initialize();

});
