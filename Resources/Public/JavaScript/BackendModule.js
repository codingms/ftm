/**
 *
 */
define(['jquery'], function (jQuery) {

	var Ftm = {};
	Ftm.ftmFormNewFileStep = 1;
	Ftm.snippetSearchDelay = (function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();

	Ftm.initialize = function() {
		Ftm.initializeFormNewFile();
	};

	Ftm.initializeFormNewFile = function() {

		if(jQuery('#ftm-form-new-file').length>0) {

			// Select a type
			jQuery('#ftm-form-new-file-type').change(function () {
				// Set a hidden field for the select
				jQuery(this).parent().append('<input type="hidden" name="' + jQuery(this).attr('name') + '" value="' + jQuery(this).val() + '">');
				// Replace the select with a label
				jQuery(this).replaceWith('<label>' + jQuery(this).val() + '</label>');
				// and send form
				//jQuery('#ftm-form-new-file').submit();
				Ftm.ftmFormNewFileSubmit();
			});

			// Select a subType
			jQuery('#ftm-form-new-file-subtype').change(function () {
				// Set a hidden field for the select
				jQuery(this).parent().append('<input type="hidden" name="' + jQuery(this).attr('name') + '" value="' + jQuery(this).val() + '">');
				// Replace the select with a label
				jQuery(this).replaceWith('<label>' + jQuery(this).val() + '</label>');
				// and send form
				//jQuery('#ftm-form-new-file').submit();
				Ftm.ftmFormNewFileSubmit();
			});

			// Select a directory
			jQuery('#ftm-form-new-file-directory').change(function () {
				// Set a hidden field for the select
				jQuery(this).parent().append('<input type="hidden" name="' + jQuery(this).attr('name') + '" value="' + jQuery(this).val() + '">');
				// Replace the select with a label
				jQuery(this).replaceWith('<label>' + jQuery(this).find('option[value="' + jQuery(this).val() + '"]').html() + '</label>');
				// and send form
				//jQuery('#ftm-form-new-file').submit();
				Ftm.ftmFormNewFileSubmit();
			});

			// Select a extension
			jQuery('#ftm-form-new-file-extension').change(function () {
				// Set a hidden field for the select
				jQuery(this).parent().append('<input type="hidden" name="' + jQuery(this).attr('name') + '" value="' + jQuery(this).val() + '" id="ftm-form-new-file-extension">');
				// Replace the select with a label
				jQuery(this).replaceWith('<label>' + jQuery(this).find('option[value="' + jQuery(this).val() + '"]').html() + '</label>');
				// and send form
				//jQuery('#ftm-form-new-file').submit();
				Ftm.ftmFormNewFileSubmit();
			});

			// Validate filename
			jQuery('#ftm-form-new-file-filename').keyup(function () {
				Ftm.ftmFormNewFileFilenameChange();
			});

			jQuery('#ftm-form-new-file-snippet-search').keyup(function () {
				Ftm.snippetSearchDelay(function () {
					Ftm.snippetSearch();
				}, 500);
			});

			/*jQuery("#ftm-form-new-file").submit(function(e) {
				if(Ftm.ftmFormNewFileStep == 5) {
					Ftm.ftmFormNewFileSubmit();
					e.preventDefault();
				}
			});*/

		}
		
	};
	
	Ftm.ftmFormNewFileSubmit = function() {

		var $form = jQuery("#ftm-form-new-file");

		jQuery.ajax({
			type: "POST",
			url: $form.attr('action'),
			data: $form.serialize(),
			dataType: 'json',
			beforeSend: function () {
				jQuery('.ftm-loading').removeClass('hidden');
			},
			success: function () {
				jQuery('.ftm-loading').addClass('hidden');
			},
			complete: function (response) {
				var responseJson = jQuery.parseJSON(response.responseText);

				//console.log('complete', response, responseJson);
				//console.log(responseJson.form);

				// Selected a type
				if (Ftm.ftmFormNewFileStep == 1) {
					Ftm.ftmFormNewFileStep = 2;

					// Active sub-type select
					var subTypSelect = jQuery('#ftm-form-new-file-subtype');
					subTypSelect.removeAttr('disabled').removeClass('readonly');

					if (typeof(responseJson.form.subTypes) != 'undefined') {
						subTypSelect.html('');
						// Set the related options and count them
						tempSize = 0;
						jQuery.each(responseJson.form.subTypes, function (key, value) {
							subTypSelect.append('<option value="' + key + '">' + value + '</option>');
							tempSize++;
						});
						// When there's only one value
						// select it directly!
						if (jQuery('#ftm-form-new-file-subtype option').length == 1) {
							subTypSelect.val(jQuery('#ftm-form-new-file-subtype option:first').val());
							subTypSelect.change();
						}
						// Set the new size
						jQuery('#ftm-form-new-file-subtype').attr('size', tempSize);
					}
				}

				// Selected a subType
				else if (Ftm.ftmFormNewFileStep == 2) {
					Ftm.ftmFormNewFileStep = 3;
					// Show the next fields
					jQuery('#ftm-form-new-file-step-2').removeClass('hidden');
					// Active directory select
					var directorySelect = jQuery('#ftm-form-new-file-directory');
					directorySelect.removeAttr('disabled').removeClass('readonly');
					if (typeof(responseJson.form.directories) != 'undefined') {
						// delete all options in subTypes select!
						jQuery.each(jQuery('#ftm-form-new-file-directory option'), function (key, value) {
							jQuery(value).remove();
						});
						// Set the related options and count them
						tempSize = 0;
						jQuery.each(responseJson.form.directories, function (key, value) {
							directorySelect.append('<option value="' + key + '">' + value + '</option>');
							tempSize++;
						});
						// When there's only one value
						// select it directly!
						if (jQuery('#ftm-form-new-file-directory option').length == 1) {
							directorySelect.val(jQuery('#ftm-form-new-file-directory option:first').val());
							directorySelect.change();
						}
						// Set the new size
						directorySelect.attr('size', tempSize);
					}
				}

				// Selected a directory
				else if (Ftm.ftmFormNewFileStep == 3) {
					Ftm.ftmFormNewFileStep = 4;
					// Show the next fields
					jQuery('#ftm-form-new-file-step-3').removeClass('hidden');
					// Active directory select
					var extensionSelect = jQuery('#ftm-form-new-file-extension');
					jQuery('#ftm-form-new-file-filename').removeAttr('disabled').removeClass('readonly');
					extensionSelect.removeAttr('disabled').removeClass('readonly');
					if (typeof(responseJson.form.extensions) != 'undefined') {
						// delete all options in subTypes select!
						extensionSelect.html('');
						// Set the related options and count them
						tempSize = 0;
						jQuery.each(responseJson.form.extensions, function (key, value) {
							extensionSelect.append('<option value="' + key + '">' + value + '</option>');
							tempSize++;
						});
						// When there's only one value
						// select it directly!
						if (jQuery('#ftm-form-new-file-extension option').length == 1) {
							extensionSelect.val(jQuery('#ftm-form-new-file-extension option:first').val());
							extensionSelect.change();
						}
						// Set the new size
						extensionSelect.attr('size', tempSize);
					}
				}

				// Selected a ...
				else if (Ftm.ftmFormNewFileStep == 4) {
					Ftm.ftmFormNewFileStep = 5;

					// Remove AJAX form
					//jQuery('#ftm-form-new-file').ajaxFormUnbind();

					if (typeof(responseJson.snippets.snippets) != 'undefined') {
						jQuery.each(responseJson.snippets.snippets.list, function (key, value) {
							var template = jQuery('#ftm-form-new-file-snippet-template').html();
							template = template.replace(/\###uid###/g, value.uid);
							template = template.replace(/\###title###/g, value.title);
							template = template.replace(/\###type###/g, value.type);
							template = template.replace(/\###subType###/g, value.subType);
							template = template.replace(/\###extension###/g, value.extension);
							template = template.replace(/\###filename###/g, value.filename);
							template = template.replace(/\###description###/g, value.description);
							template = template.replace(/\###content###/g, value.content);
							jQuery('#ftm-form-new-file-snippet-container').append(template);
						});

						// Insert filename, in case of an empty filename field
						jQuery('.ftm-form-new-file-snippet').change(function () {
							var uid = jQuery(this).val();
							var extension = jQuery('#ftm-form-new-file-extension').val();
							var filenameId = '#ftm-form-new-file-snippet-' + uid + '-filename';
							var filename = jQuery(filenameId).html().replace('.' + extension, '');
							var filenameField = jQuery('#ftm-form-new-file-filename');
							if (filenameField.val() == '') {
								filenameField.val(filename);
								Ftm.ftmFormNewFileFilenameChange();
							}
						})
					}
					else {
						jQuery('.no-snippets-available').removeClass('hidden');
					}

				}
				else if (Ftm.ftmFormNewFileStep == 5) {
					//$form.submit();
				}

				jQuery('.ftm-loading').addClass('hidden');
			},
			error: function () {
				jQuery('.ftm-loading').addClass('hidden');
			}
		});
		
	};
	
	Ftm.ftmFormNewFileFilenameChange = function() {
		var value = jQuery('#ftm-form-new-file-filename').val();
		if(value.search(/[^a-zA-Z0-9\.\-_]/)==-1 && value!='') {
			jQuery('#ftm-form-new-file-create').removeClass('hidden');
			jQuery('#ftm-form-new-file-create-and-edit').removeClass('hidden');
			jQuery('#ftm-form-new-file-create-and-back').removeClass('hidden');
		}
		else {
			jQuery('#ftm-form-new-file-create').addClass('hidden');
			jQuery('#ftm-form-new-file-create-and-edit').addClass('hidden');
			jQuery('#ftm-form-new-file-create-and-back').addClass('hidden');
		}
		// Replace action
		var form = jQuery('#ftm-form-new-file');
		form.attr('action', form.attr('action').replace('newFileAjax', 'newFile'));
	};

	/**
	 * Is called by searching for snippets while creating a new file
	 */
	Ftm.snippetSearch = function() {
		var searchWord = jQuery('#ftm-form-new-file-snippet-search').val();
		jQuery.each(jQuery('#ftm-form-new-file-snippet-container tr'), function(key, value) {
			var content = jQuery(value).find('textarea').html();
			if(typeof(content)!='undefined') {
				if (content.indexOf(searchWord) >= 0) {
					jQuery(value).removeClass('hidden');
				}
				else {
					jQuery(value).addClass('hidden');
				}
			}
		});
	};

	/**
	 * initialize function
	 * */
	return function () {
		Ftm.initialize();
		return Ftm;
	}();
});