<?php
namespace CodingMs\Ftm\Hook;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * File edit hook
 *
 * @package ftm
 * @subpackage Hook
 */
class FileEditHook {

	/**
	 * Hook-function: inject t3editor JavaScript code before the page is compiled
	 * called in file_edit.php:SC_file_edit->main
	 *
	 * @param array $parameters
	 * @param \TYPO3\CMS\Backend\Controller\File\EditFileController $pObj
	 */
	public function preOutputProcessingHook($parameters, $pObj) {
		die('preOutputProcessingHook');
	}

	/**
	 * Hook-function:
	 * called in file_edit.php:SC_file_edit->main
	 *
	 * @param array $parameters
	 * @param \TYPO3\CMS\Backend\Controller\File\EditFileController $pObj
	 */
	public function postOutputProcessingHook($parameters, $pObj) {
		// Replace the target of the back button
		// Page content contains the whole page
		if(stristr($parameters['pageContent'], 'tools_FtmFtm')) {
			// $pObj->content contains only the body content
			$pObj->content = str_replace('top.goToModule("file_list");', 'top.goToModule("tools_FtmFtm");', $pObj->content);
		}
	}

}