<?php
namespace CodingMs\Ftm\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Object\ObjectManager;
use \CodingMs\Ftm\Domain\Repository\TemplateRepository;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Kleine Tools
 *
 * @package ftm
 * @subpackage Utility
 */
class Tools {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected static $objectManager;

	/**
	 * Template Repository
	 *
	 * @var TemplateRepository
	 */
	protected static $fluidTemplateRepository;

	/**
	 * Returns all page-ids on rootline
	 *
	 * @param int $pid Page-id
	 * @return array Array of rootline page-ids
	 */
	public static function getRootlinePids($pid=0) {
		$sysPage  = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\Page\\PageRepository');
		$rootline = $sysPage->getRootLine($pid);
		$rootlinePages   = array();
		$rootlinePages[] = $pid;
		if(!empty($rootline)) {
			foreach($rootline as $tempPage) {
				$rootlinePages[] = $tempPage['pid'];
			}
		}
		return $rootlinePages;
	}

	/**
	 * Ermittelt die TYPO3-Version
	 *
	 * @return float TYPO3-Version
	 */
	public static function getTypo3Version() {

		/**
		 * Use TYPO3_version Constant
		 */

		//$compatVersion = explode('.', $GLOBALS['TYPO3_CONF_VARS']['SYS']['compat_version']);
		//return (float)($compatVersion[0].'.'.$compatVersion[1]);
		return TYPO3_version;
	}

	/**
	 * @return int
	 */
	public static function getTypo3MajorVersion() {
		$version = VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version);
		return (int)($version/1000000);
	}

	/**
	 * @return bool
	 */
	public static function isTypo3Version7() {
		$version = VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version);
		return ($version >= 7000000 && $version < 8000000);
	}

	/**
	 * @return bool
	 */
	public static function isTypo3Version6() {
		$version = VersionNumberUtility::convertVersionNumberToInteger(TYPO3_version);
		return ($version >= 6000000 && $version < 7000000);
	}

	/**
	 * Checks if a filename contains only valid characters.
	 * Filename can only contain alphabetic character, a point, score and underscore.
	 * @param string $filename
	 * @return bool
	 */
	public static function checkValidFilename($filename='') {
		if (!preg_match("/^[\pL\pN.\-_]*$/", $filename)) {
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	/**
	 * Checks if a filename contains only valid characters.
	 * Filename can only contain alphabetic character and a point.
	 * @param string $filename
	 * @return bool
	 */
	public static function checkValidFilenameWithPath($filename='') {

//        if(!preg_match("/^[\pL\pN.\/]*$/", $filename)) {
//            die('preg_match ');
//        }
//        if(strstr($filename, '..')) {
//            die('strstr ..');
//        }
//        if(substr($filename, 0, 1)=='/') {
//            die('substr 1 == /');
//        }
//        if(substr_count($filename, '/')>1) {
//            die('substr_count>1');
//        }

		if (!preg_match("/^[\pL\pN.-_\/]*$/", $filename) || strstr($filename, '..') || substr($filename, 0, 1)=='/' || substr_count($filename, '/')>1) {
			return FALSE;
		}
		else {
			return TRUE;
		}
	}


	/**
	 * Ermittelt Pfade
	 *
	 * Usage: \CodingMs\Ftm\Utility\Tools::getDirectory("Less", $fluidTemplate->getTemplateDir());
	 *
	 * @param $type string Verzeichnis-Typ
	 * @param $themeName string Name des Themes/Theme-Verzeichnis, falls noetig
	 * @param $abs boolean Absolute path TRUE/FALSE
	 * @param $extension string Path for Extension-Name?!
	 * @return string Path string
	 * @deprecated
	 */
	public static function getDirectory($type, $themeName='ftm', $abs=FALSE, $extension='') {

		$directory = '';


//        $absPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:ftm/Resources/Public/Icons/');
//        var_dump(\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_DOCUMENT_ROOT'));
//        var_dump($absPath);

		if($type=="TypoScript") {
			$directory = "typo3conf/ext/".$themeName."/Configuration/TypoScript/";
		}
		if($type=="TypoScriptLibrary") {
			$directory = "typo3conf/ext/".$themeName."/Configuration/TypoScript/Library/";
		}
		if($type=="TypoScriptConstants") {
			$directory = "typo3conf/ext/".$themeName."/Configuration/TypoScript/Constants/";
		}
		if($type=="TypoScriptPage") {
			$directory = "typo3conf/ext/".$themeName."/Configuration/PageTS/";
		}
		if($type=="TypoScriptUser") {
			$directory = "typo3conf/ext/".$themeName."/Configuration/UserTS/";
		}
		if($type=="Stylesheets") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Public/Stylesheets/";
		}
		if($type=="Icons") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Public/Icons/";
		}

		if($type=="Dyncss") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Private/Dyncss/";
		}
		if($type=="DyncssLibrary") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Private/Dyncss/Library/";
		}
		if($type=="DyncssVariables") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Private/Dyncss/Library/Variables/";
		}
		if($type=="DyncssGridElementLayouts") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Private/Dyncss/Library/GridElementLayouts/";
		}
		if($type=="DyncssContentLayouts") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Private/Dyncss/Library/ContentLayouts/";
		}
		if($type=="DyncssExtension") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Private/Extensions/".$extension."/Dyncss/";
		}

		if($type=="FluidTemplates") {
			if(trim($extension)!='') {
				$directory = "typo3conf/ext/".$themeName."/Resources/Private/Extensions/".$extension."/";
			}
			else {
				$directory = "typo3conf/ext/".$themeName."/Resources/Private/";
			}
		}

		if($type=="PrivateExtension") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Private/Extensions/";
		}
		if($type=="PublicExtension") {
			$directory = "typo3conf/ext/".$themeName."/Resources/Public/Extensions/";
		}

		// This is required by the backupService
		if($type=="Backup") {

			// ggf. noch schnell erstellen
			$relPath = "uploads/tx_ftm/backup/";
			$absPath = GeneralUtility::getFileAbsFileName($relPath);
			if(!file_exists($absPath)) {
				mkdir($absPath);
			}

			return $relPath;
		}

		// Absoluten Pfad zurückgeben!?
		if($abs) {
			$directory = GeneralUtility::getFileAbsFileName($directory);
		}

		return $directory;

	}

	/**
	 * Writes a Log dataset
	 *
	 * Usage: \CodingMs\Ftm\Utility\Tools::writeLog("some text");
	 *
	 * @param string $logString Zeile zum Loggen
	 * @return boolean
	 */
	public static function writeLog($logString) {
		$relPath = "uploads/tx_ftm/";
		$absPath = GeneralUtility::getFileAbsFileName($relPath);
		return (bool)file_put_contents($absPath."log.txt", $logString."\n", FILE_APPEND);
	}

	/**
	 * Gets a template path from configuration[view]
	 * @param array $configurationView Pass $configuration['view'] in this
	 * @param string $type
	 * @return string
     * @throws \Exception
     */
	public static function getTemplatePath($configurationView, $type='template', $file) {

		$path = '';
		$rootPath = $type . 'RootPath';
		$rootPaths = $type . 'RootPaths';

		// Are there path's' available?!
		if(isset($configurationView[$rootPaths]) && !empty($configurationView[$rootPaths])) {
			// first, sort by key
			krsort($configurationView[$rootPaths]);
			$keys = array_keys($configurationView[$rootPaths]);
			// find the first Template in array
			for($i=0 ; $i<count($keys) ; $i++) {
				// Template really exists?
				$path = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($configurationView[$rootPaths][$keys[$i]]);
				if(file_exists($path . $file)) {
					//var_dump($keys[$i], $path, 'exist');
					break;
				}
				else {
					//var_dump($keys[$i], $path, 'not exist');
				}
				//echo "<br>";
			}
		}
		else if(isset($configurationView[$rootPath])) {
			//var_dump($configuration, $type, $file);
			$path = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($configurationView[$rootPath]);
			if(file_exists($path . $file)) {
				//throw new \Exception('Tools::getTemplatePath -> path file exist');
				// path found, everythings alright!
			}
			else {
				throw new \Exception('Tools::getTemplatePath (without s) -> path file not exist');
			}
		}
		if($path=='') {
			throw new \Exception('Tools::getTemplatePath not found (' . serialize($configurationView) . ', ' . $type . ', ' . $file . ')');
		}
		return $path;
	}

	public static function convertHtmlToPlainText($html='') {

		$html = str_replace('<li>', '<li>- ', $html);
		$html = str_replace("</ul>", "</ul>\n", $html);
		$html = str_replace("</li>", "</li>\n", $html);
		$html = html_entity_decode($html);
		$html = strip_tags($html);
		return $html;
	}

	/**
	 * Converts file bytes to size string
	 * @param $value \int file size in bytes
	 */
	public static function convertFileSizeToString($value) {

		if($value==-1) {
			$returnValue = 'unknown';
		}
		else if($value < pow(1024, 1)) {
			$returnValue = $value." Byte";
		}
		elseif($value < pow(1024, 2)) {
			$returnValue = round($value/pow(1024, 1), 3);
			$returnValue = number_format($returnValue, 3, '.', '');
			$returnValue .= " KB";
		}
		elseif($value < pow(1024, 3)) {
			$returnValue = round($value/pow(1024, 2), 3);
			$returnValue = number_format($returnValue, 3, '.', '');
			$returnValue .= " MB";
		}
		else {
			$returnValue = round($value/pow(1024, 8), 3);
			$returnValue = number_format($returnValue, 3, '.', '');
			$returnValue .= " GB";
		}
		return $returnValue;
	}

	public static function getFileHeaderComment($theme='', $type='') {
		$date = date('Y-m-d - H:i:s');
		$comment = '';


		if($type=='ext_emconf.php') {
			$comment .= '########################################################################' . LF;
			$comment .= '#' . LF;
			$comment .= '# Theme : ' . $theme . LF;
			$comment .= '# Created : ' . $date . LF;
			$comment .= '#' . LF;
			$comment .= '# Auto generated by FTM' . LF;
			$comment .= '#' . LF;
			$comment .= '# www.fluid-template-manager.de' . LF;
			$comment .= '#' . LF;
			$comment .= '########################################################################' . LF;
			$comment .= LF;
		}

		return $comment;
	}
}
?>
