<?php

namespace CodingMs\Ftm\Utility;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class TsParserUtility
 *
 * Provides an API to the complex TSParser
 *
 * @package KayStrobach\Themes\Utilities
 */
class TsParserUtility extends \KayStrobach\Themes\Utilities\TsParserUtility {

	protected $tsParserSetup;

	/**
	 * @param $pageId
	 * @param int $templateUid
	 * @return bool
	 */
	public function getSetup($pid) {
		$this->initializeTsParser($pid);
		$this->tsParser->generateConfig();
		return $this->tsParser->config;
	}

}
