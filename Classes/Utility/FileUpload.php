<?php
namespace CodingMs\Ftm\Utility;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 2 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * FileUpload
 *
 * @package    ftm
 * @subpackage Utility
 */
class FileUpload {

	/**
	 * objectManager
	 *
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	protected $objectManager;

	/**
	 * Upload file
	 *
	 * @param \string $filename Files-Array ($_FILES)
	 * @param \string $targetPath Path where the file should be saved
	 * @param \string $allowedExtensions Comma separated list of allowed file extensions (* allows all file extensions)
	 * @return mixed false or file.png
	 */
	public function uploadFile($file, $targetPath='', $allowedExtensions='') {
		
		// Check file extension
		if (empty($file['name']) || !self::checkExtension($file['name'], $allowedExtensions)) {
			return FALSE;
		}
		// create new filename and upload it
		$targetPasAbsolute = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($targetPath);
		$basicFileFunctions = $this->objectManager->get('TYPO3\CMS\Core\Utility\File\BasicFileUtility');
		$newFile = $basicFileFunctions->getUniqueName($file['name'], $targetPasAbsolute);

		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($file, '$fFile in uploadFile()');
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($targetPasAbsolute, '$targetPasAbsolute in uploadFile()');
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($newFile, '$newFile in uploadFile()');
		
		if (\TYPO3\CMS\Core\Utility\GeneralUtility::upload_copy_move($file['tmp_name'], $newFile)) {
			$fileInfo = pathinfo($newFile);
			return $fileInfo['basename'];
		}
		return FALSE;
	}

	/**
	 * Check extension of given filename
	 *
	 * @param \string Filename like (upload.png)
	 * @param \string $allowedExtensions Comma separated list of allowed file extensions (* allows all file extensions)
	 * @return \bool If Extension is allowed
	 */
	public static function checkExtension($filename, $allowedExtensions) {

		// Allow all file extensions
		if($allowedExtensions=='*') {
			return TRUE;
		}
		
		// Following should be used outside of this utility
		//$allowedExtensions = $GLOBALS['TCA']['tx_wsevents_domain_model_events']['columns']['image']['config']['allowed'];
		
		$fileInfo = pathinfo($filename);
		if (!empty($fileInfo['extension']) && \TYPO3\CMS\Core\Utility\GeneralUtility::inList($allowedExtensions, strtolower($fileInfo['extension']))) {
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Read image uploadfolder from TCA
	 *
	 * @return \string path - standard "uploads/pics"
	 */
	public static function getUploadFolderFromTca() {
		// Following should be used outside of this utility
		//$path = $GLOBALS['TCA']['tx_wsevents_domain_model_events']['columns']['image']['config']['uploadfolder'];
		if (empty($path)) {
			$path = 'uploads/pics';
		}
		return $path;
	}

}