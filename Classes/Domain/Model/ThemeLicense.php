<?php
namespace CodingMs\Ftm\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ThemeLicense
 */
class ThemeLicense extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * license
	 *
	 * @var string
	 */
	protected $license = '';

	/**
	 * licenseUri
	 *
	 * @var string
	 */
	protected $licenseUri = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * resources
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLicenseResource>
	 * @cascade remove
	 */
	protected $resources = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->resources = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the license
	 *
	 * @return string $license
	 */
	public function getLicense() {
		return $this->license;
	}

	/**
	 * Sets the license
	 *
	 * @param string $license
	 * @return void
	 */
	public function setLicense($license) {
		$this->license = $license;
	}

	/**
	 * Returns the licenseUri
	 *
	 * @return string $licenseUri
	 */
	public function getLicenseUri() {
		return $this->licenseUri;
	}

	/**
	 * Sets the licenseUri
	 *
	 * @param string $licenseUri
	 * @return void
	 */
	public function setLicenseUri($licenseUri) {
		$this->licenseUri = $licenseUri;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Adds a ThemeLicenseResource
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLicenseResource $resource
	 * @return void
	 */
	public function addResource(\CodingMs\Ftm\Domain\Model\ThemeLicenseResource $resource) {
		$this->resources->attach($resource);
	}

	/**
	 * Removes a ThemeLicenseResource
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLicenseResource $resourceToRemove The ThemeLicenseResource to be removed
	 * @return void
	 */
	public function removeResource(\CodingMs\Ftm\Domain\Model\ThemeLicenseResource $resourceToRemove) {
		$this->resources->detach($resourceToRemove);
	}

	/**
	 * Returns the resources
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLicenseResource> $resources
	 */
	public function getResources() {
		return $this->resources;
	}

	/**
	 * Sets the resources
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLicenseResource> $resources
	 * @return void
	 */
	public function setResources(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $resources) {
		$this->resources = $resources;
	}

	/**
	 * Sets data from meta theme.yaml
	 * @param array $metaData
	 */
	public function setMetaData(array $metaData=array()) {
		if(isset($metaData['license'])) {
			$this->setLicense($metaData['license']);
		}
		if(isset($metaData['licenseUri'])) {
			$this->setLicenseUri($metaData['licenseUri']);
		}
		if(isset($metaData['description'])) {
			$this->setDescription($metaData['description']);
		}
		if(isset($metaData['resources']) && !empty($metaData['resources'])) {
			foreach($metaData['resources'] as $resource) {
				$themeLicenseResource = new \CodingMs\Ftm\Domain\Model\ThemeLicenseResource();
				$themeLicenseResource->setName($resource);
				$this->addResource($themeLicenseResource);
			}
		}
	}

	/**
	 * @return array
	 */
	public function getArray() {
		$array = array();
		$array['license'] = $this->getLicense();
		$array['licenseUri'] = $this->getLicenseUri();
		$array['description'] = $this->getDescription();
		$resources = $this->getResources();
		if(!empty($resources)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeLicenseResource $resource */
			foreach($resources as $resource) {
				$array['resources'][] = $resource->getName();
			}
		}
		return $array;
	}

}