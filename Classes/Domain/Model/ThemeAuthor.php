<?php
namespace CodingMs\Ftm\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ThemeAuthor
 */
class ThemeAuthor extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email = '';

	/**
	 * company
	 *
	 * @var string
	 */
	protected $company = '';

	/**
	 * companyLogo
	 *
	 * @var string
	 */
	protected $companyLogo = '';

	/**
	 * companyWebsite
	 *
	 * @var string
	 */
	protected $companyWebsite = '';

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Returns the company
	 *
	 * @return string $company
	 */
	public function getCompany() {
		return $this->company;
	}

	/**
	 * Sets the company
	 *
	 * @param string $company
	 * @return void
	 */
	public function setCompany($company) {
		$this->company = $company;
	}

	/**
	 * Returns the companyLogo
	 *
	 * @return string $companyLogo
	 */
	public function getCompanyLogo() {
		return $this->companyLogo;
	}

	/**
	 * Sets the companyLogo
	 *
	 * @param string $companyLogo
	 * @return void
	 */
	public function setCompanyLogo($companyLogo) {
		$this->companyLogo = $companyLogo;
	}

	/**
	 * Returns the companyWebsite
	 *
	 * @return string $companyWebsite
	 */
	public function getCompanyWebsite() {
		return $this->companyWebsite;
	}

	/**
	 * Sets the companyWebsite
	 *
	 * @param string $companyWebsite
	 * @return void
	 */
	public function setCompanyWebsite($companyWebsite) {
		$this->companyWebsite = $companyWebsite;
	}

	/**
	 * Sets data from meta theme.yaml
	 * @param array $metaData
	 */
	public function setMetaData(array $metaData=array()) {
		if(isset($metaData['name'])) {
			$this->setName($metaData['name']);
		}
		if(isset($metaData['email'])) {
			$this->setEmail($metaData['email']);
		}
		if(isset($metaData['company'])) {
			$this->setCompany($metaData['company']);
		}
		if(isset($metaData['companyLogo'])) {
			$this->setCompanyLogo($metaData['companyLogo']);
		}
		if(isset($metaData['companyWebsite'])) {
			$this->setCompanyWebsite($metaData['companyWebsite']);
		}
	}

	/**
	 * @return array
	 */
	public function getArray() {
		$array = array();
		$array['name'] = $this->getName();
		$array['email'] = $this->getEmail();
		$array['company'] = $this->getCompany();
		$array['companyLogo'] = $this->getCompanyLogo();
		$array['companyWebsite'] = $this->getCompanyWebsite();
		return $array;
	}
	
}