<?php
namespace CodingMs\Ftm\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package ftm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

	/**
	 * @var boolean
	 */
	protected $disable;

	/**
	 * E-Mail/Benutzername mit dem sich der Benutzer anmeldet
	 *
	 * @var string
	 */
	protected $email;

	/**
	 * Vorname
	 *
	 * @var string
	 */
	protected $firstName;

	/**
	 * Aktions-Hash
	 *
	 * @var string
	 */
	protected $actionHash;

	/**
	 * Aktions-Hash Type
	 *
	 * @var string
	 */
	protected $actionHashType;

	/**
	 * Nachname
	 *
	 * @var string
	 */
	protected $lastName;

	/**
	 * Telefonnummer
	 *
	 * @var string
	 */
	protected $telephone;

	/**
	 * Faxnummer
	 *
	 * @var string
	 */
	protected $fax;

	/**
	 * Homepage
	 *
	 * @var string
	 */
	protected $www;

	/**
	 * Zip
	 *
	 * @var string
	 */
	protected $zip;

	/**
	 * City
	 *
	 * @var string
	 */
	protected $city;

	/**
	 * Adresse
	 *
	 * @var string
	 */
	protected $address;

	/**
	 * Profilbild
	 *
	 * @var string
	 */
	protected $image;

	/**
	 * Passwort mit dem sich der Benutzer anmeldet
	 *
	 * @var string
	 */
	protected $password;

	/**
	 * Paswortwiederholung bei der Registrierung
	 *
	 * @var string
	 */
	protected $passwordRepeat;

	/**
	 * Extbase Type für die Datenbank
	 *
	 * @var string
	 */
	protected $txExtbaseType;

	/**
	 * Visibility starttime
	 *
	 * @var \DateTime
	 */
	protected $starttime;

	/**
	 * Visibility endtime
	 *
	 * @var \DateTime
	 */
	protected $endtime;

	/**
	 * Premium account start datetime
	 *
	 * @var \DateTime
	 */
	protected $accountPremiumStart;

	/**
	 * Premium account start datetime
	 *
	 * @var \DateTime
	 */
	protected $accountPremiumEnd;

	/**
	 * Premium account credits
	 *
	 * @var \integer
	 */
	protected $accountPremiumCredits;

	/**
	 * Birthday
	 *
	 * @var \DateTime
	 */
	protected $birthday;

	/**
	 * Year of birth
	 *
	 * @var \integer
	 */
	protected $yearOfBirth;

	/**
	 * Privacy accepted
	 *
	 * @var boolean
	 * @since 2.0.0
	 */
	protected $privacy;

	/**
	 * Legal accepted
	 *
	 * @var boolean
	 * @since 2.0.0
	 */
	protected $legal;

	/**
	 * Newsletter accepted
	 *
	 * @var boolean
	 * @since 2.0.0
	 */
	protected $newsletter;

	/**
	 * Zusätzliches Attribute 1
	 *
	 * @var \string
	 */
	protected $attribute1;

	/**
	 * Zusätzliches Attribute 2
	 *
	 * @var \string
	 */
	protected $attribute2;

	/**
	 * Zusätzliches Attribute 3
	 *
	 * @var \string
	 */
	protected $attribute3;

	/**
	 * Zusätzliches Attribute 4
	 *
	 * @var \string
	 */
	protected $attribute4;

	/**
	 * Zusätzliches Attribute 5
	 *
	 * @var \string
	 */
	protected $attribute5;

	/**
	 * Zusätzliches Attribute 6
	 *
	 * @var \string
	 */
	protected $attribute6;

	/**
	 * Gets the Page-Id
	 *
	 * @return int
	 */
	public function getPid() {
		return $this->pid;
	}

	/**
	 * Sets the Page-Id
	 *
	 * @param int $pid
	 * @return void
	 */
	public function setPid($pid) {
		$this->pid = $pid;
	}

	/**
	 * Gets the Unique-Id
	 *
	 * @return int
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 * Sets the Unique-Id
	 *
	 * @param int $pid
	 * @return void
	 */
	public function setUid($uid) {
		$this->uid = $uid;
	}

	/**
	 * Gets the disable-Flag
	 *
	 * @return int
	 */
	public function getDisable() {
		return $this->disable;
	}

	/**
	 * Sets the disable-Flag
	 *
	 * @param int $disable
	 * @return void
	 */
	public function setDisable($disable) {
		$this->disable = $disable;
	}

	/**
	 * Gets the Usergroup
	 *
	 * @return string
	 */
	public function getUsergroup() {
		return $this->usergroup;
	}

	/**
	 * Gets the full name
	 *
	 * @return string
	 */
	public function getFullname() {
		return $this->getName();
	}

	/**
	 * Gets the FirstName
	 *
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * Sets the FirstName
	 *
	 * @param string $firstName
	 * @return void
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * Gets the LastName
	 *
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * Sets the LastName
	 *
	 * @param string $lastName
	 * @return void
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * Gets the ActionHash
	 *
	 * @return string
	 */
	public function getActionHash() {
		return $this->actionHash;
	}

	/**
	 * Sets the ActionHash
	 *
	 * @param string $actionHash
	 * @return void
	 */
	public function setActionHash($actionHash) {
		$this->actionHash = $actionHash;
	}

	/**
	 * Gets the ActionHashType
	 *
	 * @return string
	 */
	public function getActionHashType() {
		return $this->actionHashType;
	}

	/**
	 * Sets the ActionHashType
	 *
	 * @param string $actionHashType
	 * @return void
	 */
	public function setActionHashType($actionHashType) {
		$this->actionHashType = $actionHashType;
	}

	/**
	 * Gets the Title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the Title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Gets the Zip
	 *
	 * @return string
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * Sets the Zip
	 *
	 * @param string $zip
	 * @return void
	 */
	public function setZip($zip) {
		$this->zip = $zip;
	}

	/**
	 * Gets the City
	 *
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the City
	 *
	 * @param string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Gets the Address
	 *
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * Sets the Address
	 *
	 * @param string $address
	 * @return void
	 */
	public function setAddress($address) {
		$this->address = $address;
	}

	/**
	 * Gets the Image
	 *
	 * @return string
	 */
	public function getImage() {
		$imageArray = explode(',', $this->image);
		return $imageArray;
	}

	/**
	 * Sets the Image
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $image
	 * @return void
	 */
	public function setImage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $image) {
		$this->image = $image;
	}

	/**
	 * Gets the Telephone
	 *
	 * @return string
	 */
	public function getTelephone() {
		return $this->telephone;
	}

	/**
	 * Sets the Telephone
	 *
	 * @param string $telephone
	 * @return void
	 */
	public function setTelephone($telephone) {
		$this->telephone = $telephone;
	}

	/**
	 * Gets the Fax
	 *
	 * @return string
	 */
	public function getFax() {
		return $this->fax;
	}

	/**
	 * Sets the Fax
	 *
	 * @param string $fax
	 * @return void
	 */
	public function setFax($fax) {
		$this->fax = $fax;
	}

	/**
	 * Gets the WWW
	 *
	 * @return string
	 */
	public function getWww() {
		return $this->www;
	}

	/**
	 * Gets the Password
	 *
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * Sets the Password
	 *
	 * @param string $password
	 * @return void
	 */
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	 * Gets the password repeating
	 *
	 * @return string
	 */
	public function getPasswordRepeat() {
		return $this->passwordRepeat;
	}

	/**
	 * Sets the password repeating
	 *
	 * @param string $passwordRepeat
	 * @return void
	 */
	public function setPasswordRepeat($passwordRepeat) {
		$this->passwordRepeat = $passwordRepeat;
	}

	/**
	 * Gets the extbase type
	 *
	 * @return string
	 */
	public function getTxExtbaseType() {
		return $this->txExtbaseType;
	}

	/**
	 * Sets the extbase type
	 *
	 * @param string $txExtbaseType
	 * @return void
	 */
	public function setTxExtbaseType($txExtbaseType) {
		$this->txExtbaseType = $txExtbaseType;
	}

	/**
	 * Returns TRUE if both entered passwords are equal
	 *
	 * @return boolean
	 */
	public function passwordEqual() {
		if($this->getPassword() == $this->getPasswordRepeat()) return TRUE;
		else return FALSE;
	}

	/**
	 * Returns the starttime
	 *
	 * @return \DateTime $starttime
	 */
	public function getStarttime() {
		return $this->starttime;
	}

	/**
	 * Sets the starttime
	 *
	 * @param \DateTime $starttime
	 * @return void
	 */
	public function setStarttime($starttime) {
		$this->starttime = $starttime;
	}

	/**
	 * Returns the endtime
	 *
	 * @return \DateTime $endtime
	 */
	public function getEndtime() {
		return $this->endtime;
	}

	/**
	 * Sets the endtime
	 *
	 * @param \DateTime $endtime
	 * @return void
	 */
	public function setEndtime($endtime) {
		$this->endtime = $endtime;
	}

	/**
	 * Returns the accountPremiumStart
	 *
	 * @return \DateTime $accountPremiumStart
	 */
	public function getAccountPremiumStart() {
		return $this->accountPremiumStart;
	}

	/**
	 * Sets the accountPremiumStart
	 *
	 * @param \DateTime $accountPremiumStart
	 * @return void
	 */
	public function setAccountPremiumStart($accountPremiumStart) {
		$this->accountPremiumStart = $accountPremiumStart;
	}

	/**
	 * Returns the accountPremiumEnd
	 *
	 * @return \DateTime $accountPremiumEnd
	 */
	public function getAccountPremiumEnd() {
		return $this->accountPremiumEnd;
	}

	/**
	 * Sets the accountPremiumEnd
	 *
	 * @param \DateTime $accountPremiumEnd
	 * @return void
	 */
	public function setAccountPremiumEnd($accountPremiumEnd) {
		$this->accountPremiumEnd = $accountPremiumEnd;
	}

	/**
	 * Returns the accountPremiumCredits
	 *
	 * @return integer $accountPremiumCredits
	 */
	public function getAccountPremiumCredits() {
		return $this->accountPremiumCredits;
	}

	/**
	 * Sets the accountPremiumCredits
	 *
	 * @param integer $accountPremiumCredits
	 * @return void
	 */
	public function setAccountPremiumCredits($accountPremiumCredits) {
		$this->accountPremiumCredits = $accountPremiumCredits;
	}

	/**
	 * Returns the birthday
	 *
	 * @return \DateTime $birthday
	 */
	public function getBirthday() {
		return $this->birthday;
	}

	/**
	 * Sets the birthday
	 *
	 * @param \DateTime $birthday
	 * @return void
	 */
	public function setBirthday($birthday) {
		$this->birthday = $birthday;
	}

	/**
	 * Returns the yearOfBirth
	 *
	 * @return integer $yearOfBirth
	 */
	public function getYearOfBirth() {
		return $this->yearOfBirth;
	}

	/**
	 * Sets the yearOfBirth
	 *
	 * @param integer $yearOfBirth
	 * @return void
	 */
	public function setYearOfBirth($yearOfBirth) {
		$this->yearOfBirth = $yearOfBirth;
	}

	/**
	 * Returns the privacy
	 *
	 * @return boolean $privacy
	 * @since 1.0.0
	 */
	public function getPrivacy() {
		return $this->privacy;
	}

	/**
	 * Sets the privacy
	 *
	 * @param boolean $privacy
	 * @return void
	 * @since 1.0.0
	 */
	public function setPrivacy($privacy) {
		$this->privacy = $privacy;
	}

	/**
	 * Returns the boolean state of privacy
	 *
	 * @return boolean
	 * @since 1.0.0
	 */
	public function isPrivacy() {
		return $this->getPrivacy();
	}

	/**
	 * Returns the legal
	 *
	 * @return boolean $legal
	 * @since 1.0.0
	 */
	public function getLegal() {
		return $this->legal;
	}

	/**
	 * Sets the legal
	 *
	 * @param boolean $legal
	 * @return void
	 * @since 1.0.0
	 */
	public function setLegal($legal) {
		$this->legal = $legal;
	}

	/**
	 * Returns the boolean state of legal
	 *
	 * @return boolean
	 * @since 1.0.0
	 */
	public function isLegal() {
		return $this->getLegal();
	}

	/**
	 * Returns the newsletter
	 *
	 * @return boolean $newsletter
	 * @since 1.0.0
	 */
	public function getNewsletter() {
		return $this->newsletter;
	}

	/**
	 * Sets the newsletter
	 *
	 * @param boolean $newsletter
	 * @return void
	 * @since 1.0.0
	 */
	public function setNewsletter($newsletter) {
		$this->newsletter = $newsletter;
	}

	/**
	 * Returns the boolean state of newsletter
	 *
	 * @return boolean
	 * @since 1.0.0
	 */
	public function isNewsletter() {
		return $this->getNewsletter();
	}

	/**
	 * Returns the attribute1
	 *
	 * @return \string $attribute1
	 */
	public function getAttribute1() {
		return $this->attribute1;
	}

	/**
	 * Sets the attribute1
	 *
	 * @param \string $attribute1
	 * @return void
	 */
	public function setAttribute1($attribute1) {
		$this->attribute1 = $attribute1;
	}

	/**
	 * Returns the attribute2
	 *
	 * @return \string $attribute2
	 */
	public function getAttribute2() {
		return $this->attribute2;
	}

	/**
	 * Sets the attribute2
	 *
	 * @param \string $attribute2
	 * @return void
	 */
	public function setAttribute2($attribute2) {
		$this->attribute2 = $attribute2;
	}

	/**
	 * Returns the attribute3
	 *
	 * @return \string $attribute3
	 */
	public function getAttribute3() {
		return $this->attribute3;
	}

	/**
	 * Sets the attribute3
	 *
	 * @param \string $attribute3
	 * @return void
	 */
	public function setAttribute3($attribute3) {
		$this->attribute3 = $attribute3;
	}

	/**
	 * Returns the attribute4
	 *
	 * @return \string $attribute4
	 */
	public function getAttribute4() {
		return $this->attribute4;
	}

	/**
	 * Sets the attribute4
	 *
	 * @param \string $attribute4
	 * @return void
	 */
	public function setAttribute4($attribute4) {
		$this->attribute4 = $attribute4;
	}

	/**
	 * Returns the attribute5
	 *
	 * @return \string $attribute5
	 */
	public function getAttribute5() {
		return $this->attribute5;
	}

	/**
	 * Sets the attribute5
	 *
	 * @param \string $attribute5
	 * @return void
	 */
	public function setAttribute5($attribute5) {
		$this->attribute5 = $attribute5;
	}

	/**
	 * Returns the attribute6
	 *
	 * @return \string $attribute6
	 */
	public function getAttribute6() {
		return $this->attribute6;
	}

	/**
	 * Sets the attribute6
	 *
	 * @param \string $attribute6
	 * @return void
	 */
	public function setAttribute6($attribute6) {
		$this->attribute6 = $attribute6;
	}

}
?>
