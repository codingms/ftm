<?php
namespace CodingMs\Ftm\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ThemeConstants
 */
class ThemeConstants extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * availableCategories
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeConstantsCategory>
	 * @cascade remove
	 */
	protected $availableCategories = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->availableCategories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Adds a ThemeConstantsCategory
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeConstantsCategory $availableCategory
	 * @return void
	 */
	public function addAvailableCategory(\CodingMs\Ftm\Domain\Model\ThemeConstantsCategory $availableCategory) {
		$this->availableCategories->attach($availableCategory);
	}

	/**
	 * Removes a ThemeConstantsCategory
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeConstantsCategory $availableCategoryToRemove The ThemeConstantsCategory to be removed
	 * @return void
	 */
	public function removeAvailableCategory(\CodingMs\Ftm\Domain\Model\ThemeConstantsCategory $availableCategoryToRemove) {
		$this->availableCategories->detach($availableCategoryToRemove);
	}

	/**
	 * Returns the availableCategories
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeConstantsCategory> $availableCategories
	 */
	public function getAvailableCategories() {
		return $this->availableCategories;
	}

	/**
	 * Sets the availableCategories
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeConstantsCategory> $availableCategories
	 * @return void
	 */
	public function setAvailableCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $availableCategories) {
		$this->availableCategories = $availableCategories;
	}

	/**
	 * Sets data from meta theme.yaml
	 * @param array $metaData
	 */
	public function setMetaData(array $metaData=array()) {
		if(isset($metaData['availableCategories']) && !empty($metaData['availableCategories'])) {
			foreach($metaData['availableCategories'] as $category) {
				$themeConstantsCategory = new \CodingMs\Ftm\Domain\Model\ThemeConstantsCategory();
				$themeConstantsCategory->setName($category);
				$this->addAvailableCategory($themeConstantsCategory);
			}
		}
	}

	/**
	 * @return array
	 */
	public function getArray() {
		$array = array();
		$categories = $this->getAvailableCategories();
		if(!empty($categories)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeConstantsCategory $category */
			foreach($categories as $category) {
				$array['availableCategories'][] = $category->getName();
			}
		}
		return $array;
	}

}