<?php
namespace CodingMs\Ftm\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ThemeExtension
 */
class ThemeExtension extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * version
	 *
	 * @var string
	 */
	protected $version = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the version
	 *
	 * @return string $version
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * Sets the version
	 *
	 * @param string $version
	 * @return void
	 */
	public function setVersion($version) {
		$this->version = $version;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Sets data from meta theme.yaml
	 * @param array $metaData
	 */
	public function setMetaData(array $metaData=array()) {
		if(isset($metaData['name'])) {
			$this->setName($metaData['name']);
		}
		if(isset($metaData['description'])) {
			$this->setDescription($metaData['description']);
		}
		if(isset($metaData['version'])) {
			$this->setVersion($metaData['version']);
		}
	}

	/**
	 * @return array
	 */
	public function getArray() {
		$array = array();
		// Name isn't required in theme yaml 
		//$array['name'] = $this->getName();
		$array['description'] = $this->getDescription();
		$array['version'] = $this->getVersion();
		return $array;
	}
	
}