<?php
namespace CodingMs\Ftm\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Theme
 */
class Theme extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';
	
	/**
	 * version
	 *
	 * @var string
	 */
	protected $version = '';

	/**
	 * description
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * parentTheme
	 *
	 * @var string
	 */
	protected $parentTheme = '';

	/**
	 * parentThemeVersion
	 *
	 * @var string
	 */
	protected $parentThemeVersion = '';

	/**
	 * fileHash
	 *
	 * @var string
	 */
	protected $fileHash = '';

	/**
	 * documentation
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLink>
	 * @cascade remove
	 */
	protected $documentation = NULL;

	/**
	 * demoLocation
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLink>
	 * @cascade remove
	 */
	protected $demoLocation = NULL;

	/**
	 * keywords
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeKeyword>
	 * @cascade remove
	 */
	protected $keywords = NULL;

	/**
	 * supportedExtensions
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeExtension>
	 * @cascade remove
	 */
	protected $supportedExtensions = NULL;

	/**
	 * licenses
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLicense>
	 * @cascade remove
	 */
	protected $licenses = NULL;

	/**
	 * authors
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeAuthor>
	 * @cascade remove
	 */
	protected $authors = NULL;

	/**
	 * screenshots
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeScreenshot>
	 * @cascade remove
	 */
	protected $screenshots = NULL;

	/**
	 * constants
	 *
	 * @var \CodingMs\Ftm\Domain\Model\ThemeConstants
	 */
	protected $constants = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->documentation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->demoLocation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->keywords = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->supportedExtensions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->licenses = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->authors = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->screenshots = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the version
	 *
	 * @return string $version
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * Sets the version
	 *
	 * @param string $version
	 * @return void
	 */
	public function setVersion($version) {
		$this->version = $version;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the parentTheme
	 *
	 * @return string $parentTheme
	 */
	public function getParentTheme() {
		return $this->parentTheme;
	}

	/**
	 * Sets the parentTheme
	 *
	 * @param string $parentTheme
	 * @return void
	 */
	public function setParentTheme($parentTheme) {
		$this->parentTheme = $parentTheme;
	}

	/**
	 * Returns the parentThemeVersion
	 *
	 * @return string $parentThemeVersion
	 */
	public function getParentThemeVersion() {
		return $this->parentThemeVersion;
	}

	/**
	 * Sets the parentThemeVersion
	 *
	 * @param string $parentThemeVersion
	 * @return void
	 */
	public function setParentThemeVersion($parentThemeVersion) {
		$this->parentThemeVersion = $parentThemeVersion;
	}

	/**
	 * Returns the fileHash
	 *
	 * @return string $fileHash
	 */
	public function getFileHash() {
		return $this->fileHash;
	}

	/**
	 * Sets the fileHash
	 *
	 * @param string $fileHash
	 * @return void
	 */
	public function setFileHash($fileHash) {
		$this->fileHash = $fileHash;
	}

	/**
	 * Adds a ThemeLink
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLink $documentation
	 * @return void
	 */
	public function addDocumentation(\CodingMs\Ftm\Domain\Model\ThemeLink $documentation) {
		$this->documentation->attach($documentation);
	}

	/**
	 * Removes a ThemeLink
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLink $documentationToRemove The ThemeLink to be removed
	 * @return void
	 */
	public function removeDocumentation(\CodingMs\Ftm\Domain\Model\ThemeLink $documentationToRemove) {
		$this->documentation->detach($documentationToRemove);
	}

	/**
	 * Removes all ThemeDocumentation
	 *
	 * @return void
	 */
	public function removeDocumentations() {
		if(!empty($this->documentation)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeLink $documentation */
			while($documentation = $this->documentation->current()) {
				$this->removeDocumentation($documentation);
			}
		}
	}

	/**
	 * Returns the documentation
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLink> $documentation
	 */
	public function getDocumentation() {
		return $this->documentation;
	}

	/**
	 * Sets the documentation
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLink> $documentation
	 * @return void
	 */
	public function setDocumentation(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $documentation) {
		$this->documentation = $documentation;
	}

	/**
	 * Adds a ThemeLink
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLink $demoLocation
	 * @return void
	 */
	public function addDemoLocation(\CodingMs\Ftm\Domain\Model\ThemeLink $demoLocation) {
		$this->demoLocation->attach($demoLocation);
	}

	/**
	 * Removes a ThemeLink
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLink $demoLocationToRemove The ThemeLink to be removed
	 * @return void
	 */
	public function removeDemoLocation(\CodingMs\Ftm\Domain\Model\ThemeLink $demoLocationToRemove) {
		$this->demoLocation->detach($demoLocationToRemove);
	}

	/**
	 * Removes all ThemeDemoLocation
	 *
	 * @return void
	 */
	public function removeDemoLocations() {
		if(!empty($this->demoLocation)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeLink $demoLocation */
			while($demoLocation = $this->demoLocation->current()) {
				$this->removeDemoLocation($demoLocation);
			}
		}
	}

	/**
	 * Returns the demoLocation
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLink> $demoLocation
	 */
	public function getDemoLocation() {
		return $this->demoLocation;
	}

	/**
	 * Sets the demoLocation
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLink> $demoLocation
	 * @return void
	 */
	public function setDemoLocation(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $demoLocation) {
		$this->demoLocation = $demoLocation;
	}

	/**
	 * Adds a ThemeKeyword
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeKeyword $keyword
	 * @return void
	 */
	public function addKeyword(\CodingMs\Ftm\Domain\Model\ThemeKeyword $keyword) {
		$this->keywords->attach($keyword);
	}

	/**
	 * Removes a ThemeKeyword
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeKeyword $keywordToRemove The ThemeKeyword to be removed
	 * @return void
	 */
	public function removeKeyword(\CodingMs\Ftm\Domain\Model\ThemeKeyword $keywordToRemove) {
		$this->keywords->detach($keywordToRemove);
	}

	/**
	 * Removes all ThemeKeywords
	 *
	 * @return void
	 */
	public function removeKeywords() {
		if(!empty($this->keywords)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeKeyword $keyword */
			while($keyword = $this->keywords->current()) {
				$this->removeKeyword($keyword);
			}
		}
	}

	/**
	 * Returns the keywords
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeKeyword> $keywords
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * Sets the keywords
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeKeyword> $keywords
	 * @return void
	 */
	public function setKeywords(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $keywords) {
		$this->keywords = $keywords;
	}

	/**
	 * Adds a ThemeExtension
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeExtension $supportedExtension
	 * @return void
	 */
	public function addSupportedExtension(\CodingMs\Ftm\Domain\Model\ThemeExtension $supportedExtension) {
		$this->supportedExtensions->attach($supportedExtension);
	}

	/**
	 * Removes a ThemeExtension
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeExtension $supportedExtensionToRemove The ThemeExtension to be removed
	 * @return void
	 */
	public function removeSupportedExtension(\CodingMs\Ftm\Domain\Model\ThemeExtension $supportedExtensionToRemove) {
		$this->supportedExtensions->detach($supportedExtensionToRemove);
	}

	/**
	 * Removes all ThemeLicenses
	 *
	 * @return void
	 */
	public function removeSupportedExtensions() {
		if(!empty($this->supportedExtensions)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeExtension $supportedExtension */
			while($supportedExtension = $this->supportedExtensions->current()) {
				$this->removeSupportedExtension($supportedExtension);
			}
		}
	}

	/**
	 * Returns the supportedExtensions
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeExtension> $supportedExtensions
	 */
	public function getSupportedExtensions() {
		return $this->supportedExtensions;
	}

	/**
	 * Sets the supportedExtensions
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeExtension> $supportedExtensions
	 * @return void
	 */
	public function setSupportedExtensions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $supportedExtensions) {
		$this->supportedExtensions = $supportedExtensions;
	}

	/**
	 * Adds a ThemeLicense
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLicense $license
	 * @return void
	 */
	public function addLicense(\CodingMs\Ftm\Domain\Model\ThemeLicense $license) {
		$this->licenses->attach($license);
	}

	/**
	 * Removes a ThemeLicense
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeLicense $licenseToRemove The ThemeLicense to be removed
	 * @return void
	 */
	public function removeLicense(\CodingMs\Ftm\Domain\Model\ThemeLicense $licenseToRemove) {
		$this->licenses->detach($licenseToRemove);
	}

	/**
	 * Removes all ThemeLicenses
	 *
	 * @return void
	 */
	public function removeLicenses() {
		if(!empty($this->licenses)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeLicense $license */
			while($license = $this->licenses->current()) {
				$this->removeLicense($license);
			}
		}
	}

	/**
	 * Returns the licenses
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLicense> $licenses
	 */
	public function getLicenses() {
		return $this->licenses;
	}

	/**
	 * Sets the licenses
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeLicense> $licenses
	 * @return void
	 */
	public function setLicenses(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $licenses) {
		$this->licenses = $licenses;
	}

	/**
	 * Adds a ThemeAuthor
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeAuthor $author
	 * @return void
	 */
	public function addAuthor(\CodingMs\Ftm\Domain\Model\ThemeAuthor $author) {
		$this->authors->attach($author);
	}

	/**
	 * Removes a ThemeAuthor
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeAuthor $authorToRemove The ThemeAuthor to be removed
	 * @return void
	 */
	public function removeAuthor(\CodingMs\Ftm\Domain\Model\ThemeAuthor $authorToRemove) {
		$this->authors->detach($authorToRemove);
	}

	/**
	 * Removes all ThemeAuthor
	 *
	 * @return void
	 */
	public function removeAuthors() {
		if(!empty($this->authors)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeAuthor $author */
			while($author = $this->authors->current()) {
				$this->removeAuthor($author);
			}
		}
	}

	/**
	 * Returns the authors
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeAuthor> $authors
	 */
	public function getAuthors() {
		return $this->authors;
	}

	/**
	 * Sets the authors
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeAuthor> $authors
	 * @return void
	 */
	public function setAuthors(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $authors) {
		$this->authors = $authors;
	}

	/**
	 * Adds a ThemeScreenshot
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeScreenshot $screenshot
	 * @return void
	 */
	public function addScreenshot(\CodingMs\Ftm\Domain\Model\ThemeScreenshot $screenshot) {
		$this->screenshots->attach($screenshot);
	}

	/**
	 * Removes a ThemeScreenshot
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeScreenshot $screenshotToRemove The ThemeScreenshot to be removed
	 * @return void
	 */
	public function removeScreenshot(\CodingMs\Ftm\Domain\Model\ThemeScreenshot $screenshotToRemove) {
		$this->screenshots->detach($screenshotToRemove);
	}

	/**
	 * Removes all ThemeScreenshots
	 *
	 * @return void
	 */
	public function removeScreenshots() {
		if(!empty($this->screenshots)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeScreenshot $screenshot */
			while($screenshot = $this->screenshots->current()) {
				$this->removeScreenshot($screenshot);
			}
		}
	}

	/**
	 * Returns the screenshots
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeScreenshot> $screenshots
	 */
	public function getScreenshots() {
		return $this->screenshots;
	}

	/**
	 * Sets the screenshots
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Ftm\Domain\Model\ThemeScreenshot> $screenshots
	 * @return void
	 */
	public function setScreenshots(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $screenshots) {
		$this->screenshots = $screenshots;
	}

	/**
	 * Returns the constants
	 *
	 * @return \CodingMs\Ftm\Domain\Model\ThemeConstants $constants
	 */
	public function getConstants() {
		return $this->constants;
	}

	/**
	 * Sets the constants
	 *
	 * @param \CodingMs\Ftm\Domain\Model\ThemeConstants $constants
	 * @return void
	 */
	public function setConstants(\CodingMs\Ftm\Domain\Model\ThemeConstants $constants) {
		$this->constants = $constants;
	}

	/**
	 * Sets data from meta theme.yaml
	 * @param array $metaData
	 * @return boolean Object inserted/updated
	 */
	public function setMetaData(array $metaData=array()) {

		// compare file hash
		$fileHash = md5(serialize($metaData));
		
		// If different, rewrite object data
		if($fileHash!=$this->getFileHash()) {
			
			// Theme data
			$this->setFileHash($fileHash);
			$this->setTitle('');
			if(isset($metaData['title'])) {
				$this->setTitle($metaData['title']);
			}
			$this->setVersion('');
			if(isset($metaData['version'])) {
				$this->setVersion($metaData['version']);
			}
			$this->setDescription('');
			if(isset($metaData['description'])) {
				$this->setDescription($metaData['description']);
			}
			// Parent theme and version
			$this->setParentTheme('');
			$this->setParentThemeVersion('');
			// Fix old theme property
			if(isset($metaData['parentTheme']) && is_string($metaData['parentTheme'])) {
				$temp = array();
				$temp['name'] = $metaData['parentTheme'];
				$temp['version'] = '';
				$metaData['parentTheme'] = $temp;
			}
			if(isset($metaData['parentTheme']) && is_array($metaData['parentTheme'])) {
				if(isset($metaData['parentTheme']['name'])) {
					$this->setParentTheme($metaData['parentTheme']['name']);
				}
				if(isset($metaData['parentTheme']['version'])) {
					$this->setParentThemeVersion($metaData['parentTheme']['version']);
				}
			}

			// Documentations
			$this->removeDocumentations();
			if(isset($metaData['documentation']) && !empty($metaData['documentation'])) {
				foreach($metaData['documentation'] as $documentation) {
					$themeLink = new \CodingMs\Ftm\Domain\Model\ThemeLink();
					$themeLink->setMetaData($documentation);
					$this->addDocumentation($themeLink);
				}
			}
			
			// Demo locations
			$this->removeDemoLocations();
			if(isset($metaData['demoLocation']) && !empty($metaData['demoLocation'])) {
				foreach($metaData['demoLocation'] as $demoLocation) {
					$themeLink = new \CodingMs\Ftm\Domain\Model\ThemeLink();
					$themeLink->setMetaData($demoLocation);
					$this->addDemoLocation($themeLink);
				}
			}
			
			// Keywords
			$this->removeKeywords();
			if(isset($metaData['keywords']) && !empty($metaData['keywords'])) {
				foreach($metaData['keywords'] as $keyword) {
					$themeKeyword = new \CodingMs\Ftm\Domain\Model\ThemeKeyword();
					$themeKeyword->setName($keyword);
					$this->addKeyword($themeKeyword);
				}
			}
			
			// Licenses
			$this->removeLicenses();
			if(isset($metaData['licenses']) && !empty($metaData['licenses'])) {
				foreach($metaData['licenses'] as $license) {
					$themeLicense = new \CodingMs\Ftm\Domain\Model\ThemeLicense();
					$themeLicense->setMetaData($license);
					$this->addLicense($themeLicense);
				}
			}
			
			// Screenshots
			$this->removeScreenshots();
			if(isset($metaData['screenshots']) && !empty($metaData['screenshots'])) {
				foreach($metaData['screenshots'] as $screenshot) {
					$themeScreenshot = new \CodingMs\Ftm\Domain\Model\ThemeScreenshot();
					$themeScreenshot->setMetaData($screenshot);
					$this->addScreenshot($themeScreenshot);
				}
			}
			
			// Supported extensions
			$this->removeSupportedExtensions();
			if(isset($metaData['supportedExtensions']) && !empty($metaData['supportedExtensions'])) {
				foreach($metaData['supportedExtensions'] as $extensionKey => $supportedExtension) {
					$supportedExtension['name'] = $extensionKey;
					$themeExtension = new \CodingMs\Ftm\Domain\Model\ThemeExtension();
					$themeExtension->setMetaData($supportedExtension);
					$this->addSupportedExtension($themeExtension);
				}
			}
			
			// Authors
			$this->removeAuthors();
			if(isset($metaData['authors']) && !empty($metaData['authors'])) {
				foreach($metaData['authors'] as $author) {
					$newAuthor = new \CodingMs\Ftm\Domain\Model\ThemeAuthor();
					$newAuthor->setMetaData($author);
					$this->addAuthor($newAuthor);
				}
			}

			// Constants
			$constants = $this->getConstants();
			if(!($constants instanceof \CodingMs\Ftm\Domain\Model\ThemeConstants)) {
				$constants = new \CodingMs\Ftm\Domain\Model\ThemeConstants();
			}
			if(isset($metaData['constants']) && !empty($metaData['constants'])) {
				$constants->setMetaData($metaData['constants']);
			}
			$this->setConstants($constants);
			
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

	/**
	 * Gets data for meta theme.yaml
	 * @param array $metaData
	 */
	public function getMetaData() {
		
		$metaData = array();
		$metaData['name'] = $this->getName();
		$metaData['title'] = $this->getTitle();
		$metaData['description'] = $this->getDescription();
		$metaData['parentTheme'] = array();
		$metaData['parentTheme']['name'] = $this->getParentTheme();
		$metaData['parentTheme']['version'] = $this->getParentThemeVersion();
		$metaData['version'] = $this->getVersion();
		
		// Documentation
		$documentations = $this->getDocumentation();
		if(!empty($documentations)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeLink $documentation */
			foreach($documentations as $documentation) {
				$metaData['documentation'][] = $documentation->getArray();
			}
		}

		// Demo location
		$demoLocations = $this->getDemoLocation();
		if(!empty($demoLocations)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeLink $demoLocation */
			foreach($demoLocations as $demoLocation) {
				$metaData['demoLocation'][] = $demoLocation->getArray();
			}
		}

		// Keywords
		$keywords = $this->getKeywords();
		if(!empty($keywords)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeKeyword $keyword */
			foreach($keywords as $keyword) {
				$metaData['keywords'][] = $keyword->getName();
			}
		}

		// Licenses
		$licenses = $this->getLicenses();
		if(!empty($licenses)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeLicense $license */
			foreach($licenses as $license) {
				$metaData['licenses'][] = $license->getArray();
			}
		}

		// Screenshots
		$screenshots = $this->getScreenshots();
		if(!empty($screenshots)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeAuthor $screenshot */
			foreach($screenshots as $screenshot) {
				$metaData['screenshots'][] = $screenshot->getArray();
			}
		}

		// Supported extensions
		$supportedExtensions = $this->getSupportedExtensions();
		if(!empty($supportedExtensions)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeExtension $supportedExtension */
			foreach($supportedExtensions as $supportedExtension) {
				$metaData['supportedExtensions'][$supportedExtension->getName()] = $supportedExtension->getArray();
			}
		}

		// Authors
		$authors = $this->getAuthors();
		if(!empty($authors)) {
			/** @var \CodingMs\Ftm\Domain\Model\ThemeAuthor $author */
			foreach($authors as $author) {
				$metaData['authors'][] = $author->getArray();
			}
		}
		
		// Constants
		$constants = $this->getConstants();
		if($constants instanceof \CodingMs\Ftm\Domain\Model\ThemeConstants) {
			$metaData['constants'] = $constants->getArray();
		}
		
		return $metaData;
	}
}