<?php
namespace CodingMs\Ftm\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ThemeLink
 */
class ThemeLink extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * uri
	 *
	 * @var string
	 */
	protected $uri = '';

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * Returns the uri
	 *
	 * @return string $uri
	 */
	public function getUri() {
		return $this->uri;
	}

	/**
	 * Sets the uri
	 *
	 * @param string $uri
	 * @return void
	 */
	public function setUri($uri) {
		$this->uri = $uri;
	}

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Sets data from meta theme.yaml
	 * @param array $metaData
	 */
	public function setMetaData(array $metaData=array()) {
		if(isset($metaData['uri'])) {
			$this->setUri($metaData['uri']);
		}
		if(isset($metaData['title'])) {
			$this->setTitle($metaData['title']);
		}
	}

	/**
	 * @return array
	 */
	public function getArray() {
		$array = array();
		$array['uri'] = $this->getUri();
		$array['title'] = $this->getTitle();
		return $array;
	}
}