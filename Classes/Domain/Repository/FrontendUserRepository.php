<?php
namespace CodingMs\Ftm\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package ftm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	//protected $objectType = 'Tx_Ftm_FrontendUser'; //CodingMs\Ftm\Domain\Model\FrontendUser';

	/**
	 * Ruft einen Benutzer ab, dessen Username uebereinstimmt
	 * und dessen disable-Flag ignoriert wird
	 *
	 * @param string $username Username des Benutzers
	 * @return \CodingMs\Ftm\Domain\Model\FrontendUser
	 */
	public function findOneByUsernameEnabledAndDisabled($username) {
		$query = $this->createQuery();
		// $query->getQuerySettings()->setRespectStoragePage(FALSE);
		// $query->getQuerySettings()->setRespectEnableFields(FALSE);
		$query->matching($query->equals('username', $username));
		return $query->execute()->getFirst();
	}

	/**
	 * Gets a user by username and password
	 *
	 * @param string $username Username of the user
	 * @param string $password Password of the user
	 * @return \CodingMs\Ftm\Domain\Model\FrontendUser
	 */
	public function findOneByUsernameAndPassword($username, $password) {
		$query = $this->createQuery();
		$constraints   = array();
		$constraints[] = $query->equals('username', $username);
		$constraints[] = $query->equals('password', $password);
		$query->matching(
			$query->logicalAnd($constraints)
		);
		$query->setLimit(1);
		return $query->execute()->getFirst();
	}
	
	/**
	 * Ruft einen Benutzer ab, dessen UID uebereinstimmt
	 * die page ist aber egal
	 *
	 * @param string $username Username des Benutzers
	 * @return \CodingMs\Ftm\Domain\Model\FrontendUser
	 */
	public function findOneByUidWithoutPid($uid) {

		//die($this->objectType);

		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$query->matching($query->equals('uid', $uid));
		return $query->execute()->getFirst();
	}

	/**
	 * Ermittelt alle User der uebergebenen Gruppen
	 *
	 * @param array $feGroups Frontend UserGroups
	 * @param array $settings Plugin-Settings
	 * @return \CodingMs\Ftm\Domain\Model\FrontendUser
	 */
	public function findAllByUsergroup(array $feGroups=array(), array $settings=array()) {
		$query = $this->createQuery();
		//$query->getQuerySettings()->setIgnoreEnableFields(FALSE);


		$constraints   = array();
		$constraints[] = $query->in('usergroup', $feGroups);

		// Zip range
		if(isset($settings['zip_range']) && ctype_digit($settings['zip_range'])) {
			$lowerRange = $settings['zip_range']*10000;
			$upperRange = $lowerRange+9999;
			\CodingMs\Ftm\Utility\Console::log("zip range: ".$lowerRange."-".$upperRange);
			$constraints[] = $query->greaterThanOrEqual('zip', $lowerRange);
			$constraints[] = $query->lessThanOrEqual('zip', $upperRange);
		}

		$query->matching(
			$query->logicalAnd($constraints)
		);
		return $query->execute();

	}

	/**
	 * Get frontend user by action hash
	 *
	 * @param string $actionHash Action hash of a user
	 * @return \CodingMs\Ftm\Domain\Model\FrontendUser
	 */
	public function findOneByActionHash($actionHash, $disable=0) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setIgnoreEnableFields(TRUE);
		$constraints   = array();
		$constraints[] = $query->equals('actionHash', $actionHash);
		$constraints[] = $query->equals('disable', $disable);
		$constraints[] = $query->equals('deleted', 0);
		$query->matching(
			$query->logicalAnd($constraints)
		);
		return $query->execute()->getFirst();
	}
	
}
?>