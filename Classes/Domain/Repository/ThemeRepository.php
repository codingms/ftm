<?php
namespace CodingMs\Ftm\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Themes
 */
class ThemeRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Find by name
	 *
	 * @param $title \string Name of the theme
	 * @param $pid \int Page uid
	 * @return
	 */
	public function findOneByName($name) {

		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(FALSE);

		$constraints   = array();
		$constraints[] = $query->equals('pid', 0);
		$constraints[] = $query->equals('name', $name);

		$query->matching(
			$query->logicalAnd($constraints)
		);

		return $query->execute()->getFirst();
	}
}