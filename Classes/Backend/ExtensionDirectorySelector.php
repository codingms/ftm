<?php
namespace CodingMs\Ftm\Backend;

/***************************************************************
*  Copyright notice
*
*  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

use \CodingMs\Ftm\Service\Template;
use \CodingMs\Ftm\Utility\Tools;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Select-Box for selecting a Template-Extension directory
 *
 * @package ftm
 * @subpackage backend
 */
class ExtensionDirectorySelector {

	/**
	 * Render a Flexible Content Element type selection field
	 *
	 * @param array $parameters
	 * @param mixed $parentObject
	 * @return string
	 */
	public function renderField(array &$parameters, &$parentObject) {

		// Vars
		$uid   = $parameters["row"]["uid"];
		$pid   = $parameters["row"]["pid"];
		$name  = $parameters['itemFormElName'];
		$value = $parameters['itemFormElValue'];

		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		
		// Get supported Extensions
		/** @var \CodingMs\Ftm\Service\ExtensionService $extensionService */
		$extensionService = $objectManager->get('CodingMs\\Ftm\\Service\\ExtensionService');
		$supportedExtensions = $extensionService->getInstalledExtensionsArray();

		$select = '<div><select name="' . htmlspecialchars($name) . '"  class="formField select">' . LF;
		$selectArray = array();
		if(!empty($supportedExtensions)) {
			foreach($supportedExtensions as $extensionKey => $extensionLabel) {
				$selected = ($extensionKey === $value ? ' selected="selected"' : '');
				$selectArray[$extensionKey] = '<option value="'.$extensionKey.'" '.$selected.'>'.$extensionLabel.'</option>' . LF;
			}
		}
		ksort($selectArray);
		$select .= implode("\n", $selectArray);
		$select .= '</select></div>' . LF;
		return $select;
	}

}
?>