<?php
namespace CodingMs\Ftm\ViewHelpers;

/**
 * Shows Content Element
 *
 * @package TYPO3
 * @subpackage ftm
 */
class Typo3MajorVersionViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Returns the Major Version of TYPO3
	 *
	 * @return int Major Version of TYPO3
	 */
	public function render() {
		return \CodingMs\Ftm\Utility\Tools::getTypo3MajorVersion();
	}

}