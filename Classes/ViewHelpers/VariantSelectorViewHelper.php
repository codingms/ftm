<?php
namespace CodingMs\Ftm\ViewHelpers;

/**
 * Returns a Variant-Selector
 *
 * @package TYPO3
 * @subpackage ftm
 */
class VariantSelectorViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * Parse a content element
     * @param string $variants Theme variants (comma separated)
     * @return  string      Parsed Content Element
     */
    public function render($variants='') {

        $uriBuilder = $this->controllerContext->getUriBuilder();

        $selector = '<div id="variantSelector" style="background-color: #FFFFFF; border: 1px dashed #FF0000; bottom: -1px; padding: 5px; position: fixed;left: -1px;">';
        $selector.= '<span>Variant: </span>';
        $selector.= '<select id="variantSelectorSelect">';
        $variants = explode(',', $variants);
        if(!empty($variants)) {
            foreach($variants as $variant) {
                $href = $uriBuilder->reset()
                        ->setArguments(array('variant'=>trim($variant)))
                        ->build();
                $selected = (\TYPO3\CMS\Core\Utility\GeneralUtility::_GET('variant')==trim($variant)) ? 'selected="selected"' : '';
                $selector.= '<option value="'.$href.'" '.$selected.'>'.trim($variant).'</a></option>';
            }
        }
        $selector.= '</select>';
        $selector.= '<script type="text/javascript">';
        $selector.= '  $(function(){';
        $selector.= '    $("#variantSelectorSelect").change(function() {';
        $selector.= '      window.location=this.value;';
        $selector.= '    });';
        $selector.= '  });';
        $selector.= '</script>';
        $selector.= '</div>';
        return $selector;
    }

}
?>