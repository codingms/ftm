<?php
namespace CodingMs\Ftm\ViewHelpers;

/**
 * Returns a Theme-Selector
 *
 * @package TYPO3
 * @subpackage ftm
 */
class ThemeSelectorViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @var \KayStrobach\Themes\Domain\Repository\ThemeRepository
     * @inject
     */
    protected $themeRepository;

    /**
     * Parse a content element
     *
     * @return  string      Parsed Content Element
     */
    public function render() {
        $selector = '<ul id="themesSelector">';
        $themes = $this->themeRepository->findAll();
        if(!empty($themes)) {
            /**
             * @var \KayStrobach\Themes\Domain\Model\Theme
             */
            foreach($themes as $theme) {
                $selector.= '<li>'.$theme->getExtensionName().'</li>';
            }
        }
        $selector.= '</ul>';
        return $selector;
    }

}
?>