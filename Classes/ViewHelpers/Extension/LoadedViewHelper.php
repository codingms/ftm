<?php

namespace CodingMs\Ftm\ViewHelpers\Extension;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class LoadedViewHelper extends AbstractConditionViewHelper
{

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('extensionName', 'string',
            'Name of extension that must be loaded in order to evaluate as TRUE, UpperCamelCase', true);
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overridden in extending ViewHelpers to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexibility in overriding this method.
     * @return bool
     */
    static protected function evaluateCondition($arguments = null)
    {
        $extensionName = $arguments['extensionName'];
        $extensionKey = GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName);
        $isLoaded = ExtensionManagementUtility::isLoaded($extensionKey);
        return true === $isLoaded;
    }

}
