<?php

namespace CodingMs\Ftm\ViewHelpers\Format;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Case Formatting ViewHelper
 */
class CaseViewHelper extends AbstractViewHelper
{

    const CASE_UPPER = 'upper';
    const CASE_LOWER = 'lower';
    const CASE_UCWORDS = 'ucwords';
    const CASE_UCFIRST = 'ucfirst';
    const CASE_LCFIRST = 'lcfirst';
    const CASE_CAMELCASE = 'CamelCase';
    const CASE_LOWERCAMELCASE = 'lowerCamelCase';
    const CASE_UNDERSCORED = 'lowercase_underscored';

    /**
     * @param string $string
     * @param string $mode
     * @return string
     */
    public function render($string = null, $mode = null)
    {
        if (null === $string) {
            $string = $this->renderChildren();
        }
        switch ($mode) {
            case self::CASE_LOWER:
                $string = $GLOBALS['TSFE']->csConvObj->conv_case($GLOBALS['TSFE']->renderCharset, $string, 'toLower');
                break;
            case self::CASE_UPPER:
                $string = $GLOBALS['TSFE']->csConvObj->conv_case($GLOBALS['TSFE']->renderCharset, $string, 'toUpper');
                break;
            case self::CASE_UCWORDS:
                $string = ucwords($string);
                break;
            case self::CASE_UCFIRST:
                $string = $GLOBALS['TSFE']->csConvObj->convCaseFirst($GLOBALS['TSFE']->renderCharset, $string,
                    'toUpper');
                break;
            case self::CASE_LCFIRST:
                $string = $GLOBALS['TSFE']->csConvObj->convCaseFirst($GLOBALS['TSFE']->renderCharset, $string,
                    'toLower');
                break;
            case self::CASE_CAMELCASE:
                $string = GeneralUtility::underscoredToUpperCamelCase($string);
                break;
            case self::CASE_LOWERCAMELCASE:
                $string = GeneralUtility::underscoredToLowerCamelCase($string);
                break;
            case self::CASE_UNDERSCORED:
                $string = GeneralUtility::camelCaseToLowerCaseUnderscored($string);
                break;
            default:
                break;
        }
        return $string;
    }

}
