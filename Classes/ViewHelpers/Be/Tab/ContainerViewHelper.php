<?php
namespace CodingMs\Ftm\ViewHelpers\Be\Tab;


/**
 * 
 */
class ContainerViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper {

	/**
	 * @var string
	 */
	protected $tagName = 'div';

	/**
	 * Initialize arguments
	 *
	 * @return void
	 * @api
	 */
	public function initializeArguments() {
		$this->registerUniversalTagAttributes();
	}

	/**
	 *
	 * @param string $name
	 * @param int $active
	 * @param array $tabs
	 */
	public function render($name='', $active=0, array $tabs=array()) {

		$before = '<ul class="nav nav-tabs t3js-tabs" role="tablist" id="tabs-DTM-' . $name . '" data-store-last-tab="1">';
		if(!empty($tabs)) {
			$tabCount = 1;
			foreach($tabs as $key=>$label) {

				$before .= '<li role="presentation" class="' . (($tabCount==$active) ? 'active' : '') . '">';
				$before .= '<a href="#DTM-' . $name . '-' . $tabCount . '" title="" aria-controls="DTM-' . $name . '-' . $tabCount . '" role="tab" data-toggle="tab" aria-expanded="true">';
				$before .= $label;
				//$before .= '<img name="DTM-' . $name . '-' . $tabCount . '-REQ" src="gfx/clear.gif" class="t3-TCEforms-reqTabImg" alt="">';
				$before .= '	</a>';
				$before .= '</li>';
				$tabCount++;
			}
		}
		$before .= '</ul>';
		$before .= '<div class="tab-content">';

		$after = '</div>';
		
		$this->tag->addAttribute('role', 'tabpanel');
		$this->tag->setContent($before . $this->renderChildren() . $after);
		$this->tag->forceClosingTag(TRUE);
		return $this->tag->render();
	}
}
?>
