<?php
namespace CodingMs\Ftm\ViewHelpers\Be\Tab;


/**
 * 
 */
class ContentViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper {

	/**
	 * @var string
	 */
	protected $tagName = 'div';

	/**
	 * Initialize arguments
	 *
	 * @return void
	 * @api
	 */
	public function initializeArguments() {
		$this->registerUniversalTagAttributes();
	}

	/**
	 * @param string $containerTab
	 * @param boolean $active
	 * @return string
	 */
	public function render($containerTab='', $active='') {
		$before = '<div class="panel panel-tab"><div class="panel-body">';
		$after ='</div></div>';
		$this->tag->addAttribute('id', 'DTM-' . $containerTab);
		$this->tag->addAttribute('role', 'tabpanel');
		$this->tag->addAttribute('class', 'tab-pane ' . (($active) ? 'active' : ''));
		$this->tag->setContent($before . $this->renderChildren() . $after);
		$this->tag->forceClosingTag(TRUE);
		return $this->tag->render();
	}
}
?>
