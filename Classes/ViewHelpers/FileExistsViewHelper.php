<?php
namespace CodingMs\Ftm\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Checks if a file exists
 *
 * {ftm:fileExists(file: 'fileadmin/image.jpg')}
 * <f:if condition="{ftm:fileExists(file: 'fileadmin/{picture}')}">
 * Show Picture
 * </if>
 */
class FileExistsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param $file string Filename
     */
    public function render($file) {
        if (!$file || substr($file, -1) == '/') {
            return 0;
        }
        if (file_exists(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($file)) && filesize(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($file)) > 0) {
            return 1;
        }
        return 0;
    }
}
?>
