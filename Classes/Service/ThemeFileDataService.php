<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Ftm\Utility\Tools;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Theme file data service
 *
 * @package ftm
 * @subpackage Service
 */
class ThemeFileDataService {

	/**
	 * @var array Theme file data
	 */
	protected $themeFileData = array();

	/**
	 * @var string Absolute theme path
	 */
	protected $absPath = '';

	/**
	 * @var string Theme name
	 */
	protected $theme = '';

	/**
	 * @var string Theme name
	 */
	protected $themeReadOnly = TRUE;

	/**
	 * @var string Theme scope
	 */
	protected $themeScope = '';

	/**
	 * @var array Filter
	 */
	protected $filter = array();

	/**
	 * Gets the file data of a theme
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param \string $theme
	 * @param \string $themeScope
	 * @param \array $settings
	 * @return array Array with theme file data
	 * @since 2.0.0
	 */
	public function getThemeFileData($theme, $themeScope, array $settings=array()) {
		$this->theme = $theme;
		if(!in_array($theme, $settings['readonlyThemes'])) {
			$this->themeReadOnly = FALSE;
		}
		$this->themeScope = $themeScope;
		$this->filter = $settings['overview']['filter'];
		$this->absPath = GeneralUtility::getFileAbsFileName('typo3conf/ext/' . $this->theme);
		if(file_exists($this->absPath)) {
			$this->iterateDir($this->absPath);
		}
		ksort($this->themeFileData);
		return $this->themeFileData;
	}

	/**
	 * Iterate
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param \string $path
	 * @return void
	 * @since 2.0.0
	 */
	protected function iterateDir($path) {
		/** @var \DirectoryIterator $file */
		foreach(new \DirectoryIterator($path) as $file) {
			if(!$file->isDot()) {
				if($file->isDir()) {
					$this->iterateDir($file->getPathname());
				}
				else {
					$this->extractFileData($file);
				}
			}
		}
	}

	/**
	 * Extracts file data
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param \DirectoryIterator $file
	 * @return void
	 * @since 2.0.0
	 */
	protected function extractFileData(\DirectoryIterator $file) {
		$relPath = str_replace($this->absPath, '', $file->getPath());
		$relPathFilename = $relPath . '/' . $file->getFilename();
		// First index is empty!!
		$relPathParts = explode('/', $relPath);
		$fileData = array();
        //
		// General
		$fileData['scope'] = 'Theme';
		$fileData['relPath'] = substr($relPath, 1, strlen($relPath)-1) . '/';
		$fileData['absPath'] = $file->getPath();
		$fileData['name'] = $file->getFilename();
		$fileData['extension'] = $file->getExtension();
        //
		// Permissions
		$fileData['isWritable'] = $this->themeReadOnly ? FALSE : $file->isWritable();
		$fileData['isReadable'] = $file->isReadable();
		if($fileData['isReadable']) {
			$fileData['permissionsString'] = '<span style="color: green">Read</span> / ';
		}
		else {
			$fileData['permissionsString'] = '<span style="color: red">Read</span> / ';
		}
		if($fileData['isWritable']) {
			$fileData['permissionsString'] .= '<span style="color: green">Write</span>';
		}
		else {
			$fileData['permissionsString'] .= '<span style="color: red">Write</span>';
		}
		//
		// Size
		$fileData['size'] = -1;
		if($fileData['isReadable']) {
			$fileData['size'] = $file->getSize();
		}
		$fileData['sizeString'] = Tools::convertFileSizeToString($fileData['size']);
        //
		// Extension data
		$fileData['isExtension'] = FALSE;
		$fileData['extensionKey'] = '';
		if($relPathParts[3] === 'Extensions') {
			$fileData['isExtension'] = TRUE;
			$fileData['extensionKey'] = GeneralUtility::underscoredToUpperCamelCase($relPathParts[4]);
			$fileData['scope'] = $fileData['extensionKey'];
		}
		//
		// Check the scope
		// If scope:theme and file from scope:extension
		if($this->themeScope=='Theme' && $fileData['isExtension']) {
			return;
		}
		// If scope:extension and file from scope:theme
		if($this->themeScope!='Theme' && !$fileData['isExtension']) {
			return;
		}
		// If scope:extension but file from a different extension
		$themeScopeUpperCamelCase =  GeneralUtility::underscoredToUpperCamelCase($this->themeScope);
		if($fileData['isExtension'] && $themeScopeUpperCamelCase!= $fileData['extensionKey']) {
			return;
		}
        //
		// No hidden folder/files, processed folder, contributions etc.
		if($relPathParts[1]=='_processed_' || substr($relPathParts[1], 0, 1)=='.' || substr($fileData['name'], 0, 1)=='.' || $relPathParts[3]=='Contrib') {
			return;
		}



		$fileData['fullFilePath'] = 'typo3conf/ext/theme_test_ftm' . $relPathFilename;


		//BackendUtility::getModuleUrl('tools_FtmFtm');
		//$returnUrl = '&returnUrl=' . rawurlencode(BackendUtility::getModuleUrl('tools_FtmFtm', array(), FALSE, TRUE));
		//$fileData['editFileLink'] = 'file_edit.php?target=typo3conf/ext/' . $this->theme . $relPathFilename . $returnUrl;



		$imageTypes = array('gif', 'jpg', 'jpeg', 'png', 'ico', 'svg');
		$mediaTypes = array('swf', 'flv', 'otf', 'ttf', 'woff', 'woff2', 'eot');
		$fontTypes = array('otf', 'ttf', 'woff', 'woff2', 'eot');
		$flashTypes = array('swf', 'flv');
		$themeTypes = array('txt', 'php', 'yaml', 'sql');
		$languageTypes = array('xml', 'xlf');


		$fileData['isEditable'] = FALSE;



		$fileData = $this->extractFileDataBasic($fileData);
		$fileData = $this->extractFileDataDyncss($fileData);
		$fileData = $this->extractFileDataImages($fileData, $imageTypes) ;
		$fileData = $this->extractFileDataTemplates($fileData, $relPathParts);
		$fileData = $this->extractFileDataTypoScript($fileData);
		$fileData = $this->extractFileDataJavaScript($fileData);
		$fileData = $this->extractFileDataStylesheets($fileData);
		$fileData = $this->extractFileDataDocumentation($fileData);

		// Flexforms
		if($fileData['extension']=='xml' && $relPathParts[1] === 'Configuration') {
			$fileData['type'] = 'Flexforms';
			if($relPathParts[2] === 'Elements') {
				$fileData['subType'] = 'Elements/Flexforms';
			}
			else {
				$fileData['subType'] = '';
			}
		}

		else if(in_array($fileData['extension'], $languageTypes)) {
			//echo $relPathParts[3]."<br>";
			if($relPathParts[3] === 'Language' || $relPathParts[5] === 'Language') {
				$fileData['type'] = 'Language';
				$fileData['isEditable'] = TRUE;
				$fileData['subType'] = $fileData['extension']=='xml' ? 'XML' : 'XLIFF';
				$fileData['icon'] = 't3-icon t3-icon-flags t3-icon-flags-multiple t3-icon-multiple';
			}
		}

		else if(in_array($fileData['extension'], $mediaTypes)) {
			//echo $relPathParts[3]."<br>";
			//if($relPathParts[3] === 'Language' || $relPathParts[5] === 'Language') {
				$fileData['type'] = 'Media';
				$fileData['isEditable'] = TRUE;

				if(in_array($fileData['extension'], $fontTypes)) {
					$fileData['subType'] = 'Font';
				}
				if(in_array($fileData['extension'], $flashTypes)) {
					$fileData['subType'] = 'Flash';
				}

				$fileData['subType'] = $fileData['extension']=='xml' ? 'XML' : 'XLIFF';
				$fileData['icon'] = 't3-icon t3-icon-flags t3-icon-flags-multiple t3-icon-multiple';
			//}
		}

		// Files that are not writable, are not editable!
		if($fileData['isEditable']) {
			$fileData['isEditable'] = $fileData['isWritable'];
		}

		// Filter entries
		if(isset($this->filter[$fileData['type']])) {
			if($this->filter[$fileData['type']]==1) {
				$this->themeFileData[$relPathFilename] = $fileData;
			}
		}
		else {

			// Don't show contribution files
			if($relPathParts[3] === 'Contrib') {

			}
			else {
				$fileData['type'] = 'X->' . $fileData['type'];
				$this->themeFileData[$relPathFilename] = $fileData;
			}

		}

	}


	protected function extractFileDataBasic($fileData) {
		// Theme scope
		if($this->themeScope=='Theme') {
			if($fileData['extension']=='php') {
				$fileData['type'] = 'Theme';
				$fileData['subType'] = 'Theme';
				$fileData['icon'] = 't3-icon t3-icon-mimetypes t3-icon-mimetypes-text t3-icon-text-php';
			}
			else if($fileData['name']=='theme.yaml') {
				$fileData['type'] = 'Theme';
				$fileData['subType'] = 'Meta';
				$fileData['icon'] = 't3-icon t3-icon-mimetypes t3-icon-mimetypes-other t3-icon-other-other';
			}
			else if($fileData['name']=='ext_icon.png' || $fileData['name']=='ext_icon.png') {
				$fileData['type'] = 'Theme';
				$fileData['subType'] = 'Image';
				$fileData['icon'] = 't3-icon t3-icon-mimetypes t3-icon-mimetypes-other t3-icon-other-other';
			}
		}
		return $fileData;
	}
	protected function extractFileDataImages($fileData, $imageTypes) {
		// Theme scope
		if($this->themeScope=='Theme') {
			if(in_array($fileData['extension'], $imageTypes)) {
				$fileData['type'] = 'Image';
				$fileData['subType'] = $fileData['relPath'];
				$fileData['icon'] = 't3-icon t3-icon-mimetypes t3-icon-mimetypes-media t3-icon-media-image';
			}
		}
		return $fileData;
	}
	protected function extractFileDataDocumentation($fileData) {
		// Theme scope
		if($this->themeScope=='Theme') {
			if($fileData['extension']=='rst') {
				$fileData['type'] = 'Documentation';
				$fileData['subType'] = 'ReStructuredText';
				$fileData['icon'] = 'mimetypes-text-text';
				$fileData['isEditable'] = TRUE;
			}
			else if($fileData['extension']=='md') {
				$fileData['type'] = 'Documentation';
				$fileData['subType'] = 'MarkDown';
				$fileData['icon'] = 'mimetypes-text-text';
				$fileData['isEditable'] = TRUE;
			}
            else if($fileData['extension']=='txt') {
                /**
                 * @todo and filename!= setup, constants, tsconfig
                 */
                $fileData['type'] = 'Documentation';
                $fileData['subType'] = 'Text';
                $fileData['icon'] = 'mimetypes-text-text';
                $fileData['isEditable'] = TRUE;
            }
		}
		return $fileData;
	}

    /**
     * Extract HTML/Fluid/Template files
     * @param $fileData
     * @param $relPathParts
     * @return mixed
     */
	protected function extractFileDataTemplates($fileData, $relPathParts) {
		// Theme scope
		if($this->themeScope=='Theme') {
			if($fileData['extension']=='html') {
				$fileData['type'] = 'Fluid';
				$fileData['icon'] = 'mimetypes-text-html';
				$fileData['isEditable'] = TRUE;
				if($relPathParts[3] === 'Layouts') {
					$fileData['subType'] = $this->getSubType('Layouts', $relPathParts);
				}
				else if($relPathParts[3] === 'Partials') {
					$fileData['subType'] = $this->getSubType('Partials', $relPathParts);
				}
				else if($relPathParts[3] === 'Templates') {
					$fileData['subType'] = $this->getSubType('Templates', $relPathParts);
				}
				else if($relPathParts[5] === 'Widget') {
					$fileData['subType'] = $this->getSubType('Widget', $relPathParts);
				}
			}
		}
		// File is an Extension file
		else if($this->themeScope!='Theme') {
			// Fluid Templates
			if($fileData['extension']=='html') {
				$fileData['type'] = 'Fluid';
				$fileData['icon'] = 'mimetypes-text-html';
				$fileData['isEditable'] = TRUE;
				if($relPathParts[5] === 'Layouts') {
					$fileData['subType'] = $this->getSubType('Layouts', $relPathParts);
				}
				else if($relPathParts[5] === 'Partials') {
					$fileData['subType'] = $this->getSubType('Partials', $relPathParts);
				}
				else if($relPathParts[5] === 'Templates') {
					$fileData['subType'] = $this->getSubType('Templates', $relPathParts);
				}
				else if($relPathParts[7] === 'Widget') {
					$fileData['subType'] = $this->getSubType('Widget', $relPathParts);
				}
			}
			// Traditional-Template
			else if($fileData['extension']=='tmpl') {
				$fileData['type'] = 'Fluid';
				$fileData['icon'] = 'mimetypes-text-html';
				$fileData['isEditable'] = TRUE;
				$fileData['subType'] = 'Templates';
			}
		}
		return $fileData;
	}

    /**
     * Extract JavaScript files
     * @param $fileData
     * @return mixed
     */
	protected function extractFileDataJavaScript($fileData) {
		// Theme scope
		if($this->themeScope=='Theme') {
			if($fileData['extension']=='js') {
				$fileData['type'] = 'JavaScript';
				$fileData['subType'] = strtoupper($fileData['extension']);
				$fileData['icon'] = 'mimetypes-text-js';
				$fileData['isEditable'] = TRUE;
			}
		}
		// File is an Extension file
		else if($this->themeScope!='Theme') {
		}
		return $fileData;
	}

    /**
     * Extract TypoScript files
     * @param $fileData
     * @return mixed
     */
	protected function extractFileDataTypoScript($fileData) {
		// Theme scope
		if($this->themeScope=='Theme') {
			// Page TypoScript
			if($fileData['extension']=='typoscript') {

                /**
                 * @todo identify sub type
                 */

				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = '???';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
			else if($fileData['extension']=='pagets') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Page';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
			else if($fileData['name']=='tsconfig.txt') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Page';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = FALSE;
			}
			// Setup TypoScript
			else if($fileData['extension']=='setupts') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Setup';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
			else if($fileData['name']=='setup.txt') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Setup';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = FALSE;
			}
			// Constants TypoScript
			else if($fileData['extension']=='constantsts') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Constants';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
			else if($fileData['name']=='constants.txt') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Constants';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = FALSE;
			}
			// User TypoScript
			else if($fileData['extension']=='userts') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'User';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
		}
		// File is an Extension file
		else if($this->themeScope!='Theme') {
			// Page TypoScript
			if($fileData['name']=='tsconfig.txt') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Page';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
			// Setup TypoScript
			else if($fileData['name']=='setup.txt' || $fileData['extension']=='setupts') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Setup';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
			// Constants TypoScript
			else if($fileData['name']=='constants.txt' || $fileData['extension']=='constantsts') {
				$fileData['type'] = 'TypoScript';
				$fileData['subType'] = 'Constants';
				$fileData['icon'] = 'mimetypes-text-typoscript';
				$fileData['isEditable'] = TRUE;
			}
		}
		return $fileData;
	}

    /**
     * Extract dynamic CSS files
     * @param $fileData
     * @return mixed
     */
	protected function extractFileDataDyncss($fileData) {
		// Theme scope
		if($this->themeScope=='Theme') {
			if($fileData['extension']=='less' || $fileData['extension']=='sass' || $fileData['extension']=='scss') {
				$fileData['type'] = 'Dyncss';
				$fileData['subType'] = ucfirst($fileData['extension']);
				$fileData['icon'] = 'mimetypes-text-css';
				$fileData['isEditable'] = TRUE;
			}
		}
		// File is an Extension file
		else if($this->themeScope!=='Theme') {
			if($fileData['extension']=='less' || $fileData['extension']=='sass' || $fileData['extension']=='scss') {
				$fileData['type'] = 'Dyncss';
				$fileData['subType'] = ucfirst($fileData['extension']);
				$fileData['icon'] = 'mimetypes-text-css';
				$fileData['isEditable'] = TRUE;
			}
		}
		return $fileData;
	}

    /**
     * Extract CSS files
     * @param $fileData
     * @return mixed
     */
	protected function extractFileDataStylesheets($fileData) {
		// Theme scope
		if($this->themeScope==='Theme') {
			if ($fileData['extension'] == 'css') {
				$fileData['type'] = 'Stylesheets';
				$fileData['subType'] = strtoupper($fileData['extension']);
				$fileData['icon'] = 'mimetypes-text-css';
				$fileData['isEditable'] = TRUE;
			}
		}
		// File is an Extension file
		else if($this->themeScope!=='Theme') {
			if($fileData['extension']=='css') {
				$fileData['type'] = 'Stylesheets';
				$fileData['subType'] = ucfirst($fileData['extension']);
				$fileData['icon'] = 'mimetypes-text-css';
				$fileData['isEditable'] = TRUE;
			}
		}
		return $fileData;
	}

    /**
     * @param $startFrom
     * @param $relPathParts
     * @return string
     */
	protected function getSubType($startFrom, $relPathParts) {
		$start = FALSE;
		$subTypeParts = array();
		foreach($relPathParts as $part) {
			if($part == $startFrom) {
				$start = TRUE;
			}
			if($start) {
				$subTypeParts[] = $part;
			}
		}
		return implode('/', $subTypeParts);
	}

}
