<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Checks the extension configuration
 *
 * @package TYPO3
 * @subpackage ftm
 * 
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.0.0
 */
class ExtensionConfigurationService {

	/**
	 * Checks the extension configuration and returns it
	 *
	 * @param array Array mit der Extension-Configuration
	 * @return array
	 */
	public static function validate(array $extConf=array()) {
		
		if(!isset($extConf['pluginCloudHost']) || trim($extConf['pluginCloudHost'])=='') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.extension_configuration_plugin_cloud_host_failed', 'Ftm');
			throw new \Exception($translation, 1);
		}

		if(!isset($extConf['allowLog']) || trim($extConf['allowLog'])=='') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.extension_configuration_allow_log_failed', 'Ftm');
			throw new \Exception($translation, 1);
		}

		return $extConf;
	}

	public static function getConfiguration() {
		return self::validate(unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ftm']));
	}
	
}

?>