<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \CodingMs\Ftm\Service\BackupService;
use CodingMs\Ftm\Utility\TsParserUtility;

/**
 * Services for documentation generation
 *
 * @package ftm
 * @subpackage Service
 * 
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 2.1.0
 */
class DocumentationService {

	/**
	 * @var TsParserUtility
	 */
	protected $tsParser = NULL;


	/**
	 * Clear realurl cache
	 *
	 * @param $theme string Theme-Key
	 * @return boolean
	 */
	static public function generateDocumentation($theme='') {

		$docRoot = GeneralUtility::getFileAbsFileName('EXT:' . $theme . '/Documentation');
		if(!file_exists($docRoot)) {
			mkdir($docRoot);
		}
		
		$pid = (int)GeneralUtility::_GET('id');
		// extension configuration
		$extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['themes']);
		$extensionConfiguration['categoriesToShow'] = GeneralUtility::trimExplode(',', $extensionConfiguration['categoriesToShow']);
		$extensionConfiguration['constantsToHide'] = GeneralUtility::trimExplode(',', $extensionConfiguration['constantsToHide']);
		
		$tsParserWrapper = new TsParserUtility();

		$pid = 42;
		
		$categories = $tsParserWrapper->getCategories($pid);
		$subcategories = $tsParserWrapper->getSubCategories($pid);
		$constants = $tsParserWrapper->getConstants($pid);
		foreach ($categories as $categoryName => $category) {
			echo $categoryName . "<br>";
		}
		
		echo "<pre>";

		//var_dump($tsParserWrapper->getSetup($pid));
		var_dump($constants);
		
		exit;
		return TRUE;
	}

	/**
	 * Delete realurl autoconf
	 *
	 * @return boolean
	 */
	static public function deleteAutoConfiguration() {
		$success = FALSE;
		$absPath = GeneralUtility::getFileAbsFileName('typo3conf/realurl_autoconf.php');
		if(BackupService::backupFile($absPath)) {
			$success = unlink($absPath);
		}
		return $success;
	}
	
}

?>