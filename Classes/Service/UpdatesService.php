<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Perform an Update-Check
 *
 * @package ftm
 * @subpackage Service
 * 
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.1.0
 */
class UpdatesService {

	/**
	 * Check for Updates
	 *
	 * @param string $extension Extension-Key
	 * @param array $thisVersionParts Version number parts
	 * @return boolean|string
	 */
	public static function check($extension='ftm', $thisVersionParts) {

		$updateMessage = FALSE;
		
		// Wenn cache leer ist, ist ggf die aktuelle Version nicht verfügbar
		if(trim($thisVersionParts[0])=='' && trim($thisVersionParts[1])=='' && trim($thisVersionParts[2])=='') {
			return $updateMessage;
		}

		/**
		 * @todo additinal use of curl!!
		 */
		
		// Aktuelle Versions-Info ermitteln
		$context = array('http' => array('header' => 'Referer: http://'.$_SERVER['HTTP_HOST']));
		$xcontext = stream_context_create($context);
		$currentVersionJson  = @file_get_contents('http://fluid-template-manager.de/updates/'.$extension.'.json', FALSE, $xcontext);
		if(!$currentVersionJson) {
			return LocalizationUtility::translate("tx_ftm_message.error_version_not_detected", "Ftm");
		}
		$currentVersionArray = json_decode($currentVersionJson, TRUE);
		$currentVersionParts = explode('.', $currentVersionArray['version']);
		
		// Major-Release größer
		// Installierte Version>Update-Check-Version
		if($thisVersionParts[0]>$currentVersionParts[0]) {
			// Eigene Version aktueller als im Service
			// eigentlich nicht möglich
		}
		// Major-Release kleiner
		// Installierte Version<Update-Check-Version
		else if($thisVersionParts[0]<$currentVersionParts[0]) {
			$updateMessage = LocalizationUtility::translate("tx_ftm_message.info_major_release_available", "Ftm") . "<br />";
		}
		// Ansonsten kann nur ein Minor-Release oder Patch vorhanden sein 
		else {

			// Minor-Release größer
			// Installierte Version>Update-Check-Version
			if($thisVersionParts[1]>$currentVersionParts[1]) {
				// Eigene Version aktueller als im Service
				// eigentlich nicht möglich
			}
			// Minor-Release kleiner
			else if($thisVersionParts[1]<$currentVersionParts[1]) {
				$updateMessage = LocalizationUtility::translate("tx_ftm_message.info_minor_release_available", "Ftm") . "<br />";
			}
			// Ansonsten kann nur ein Patch vorhanden sein
			else {
				
				// Patch kleiner
				if($thisVersionParts[2]<$currentVersionParts[2]) {
					$updateMessage = LocalizationUtility::translate("tx_ftm_message.info_patch_available", "Ftm") . "<br />";
				}
			
			}
		}
		
		if(trim($updateMessage)!='') {
			$updateMessage .= "<br />";
			$updateMessage .= LocalizationUtility::translate("tx_ftm_message.warning_update_message", "Ftm");
			$thisVersionString = $thisVersionParts[0] . '.' .$thisVersionParts[1] . '.' .$thisVersionParts[2];
			$updateMessage .= '(' . $thisVersionString . ' -> ' .$currentVersionArray['version'] . ')';
		}
		
		return $updateMessage;
	}
	
}

?>