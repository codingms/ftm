<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Theme file data service
 *
 * @package ftm
 * @subpackage Service
 */
class SettingsService {

	/**
	 * Gets the settings
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @return array Array with settings
	 * @since 2.0.0
	 */
	public function getSettings() {
		$settings = array();
		$beUserUid = $GLOBALS['BE_USER']->user['uid'];
		$absFile = GeneralUtility::getFileAbsFileName('uploads/tx_ftm/settings.' . $beUserUid . '.serialized');
		if(file_exists($absFile)) {
			$serialized = file_get_contents($absFile);
			$settings = unserialize($serialized);
			if(!is_array($settings)) {
				$settings = array();
			}
		}
		return $settings;
	}

	/**
	 * Save the settings
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param array $settings
	 * @return \int
	 * @since 2.0.0
	 */
	public function setSettings($settings) {
		$beUserUid = $GLOBALS['BE_USER']->user['uid'];
		$absFile = GeneralUtility::getFileAbsFileName('uploads/tx_ftm/settings.' . $beUserUid . '.serialized');
		$serialized = serialize($settings);
		$absFtmPath = GeneralUtility::getFileAbsFileName('uploads/tx_ftm/');
		// check/create tx_ftm-dir
		if(!file_exists($absFtmPath)) {
			mkdir($absFtmPath);
		}
		// Be sure the file is secure!
		file_put_contents($absFtmPath . '.htaccess', 'deny from all');
		return file_put_contents($absFile, $serialized);
	}

	/**
	 * Reset the settings
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param array $settings
	 * @return \int
	 * @since 2.0.0
	 */
	public static function resetSettings() {
		$beUserUid = $GLOBALS['BE_USER']->user['uid'];
		$absFile = GeneralUtility::getFileAbsFileName('uploads/tx_ftm/settings.' . $beUserUid . '.serialized');
		return unlink($absFile);
	}

	/**
	 * Counts the activated columns
	 * @param $settings
	 * @return int
	 */
	public function getOverviewColumnCount($settings) {
		$count = 0;
		if(!empty($settings['overview']['columns'])) {
			foreach($settings['overview']['columns'] as $column=>$value) {
				if(trim($value)=='1') {
					$count++;
				}
			}
		}
		return $count;
	}
}