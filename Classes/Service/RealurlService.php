<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \CodingMs\Ftm\Service\BackupService;

/**
 * Services for realurl
 *
 * @package ftm
 * @subpackage Service
 * 
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.1.0
 */
class RealurlService {

	/**
	 * Clear realurl cache
	 *
	 * @return boolean
	 */
	static public function clearCache() {
		$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_realurl_chashcache', '1=1');
		//$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_realurl_errorlog', '1=1');
		$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_realurl_pathcache', '1=1');
		//$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_realurl_redirects', '1=1');
		//$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_realurl_uniqalias', '1=1');
		$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_realurl_urldecodecache', '1=1');
		$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_realurl_urlencodecache', '1=1');
		return TRUE;
	}

	/**
	 * Delete realurl autoconf
	 *
	 * @return boolean
	 */
	static public function deleteAutoConfiguration() {
		$success = FALSE;
		$absPath = GeneralUtility::getFileAbsFileName('typo3conf/realurl_autoconf.php');
		if(BackupService::backupFile($absPath)) {
			$success = unlink($absPath);
		}
		return $success;
	}
	
}

?>