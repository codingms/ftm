<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \CodingMs\Ftm\Utility\Tools;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Extension-Services
 *
 * @package ftm
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 2.0.0
 */
class ExtensionService {

	/**
	 * @var \TYPO3\CMS\Extensionmanager\Utility\ListUtility
	 * @inject
	 */
	protected $listUtility;

	/**
	 * Returns an array with all installed extensions
	 *
	 * @return array
	 */
	public function getInstalledExtensionsArray($type='Local') {

		$installedExtensions = array();
		$availableExtensions = $this->listUtility->getAvailableExtensions();
		$availableAndInstalledExtensions = $this->listUtility->getAvailableAndInstalledExtensions($availableExtensions);
		$availableAndInstalledExtensions = $this->listUtility->enrichExtensionsWithEmConfAndTerInformation($availableAndInstalledExtensions);

		/**
		 * @todo auch system erweiterungen listen!
		 * ggf. mit blacklist unsinnige erweiterungen ausblenden
		 */

		if(!empty($availableAndInstalledExtensions)) {
			foreach($availableAndInstalledExtensions as $extension) {
				//if($extension['type']==$type) {
					$extensionLabel = GeneralUtility::underscoredToUpperCamelCase($extension['key']);
					$installedExtensions[$extension['key']] = $extensionLabel . ' (version ' . $extension['version'] . ')';
				//}
			}
		}

		return $installedExtensions;
	}

	/**
	 * Delete all cached extensions from TER
	 * @return bool|\mysqli_result|object
	 */
	static public function deleteRepositoryCache() {
		/** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
		$db = $GLOBALS['TYPO3_DB'];
		return $db->exec_DELETEquery('tx_extensionmanager_domain_model_extension', '1=1');
	}

	/**
	 * Returns an array with all supported extension keys.
	 *
	 * @param string $extension Extension-Key
	 * @return boolean
	 */
	public function createExtensionDirectories($extensionKey='') {

		$success = TRUE;

		$relPath = 'typo3conf/ext/' . $extensionKey;
		$absPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($relPath);

		$directories = array();
		$directories[] = 'Configuration';
		$directories[] = 'Configuration/BackendLayouts';
		$directories[] = 'Configuration/Elements';
		$directories[] = 'Configuration/Elements/FlexForms';
		$directories[] = 'Configuration/Elements/PageTS';
		$directories[] = 'Configuration/Elements/TypoScript';
		$directories[] = 'Configuration/PageTS';
		$directories[] = 'Configuration/PageTS/Library';
		$directories[] = 'Configuration/TypoScript';
		$directories[] = 'Configuration/TypoScript/Library';
		$directories[] = 'Configuration/UserTS';
		//$directories[] = 'Documentation';
		//$directories[] = 'Initialisation';
		//$directories[] = 'Initialisation/Files';
		//$directories[] = 'Initialisation/Extensions';
		$directories[] = 'Meta';
		$directories[] = 'Meta/Screenshots';
		$directories[] = 'Resources';
		$directories[] = 'Resources/Private';
		$directories[] = 'Resources/Private/Dyncss';
		$directories[] = 'Resources/Private/Dyncss/Library';
		$directories[] = 'Resources/Private/Dyncss/Library/Elements';
		$directories[] = 'Resources/Private/Dyncss/Library/Variants';
		$directories[] = 'Resources/Private/Extensions';
		$directories[] = 'Resources/Private/Language';
		$directories[] = 'Resources/Private/Layouts';
		$directories[] = 'Resources/Private/Partials';
		$directories[] = 'Resources/Private/Partials/Area';
		$directories[] = 'Resources/Private/Partials/Content';
		$directories[] = 'Resources/Private/Partials/Elements';
		$directories[] = 'Resources/Private/Templates';
		$directories[] = 'Resources/Private/Templates/Theme';
		$directories[] = 'Resources/Private/Templates/ViewHelpers';
		$directories[] = 'Resources/Public';
		$directories[] = 'Resources/Public/Contrib';
		$directories[] = 'Resources/Public/Extensions';
		$directories[] = 'Resources/Public/Fonts';
		$directories[] = 'Resources/Public/Icons';
		$directories[] = 'Resources/Public/Images';
		$directories[] = 'Resources/Public/JavaScript';
		$directories[] = 'Resources/Public/Stylesheets';


		foreach($directories as $directory) {
			if(!mkdir($absPath . '/' . $directory)) {
				$translation = LocalizationUtility::translate("tx_ftm_exception.directory_creation_failed", "Ftm", array($directory));
				throw new \Exception($translation);
			}
		}

		return $success;
	}

	/**
	 * Checks if an extension key is valid
	 *
	 * @param $extensionKey
	 * @return bool
	 */
	public function isValidExtensionKey($extensionKey) {

		$valid = TRUE;

		/**
		 * Please make sure that the extension key you register follows the following rules:
		 *
		 * Allowed characters are: a-z (lowercase), 0-9 and '_' (underscore)
		 * The key must not being with one of the following prefixes: tx,user_,pages,tt_,sys_,ts_language_,csh_
		 * Extension keys cannot start or end with 0-9 and '_' (underscore)
		 * An extension key must have minimum 3, maximum 30 characters (not counting underscores)
		 */

		if(!preg_match('/^[0-9a-z_]+$/', $extensionKey)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_invalid_characters', 'Ftm');
			throw new \Exception($translation);
		}
		if(strlen($extensionKey)<3) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_too_short', 'Ftm');
			throw new \Exception($translation);
		}
		if(strlen(str_replace('_', '', $extensionKey))>30) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_too_long', 'Ftm');
			throw new \Exception($translation);
		}
		if(!ctype_alpha($extensionKey[0])) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_a_character', 'Ftm');
			throw new \Exception($translation);
		}
		if(substr($extensionKey, 0, 2) == 'tx') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_tx', 'Ftm');
			throw new \Exception($translation);
		}
		if(substr($extensionKey, 0, 5) == 'user_') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_user', 'Ftm');
			throw new \Exception($translation);
		}
		if(substr($extensionKey, 0, 5) == 'pages') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_pages', 'Ftm');
			throw new \Exception($translation);
		}
		if(substr($extensionKey, 0, 3) == 'tt_') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_tt', 'Ftm');
			throw new \Exception($translation);
		}
		if(substr($extensionKey, 0, 4) == 'sys_') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_sys', 'Ftm');
			throw new \Exception($translation);
		}
		if(substr($extensionKey, 0, 12) == 'ts_language_') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_ts_language', 'Ftm');
			throw new \Exception($translation);
		}
		if(substr($extensionKey, 0, 4) == 'csh_') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_extension_key_fails_start_with_csh', 'Ftm');
			throw new \Exception($translation);
		}

		return $valid;
	}

	public function createEmConf($extensionKey, array $metaData=array(), array $settings=array()) {

		// But don't work on theme base packages
		if(in_array($extensionKey, $settings['readonlyThemes'])) {
			return 0;
		}

		$relPath = 'typo3conf/ext/' . $extensionKey;
		$absPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($relPath);

		$emConf = '<?php' . LF;
		$emConf .= \CodingMs\Ftm\Utility\Tools::getFileHeaderComment($extensionKey, 'ext_emconf.php');

		/**
		 * Documentation:
		 * http://docs.typo3.org/typo3cms/CoreApiReference/ExtensionArchitecture/DeclarationFile/Index.html
		 */

		$emConfArray = array();
		$emConfArray['title'] = $metaData['title'];
		//$emConfArray['description'] = $metaData['description'];
		$emConfArray['category'] = 'templates';
		$emConfArray['shy'] = 0;

		// Version of the extension.
		// Automatically managed by EM / TER. Format is [int].[int].[int]
		$emConfArray['version'] = $metaData['version'];

		$emConfArray['constraints']['depends'] = array();
		$emConfArray['constraints']['conflicts'] = array();
		$emConfArray['constraints']['suggests'] = array();


		$emConf .= '$EM_CONF[$_EXTKEY] = array(' . LF;
		$emConf .= '	\'title\'            => \'' . $metaData['title'] . '\',' . LF;
		$emConf .= '	\'description\'      => \'' . $metaData['description'] . '\',' . LF;
		$emConf .= '	\'category\'         => \'templates\',' . LF;
		$emConf .= '	\'shy\'              => 0,' . LF;
		$emConf .= '	\'version\'          => \'' . $metaData['version'] . '\',' . LF;

		$emConf .= '	\'dependencies\'     => \'\',' . LF;
		$emConf .= '	\'conflicts\'        => \'\',' . LF;
		$emConf .= '	\'priority\'         => \'bottom\',' . LF;
		$emConf .= '	\'loadOrder\'        => \'\',' . LF;
		$emConf .= '	\'module\'           => \'\',' . LF;
		$emConf .= '	\'state\'            => \'stable\',' . LF;
		$emConf .= '	\'uploadfolder\'     => 0,' . LF;
		$emConf .= '	\'createDirs\'       => \'\',' . LF;
		$emConf .= '	\'modify_tables\'    => \'\',' . LF;
		$emConf .= '	\'clearcacheonload\' => 0,' . LF;
		$emConf .= '	\'lockType\'         => \'\',' . LF;
		// Authors
		$authors = array();
		$authorEmails = array();
		$authorCompanies = array();
		if(isset($metaData['authors']) && count($metaData['authors'])>0) {
			foreach($metaData['authors'] as $author) {
				$authors[] = $author['name'];
				$authorEmails[] = $author['email'];
				$authorCompanies[] = $author['company'];
			}
		}
		$emConf .= '	\'author\'           => \'' . implode(', ', $authors) . '\',' . LF;
		$emConf .= '	\'author_email\'     => \'' . implode(', ', $authorEmails) . '\',' . LF;
		$emConf .= '	\'author_company\'   => \'' . implode(', ', $authorCompanies) . '\',' . LF;
		// Coding guidelines
		$emConf .= '	\'CGLcompliance\'      => \'\',' . LF;
		$emConf .= '	\'CGLcompliance_note\' => \'\',' . LF;


		$depends = array();
		// $typo3Version = \CodingMs\Ftm\Utility\Tools::getTypo3MajorVersion();
		//$depends['typo3'] = $typo3Version . '.0.0-' . $typo3Version . '.99.99';
		$depends['themes'] = '8.7.0-8.7.99';
		// Set dependency on patent theme
		if(!empty($metaData['parentTheme']) && $metaData['parentTheme']['name'] != 'none') {
			$depends[$metaData['parentTheme']['name']] = $metaData['parentTheme']['version'];
		}
		$emConf .= '	\'constraints\' => array(' . LF;
		$emConf .= '		\'depends\' => array(' . LF;
		if(!empty($depends)) {
			foreach($depends as $key=>$version) {
				$emConf .= '			\'' . $key . '\' => \'' . $version . '\',' . LF;
			}
		}
		$emConf .= '		),' . LF;
		$emConf .= '		\'conflicts\' => array(' . LF;
		$emConf .= '		),' . LF;
		$emConf .= '		\'suggests\' => array(' . LF;
		$emConf .= '		),' . LF;
		$emConf .= '	),' . LF;

		$emConf .= '	\'_md5_values_when_last_written\' => \'\'' . LF;
		$emConf .= ');' . LF;
		$emConf .= LF;
		$emConf .= '?>' . LF;

		// write ext_emconf.php
		return file_put_contents($absPath . '/ext_emconf.php', $emConf);
	}

}

?>
