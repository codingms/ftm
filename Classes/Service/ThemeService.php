<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\FormProtection\Exception;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Theme file data service
 *
 * @package ftm
 * @subpackage Service
 */
class ThemeService {

	/**
	 * @var \KayStrobach\Themes\Domain\Repository\ThemeRepository
	 * @inject
	 */
	protected $themeRepository;

	/**
	 * @var \CodingMs\Ftm\Domain\Repository\ThemeRepository
	 * @inject
	 */
	protected $themeYamlRepository;

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	public $objectManager;

	/**
	 * Persistence-manager
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;

	/**
	 * @var \CodingMs\Ftm\Service\ExtensionService
	 * @inject
	 */
	protected $extensionService;


	/**
	 * @var array Available themes
	 */
	protected $availableThemes = array();

	/**
	 * Gets the file data of a theme
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param \string $path
	 * @return array Array with theme file data
	 * @since 2.0.0
	 */
	public function getAvailableThemes() {
		$this->availableThemes = $this->themeRepository->findAll();
		return $this->availableThemes;
	}

	/**
	 * Checks if a theme is valid
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param \string $path
	 * @return \boolean
	 * @since 2.0.0
	 */
	public function isValidTheme($theme) {
		$isValidTheme = FALSE;
		if(empty($this->availableThemes)) {
			$this->getAvailableThemes();
		}
		foreach($this->availableThemes as $availableTheme) {
			/** @var \KayStrobach\Themes\Domain\Model\Theme $availableTheme */
			if($availableTheme->getExtensionName() === $theme) {
				$isValidTheme = TRUE;
				break;
			}
		}
		return $isValidTheme;
	}

	/**
	 * Loads and refresh the Theme YAML and Configuration
	 * @param       $theme
	 * @param array $metaData
	 * @param array $settings Extension settings
	 * @return string
	 * @throws \Exception
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function loadThemeYaml($theme, array $metaData=array(), $settings=array()) {

		$changed = 'none';
		if(in_array($theme, $settings['readonlyThemes'])) {
			return $changed;
		}

		// Get the theme yaml object
		/** @var \CodingMs\Ftm\Domain\Model\Theme $themeYaml */
		$themeYaml = $this->getThemeYamlObject($theme);
        //
		// Was the object modified?
		// Rewrite the theme.yaml
		if($themeYaml->getFileHash()=='write-object-back-into-yaml-file') {

			// Convert the data object into a PHP array
			$metaData = $themeYaml->getMetaData();

			// Write it into theme.yaml
			if($this->setMetaDataIntoThemeYamlFile($theme, $metaData, $settings)) {
				$changed = 'yaml-file-refreshed';
			}

			// Refresh the file hash
			$themeYaml->setFileHash(md5(serialize($metaData)));
		}
		// Otherwise, check if then theme.yaml has changed
		else {

			// Set the meta data
			// The setMetaData method compares the hash
			// when it's different, the theme.yaml data is
			// written into the theme-yaml-object
			if($themeYaml->setMetaData($metaData)) {
				$changed = 'yaml-object-refreshed';
			}
		}

		// Rewrite em_conf.php
		if($changed != 'none') {
			$this->extensionService->createEmConf($theme, $metaData, $settings);
		}

		// Update in repository
		$this->themeYamlRepository->update($themeYaml);
		$this->persistenceManager->persistAll();

		return $changed;
	}

	/**
	 * Returns the Theme Configuration/YAML Object!
	 * @param $theme
	 * @return \CodingMs\Ftm\Domain\Model\Theme
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function getThemeYamlObject($theme) {

		/** @var \CodingMs\Ftm\Domain\Model\Theme $themeYaml */
		$themeYaml = $this->themeYamlRepository->findOneByName($theme);
		if(!($themeYaml instanceof \CodingMs\Ftm\Domain\Model\Theme)) {

			/** @var \CodingMs\Ftm\Domain\Model\Theme $themeYaml */
			$themeYaml = $this->objectManager->get('CodingMs\\Ftm\\Domain\\Model\\Theme');
			$themeYaml->setPid(0);
			$themeYaml->setName($theme);
			$this->themeYamlRepository->add($themeYaml);
			$this->persistenceManager->persistAll();
		}
		return $themeYaml;
	}


	public function setMetaDataIntoThemeYamlFile($theme, $metaData=array(), $settings) {

		// But don't work on theme base packages
		if(in_array($theme, $settings['readonlyThemes'])) {
			return 0;
		}

		// Filename for the Meta/theme.yaml
		// can't use extPath, because in case of new theme
		// the theme extension isn't loaded yet
		//$themeYamlPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($theme);
		$themeYamlPath = 'typo3conf/ext/' . $theme;
		$themeYamlPath = GeneralUtility::getFileAbsFileName($themeYamlPath);
		$themeYamlFile = $themeYamlPath . '/Meta/theme.yaml';

		if (is_dir($themeYamlPath)) {
			if (class_exists('\Symfony\Component\Yaml\Yaml')) {
				$metaDataYaml = \Symfony\Component\Yaml\Yaml::dump(
					$metaData
				);
				BackupService::backupFile($themeYamlFile);
				return file_put_contents($themeYamlFile, $metaDataYaml);
			}
			else {
				$translation = LocalizationUtility::translate('tx_ftm_exception.yaml_parser_not_found', 'Ftm');
				throw new \Exception($translation);
			}
		}
		else {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_not_found', 'Ftm', array($themeYamlFile));
			throw new \Exception($translation);
		}
	}

	/**
	 * @param $theme
	 * @return array
	 * @throws \Exception
	 */
	public function getMetaDataFromThemeYamlFile($theme) {
		$metaData = array();
		if (is_file(ExtensionManagementUtility::extPath($theme) . 'Meta/theme.yaml')) {
			if (class_exists('\Symfony\Component\Yaml\Yaml')) {
				$metaData = \Symfony\Component\Yaml\Yaml::parse(
					file_get_contents(ExtensionManagementUtility::extPath($theme) . 'Meta/theme.yaml')
				);
			} else {
				$translation = LocalizationUtility::translate('tx_ftm_exception.yaml_parser_not_found', 'Ftm');
				throw new \Exception($translation);
			}
		}
		return $metaData;
	}

	/**
	 * Gets the first available theme
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @return \string The first available theme
	 * @since 2.0.0
	 */
	public function getFirstAvailableTheme() {
		$firstTheme = '';
		if(empty($this->availableThemes)) {
			$this->getAvailableThemes();
		}
		if(isset($this->availableThemes[0])) {
			/** @var \KayStrobach\Themes\Domain\Model\Theme $firstTheme */
			$firstTheme = $this->availableThemes[0];
			if($firstTheme instanceof \KayStrobach\Themes\Domain\Model\Theme) {
				$firstTheme = $firstTheme->getExtensionName();
			}
		}
		return $firstTheme;
	}

	/**
	 * Creates a new Theme
	 *
	 * @param        $extensionName
	 * @param        $title
	 * @param string $parentTheme
	 * @param array $settings Extension settings
	 * @return bool
     * @throws \Exception
	 */
	public function createNewTheme($extensionKey, $title, $parentTheme='none', $settings) {

		$success = TRUE;

		// Check if the directory already exists
		$relPath = 'typo3conf/ext/' . $extensionKey;
		$absPath = GeneralUtility::getFileAbsFileName($relPath);

		// Check if the extension key is a valid one
		//if(!GeneralUtility::validPathStr($extensionName)) {
		if(!$this->extensionService->isValidExtensionKey($extensionKey)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_key_is_not_valid', 'Ftm', array($extensionKey));
			throw new \Exception($translation);
		}

		// Check if the directory already exists
		if(is_dir($absPath)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_key_already_exists', 'Ftm', array($extensionKey));
			throw new \Exception($translation);
		}

		// Try to create the directory
		if(!mkdir($absPath)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_key_directory_creation_failed', 'Ftm', array($extensionKey));
			throw new \Exception($translation);
		}

		// Copy the icon
		$relPathThemes = 'typo3conf/ext/themes';
		$absPathThemes = GeneralUtility::getFileAbsFileName($relPathThemes);
		if(!copy($absPathThemes . '/ext_icon.png', $absPath . '/ext_icon.png')) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_icon_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}

		$metaData = array();
		$metaData['title'] = $title;
		$metaData['parentTheme'] = array();
		$metaData['description'] = 'New Theme, generated by EXT:FTM' . LF;
		if($parentTheme!='none') {
			$metaData['description'] .= 'Inherit from: ' . $parentTheme . LF;
			$metaData['parentTheme']['name'] = $parentTheme;
			$metaData['parentTheme']['version'] = ExtensionManagementUtility::getExtensionVersion($parentTheme);
		}
		else {
			$metaData['parentTheme']['name'] = $parentTheme;
			$metaData['parentTheme']['version'] = '';
		}
		$metaData['version'] = '0.0.1';


		// Create directories
		$this->extensionService->createExtensionDirectories($extensionKey);

		// Create ext_emconf.php
		if(!$this->extensionService->createEmConf($extensionKey, $metaData, $settings)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_configuration_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}

		// Create theme.yaml
		if(!$this->setMetaDataIntoThemeYamlFile($extensionKey, $metaData, $settings)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_yaml_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}

		// Create PageTS tsconfig.txt
		if(!$this->createTsconfigFile($extensionKey, $parentTheme)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_tsconfig_txt_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}

		// Create TypoScript constants.txt
		if(!$this->createConstantsFile($extensionKey, $parentTheme)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_constants_txt_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}

		// Create TypoScript setup.txt
		if(!$this->createSetupFile($extensionKey, $parentTheme)) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_setup_txt_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}

		return $success;
	}


	protected function createTsconfigFile($extensionKey, $parentTheme='', $extensions=array()) {

		$tsconfig = '';

		// Parent theme is setted, include his pagets
		if($parentTheme!='none' && $parentTheme!='') {
			$tsconfig .= '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $parentTheme . '/Configuration/PageTS/tsconfig.txt">' . LF;
		}

		// Include own pagets libraries
		$tsconfig .= '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extensionKey . '/Configuration/PageTS/Library/" extensions="pagets">' . LF;
		$tsconfig .= '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extensionKey . '/Configuration/BackendLayouts/" extensions="pagets">' . LF;
		$tsconfig .= '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extensionKey . '/Configuration/Elements/PageTS/" extensions="pagets">' . LF;

		// Include extension pagets libraries
		if(!empty($extensions)) {
			$tsconfig .= LF . LF . '# Extensions' . LF;
			foreach($extensions as $extension => $extensionData) {
				$extension = GeneralUtility::underscoredToUpperCamelCase($extension);
				$file = 'EXT:' . $extensionKey . '/Resources/Private/Extensions/' . $extension . '/PageTS/tsconfig.txt';
				$absFile = GeneralUtility::getFileAbsFileName($file);
				if(file_exists($absFile)) {
					$tsconfig .= '<INCLUDE_TYPOSCRIPT: source="FILE:' . $file . '">' . LF;
				}
			}
		}

		// Build file name
		$relPath = 'typo3conf/ext/' . $extensionKey;
		$absPath = GeneralUtility::getFileAbsFileName($relPath);
		$tsconfigFile = $absPath . '/Configuration/PageTS/tsconfig.txt';

		// Create file and return written bytes
		$currentContent = @file_get_contents($tsconfigFile);
		if($currentContent!=$tsconfig) {
			BackupService::backupFile($tsconfigFile);
			return file_put_contents($tsconfigFile, $tsconfig);
		}
		else {
			return -1;
		}
	}


	protected function createConstantsFile($extensionKey, $parentTheme='', $extensions=array()) {

		$constants = '';

		// Parent theme is setted, include his pagets
		if($parentTheme!='none' && $parentTheme!='') {
			$constants .= '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $parentTheme . '/Configuration/TypoScript/constants.txt">' . LF;
		}

		// Include own pagets libraries
		$constants .= '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extensionKey . '/Configuration/TypoScript/Library/" extensions="constantsts">' . LF;

		// Include extension pagets libraries
		if(!empty($extensions)) {
			$constants .= LF . LF . '# Extensions' . LF;
			foreach($extensions as $extension => $extensionData) {
				$extension = GeneralUtility::underscoredToUpperCamelCase($extension);
				$file = 'EXT:' . $extensionKey . '/Resources/Private/Extensions/' . $extension . '/TypoScript/constants.txt';
				$absFile = GeneralUtility::getFileAbsFileName($file);
				if(file_exists($absFile)) {
					$constants .= '<INCLUDE_TYPOSCRIPT: source="FILE:' . $file . '">' . LF;
				}
			}
		}

		// Build file name
		$relPath = 'typo3conf/ext/' . $extensionKey;
		$absPath = GeneralUtility::getFileAbsFileName($relPath);
		$constantsFile = $absPath . '/Configuration/TypoScript/constants.txt';

		// Create file and return written bytes
		$currentContent = @file_get_contents($constantsFile);
		if($currentContent!=$constants) {
			BackupService::backupFile($constantsFile);
			return file_put_contents($constantsFile, $constants);
		}
		else {
			return -1;
		}
	}


	protected function createSetupFile($extensionKey, $parentTheme='', $extensions=array()) {

		$setup = '';

		// Parent theme is setted, include his pagets
		if($parentTheme!='none' && $parentTheme!='') {
			$setup .= '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $parentTheme . '/Configuration/TypoScript/setup.txt">' . LF;
		}

		// Include own pagets libraries
		$setup .= '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extensionKey . '/Configuration/Elements/TypoScript/" extensions="setupts">' . LF;
		$setup .= '<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $extensionKey . '/Configuration/TypoScript/Library/" extensions="setupts">' . LF;

		// Include extension pagets libraries
		if(!empty($extensions)) {
			$setup .= LF . LF . '# Extensions' . LF;
			foreach($extensions as $extension => $extensionData) {
				$extension = GeneralUtility::underscoredToUpperCamelCase($extension);
				$file = 'EXT:' . $extensionKey . '/Resources/Private/Extensions/' . $extension . '/TypoScript/setup.txt';
				$absFile = GeneralUtility::getFileAbsFileName($file);
				if(file_exists($absFile)) {
					$setup .= '<INCLUDE_TYPOSCRIPT: source="FILE:' . $file . '">' . LF;
				}
			}
		}

		// Build file name
		$relPath = 'typo3conf/ext/' . $extensionKey;
		$absPath = GeneralUtility::getFileAbsFileName($relPath);
		$setupFile = $absPath . '/Configuration/TypoScript/setup.txt';

		// Create file and return written bytes
		$currentContent = @file_get_contents($setupFile);
		if($currentContent!=$setup) {
			BackupService::backupFile($setupFile);
			return file_put_contents($setupFile, $setup);
		}
		else {
			return -1;
		}
	}

	/**
	 * Installs a Theme-Extension in TYPO3
	 *
	 * @param $extensionKey string Extension-Key
	 */
	public function installTheme($extensionKey) {

		/** @var \TYPO3\CMS\Extensionmanager\Service\ExtensionManagementService $managementService */
		$managementService = $this->objectManager->get('TYPO3\\CMS\\Extensionmanager\\Service\\ExtensionManagementService');

		/** @var \TYPO3\CMS\Extensionmanager\Utility\ExtensionModelUtility $extensionModelUtility */
		$extensionModelUtility = $this->objectManager->get('TYPO3\\CMS\\Extensionmanager\\Utility\\ExtensionModelUtility');

		/** @var \TYPO3\CMS\Extensionmanager\Utility\InstallUtility $installUtility */
		$installUtility = $this->objectManager->get('TYPO3\\CMS\\Extensionmanager\\Utility\\InstallUtility');

		// Get ext_emconf-Array
		$emConf = $installUtility->enrichExtensionWithDetails($extensionKey);

		// install
		$managementService->installExtension(
			$extensionModelUtility->mapExtensionArrayToModel($emConf)
		);
	}

	public function getThemeScopes($extensionKey) {

		// Read the meta data (theme.yaml)
		$metaData = $this->getMetaDataFromThemeYamlFile($extensionKey);
		$themeScopes = array();
		$themeScopes['Theme'] = 'Theme';
		if(!empty($metaData['supportedExtensions'])) {
			foreach($metaData['supportedExtensions'] as $extensionKey => $extensionData) {
				$extensionKeyUppercase = GeneralUtility::underscoredToUpperCamelCase($extensionKey);
				$themeScopes[$extensionKeyUppercase] = $extensionKeyUppercase . ' (version ' . $extensionData['version'] . ')';
			}
		}
		return $themeScopes;
	}

    /**
     * @param $extensionKey
     * @param $parentTheme
     * @param array $extensions
     * @return array
     * @throws \Exception
     */
	public function refreshThemeFiles($extensionKey, $parentTheme, $extensions=array()) {

		$refreshedFiles = array();

		// Create PageTS tsconfig.txt
		$bytes = $this->createTsconfigFile($extensionKey, $parentTheme, $extensions);
		if($bytes==0) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_tsconfig_txt_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}
		else if($bytes>0) {
			$refreshedFiles[] = 'tsconfig.txt';
		}

		// Create TypoScript constants.txt
		$bytes = $this->createConstantsFile($extensionKey, $parentTheme, $extensions);
		if($bytes==0) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_constants_txt_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}
		else if($bytes>0) {
			$refreshedFiles[] = 'constants.txt';
		}

		// Create TypoScript setup.txt
		$bytes = $this->createSetupFile($extensionKey, $parentTheme, $extensions);
		if($bytes==0) {
			$translation = LocalizationUtility::translate('tx_ftm_exception.theme_extension_setup_txt_creation_failed', 'Ftm');
			throw new \Exception($translation);
		}
		else if($bytes>0) {
			$refreshedFiles[] = 'setup.txt';
		}

		return $refreshedFiles;
	}

	public static function fixPermissions($extensionKey) {
		$success = TRUE;
		$absPath = GeneralUtility::getFileAbsFileName('typo3conf/ext/' . $extensionKey);
		if(file_exists($absPath)) {
			$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($absPath));
			foreach($iterator as $item) {
				if(!@chmod($item, 0777)) {
					$success = FALSE;
				}
			}
		}
		else {
			$success = FALSE;
		}
		return $success;
	}

}
