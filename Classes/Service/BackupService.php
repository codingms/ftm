<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \CodingMs\Ftm\Utility\Tools;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Creates Backups on generated files
 *
 * @package ftm
 * @subpackage Service
 * 
 * @author Thomas Deuling <typo3@coding.ms>
 * @since 1.0.0
 */
class BackupService {

	/**
	 * Creates Backups on generated files
	 * @param string $file Filename of the file to be backuped
	 * @return boolean Backup could be written!?
	 */
	public static function backupFile($file) {
		
		// Backup-Content
		$fileContent = @file_get_contents($file);
		// Don't backup empty files
		if(!$fileContent) {
			return TRUE;
		}
	   
		// Get path
		$relPath = Tools::getDirectory("Backup", '');
		$absPath = GeneralUtility::getFileAbsFileName($relPath);

		// Create filename of the backup file
		$fileName = str_replace(PATH_site, '', $file);
		$fileName = str_replace('/', '--', $fileName);
		$fileName = date('Y-m-d_H-i-s_').$fileName;
		
		// .htaccess file check
		if(!file_exists($absPath . '.htaccess')) {
			$htaccess = 'deny from all';
			file_put_contents($absPath . '.htaccess', $htaccess);
		}
		
		// Write backup file
		if($fileContent=='') {
			return TRUE;
		}
		else {
			return (bool)file_put_contents($absPath.$fileName, $fileContent);
		}
	}

	public static function backupFullDatabase() {

		$database = $GLOBALS['TYPO3_CONF_VARS']['DB']['database'];
		$username = $GLOBALS['TYPO3_CONF_VARS']['DB']['username'];
		$password = $GLOBALS['TYPO3_CONF_VARS']['DB']['password'];
		$host = $GLOBALS['TYPO3_CONF_VARS']['DB']['host'];

		/** @var \TYPO3\CMS\Core\Database\DatabaseConnection $db */
		$db = $GLOBALS['TYPO3_DB'];

		$ignoreTables = array();
		$ignoreTables[] = 'tx_extensionmanager_domain_model_extension';

		$result = $db->sql_query('SHOW TABLES FROM ' . $database . ' LIKE \'cf_%\'');
		if (is_a($result, '\\mysqli_result')) {
			while ($tempRow = $db->sql_fetch_assoc($result)) {
				$ignoreTables[] = array_shift($tempRow);
			}
			$db->sql_free_result($result);
		}
		$result = $db->sql_query('SHOW TABLES FROM ' . $database . ' LIKE \'cache_%\'');
		if (is_a($result, '\\mysqli_result')) {
			while ($tempRow = $db->sql_fetch_assoc($result)) {
				$ignoreTables[] = array_shift($tempRow);
			}
			$db->sql_free_result($result);
		}


		$dumpSettings = array();
		$dumpSettings['exclude-tables'] = $ignoreTables;
		$dumpSettings['compress'] = 'None';
		$dumpSettings['compress'] = 'Gzip'; // None // Gzip
		
		// Get path
		$relPath = Tools::getDirectory("Backup", '');
		$absPath = GeneralUtility::getFileAbsFileName($relPath);
		$fileName = date('Y-m-d_H-i-s_') . 'backup.sql';
		if($dumpSettings['compress']=='Gzip') {
			$fileName .= '.gz';
		}

		
		$dump = new \Ifsnop\Mysqldump\Mysqldump('mysql:host=' . $host . ';dbname=' . $database, $username, $password, $dumpSettings);
		$dump->start($absPath.$fileName);

		$content = file_get_contents($absPath.$fileName);
		header("Cache-Control: no-cache, must-revalidate");
		header('Content-Length: ' . strlen($content));
		header('Content-Disposition: inline; filename="' . $fileName . '"');
		header('Content-Type: application/octet-stream');
		echo $content;
		exit;
	}
	
}

?>