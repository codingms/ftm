<?php
namespace CodingMs\Ftm\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Thomas Deuling <typo3@coding.ms>, coding.ms
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Theme snippet data service
 *
 * @package ftm
 * @subpackage Service
 */
class SnippetService {

	/**
	 * Search snippets
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param array $params
	 * @param array $settings
	 * @return array Array with snippets
	 * @since 2.0.0
	 */
	static public function searchSnippets($params=array(), $settings=array()) {
		
		// collect request parameters
		$configuration = \CodingMs\Ftm\Service\ExtensionConfigurationService::getConfiguration();
		$params['pcKey'] = $settings['pluginCloud']['key'];
		$params['user'] = $settings['pluginCloud']['user'];
		$params['password'] = $settings['pluginCloud']['password'];
		$params['allowLog'] = $configuration['allowLog'];
		
		// Build request
		$context = self::getStreamContext($params);
		$request = $configuration['pluginCloudHost'] . '/searchSnippets.php';
		
		// Get and return snippet data
		if(function_exists('curl_init')) {
			$ch = curl_init($request);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, array('User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Safari/534.45'));
			$json = curl_exec($ch);
		}
		else {
			$json = @file_get_contents($request, FALSE, $context);
		}

		/**
		 * @todo: server returns an error json like:
		 *      json_encode(array('error' => array('code' => $jsonError, 'message'=> $jsonErrorMessage)))
		 *      This must be checked!!
		 */
		
		if(!$json) {
			return NULL;
		}
		return json_decode($json, TRUE);
	}

	/**
	 * Load a complete Snippet
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param array $params
	 * @param array $settings
	 * @return array Array with snippet
	 * @since 2.0.0
	 */
	static public function loadSnippet($params=array(), $settings=array()) {

		// collect request parameters
		$configuration = \CodingMs\Ftm\Service\ExtensionConfigurationService::getConfiguration();
		$params['pcKey'] = $settings['pluginCloud']['key'];
		$params['user'] = $settings['pluginCloud']['user'];
		$params['password'] = $settings['pluginCloud']['password'];
		$params['allowLog'] = $configuration['allowLog'];

		// Build request
		$context = self::getStreamContext($params);
		$request = $configuration['pluginCloudHost'] . '/loadSnippet.php';

		// Get and return snippet data
		if(function_exists('curl_init')) {
			$ch = curl_init($request);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, array('User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Safari/534.45'));
			$json = curl_exec($ch);
		}
		else {
			$json = @file_get_contents($request, FALSE, $context);
		}

		/**
		 * @todo: server returns an error json like:
		 *      json_encode(array('error' => array('code' => $jsonError, 'message'=> $jsonErrorMessage)))
		 *      This must be checked!!
		 */
		
		if(!$json) {
			return NULL;
		}
		return json_decode($json, TRUE);
	}

	/**
	 * Saves a complete Snippet
	 *
	 * @author Thomas Deuling <typo3@coding.ms>
	 * @param array $params
	 * @param array $settings
	 * @return array Array with return data
	 * @since 2.0.0
	 */
	static public function saveSnippet($params=array(), $settings=array()) {

		// collect request parameters
		$configuration = \CodingMs\Ftm\Service\ExtensionConfigurationService::getConfiguration();
		$params['pcKey'] = $settings['pluginCloud']['key'];
		$params['user'] = $settings['pluginCloud']['user'];
		$params['password'] = $settings['pluginCloud']['password'];
		$params['allowLog'] = $configuration['allowLog'];

		// Build request
		$context = self::getStreamContext($params);
		$request = $configuration['pluginCloudHost'] . '/saveSnippet.php';

		// Get and return snippet data
		if(function_exists('curl_init')) {
			$ch = curl_init($request);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, array('User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Safari/534.45'));
			$json = curl_exec($ch);
		}
		else {
			$json = @file_get_contents($request, FALSE, $context);
		}

		/**
		 * @todo: server returns an error json like:
		 *      json_encode(array('error' => array('code' => $jsonError, 'message'=> $jsonErrorMessage)))
		 *      This must be checked!!
		 */
		
		if(!$json) {
			return NULL;
		}
		return json_decode($json, TRUE);
		
	}

	static protected function getStreamContext($params=array()) {
		$postdata = http_build_query($params, '', '&');
		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);
		return stream_context_create($opts);
	}

}