<?php

namespace CodingMs\Modules\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * Backend base controller
 */
class BackendController extends ActionController
{

    /**
     * Object-Manager
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * BackendTemplateContainer
     *
     * @var BackendTemplateView
     */
    protected $view;

    /**
     * Backend Template Container
     *
     * @var BackendTemplateView
     */
    protected $defaultViewObjectName = BackendTemplateView::class;

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    /**
     * @var int
     */
    protected $pageUid;

    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        $this->pageUid = (int)GeneralUtility::_GP('id');
        /** @var BackendTemplateView $view */
        if ($this->view->getModuleTemplate() !== null) {
            $pageRenderer = $this->view->getModuleTemplate()->getPageRenderer();
            $extRealPath = '../' . ExtensionManagementUtility::siteRelPath('ftm');
            $pageRenderer->addCssFile($extRealPath . 'Resources/Public/Stylesheets/BackendModule.css');
            $pageRenderer->loadRequireJsModule('TYPO3/CMS/Backend/Modal');
        }
        // Initialize icon factory
        $this->iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        parent::initializeView($view);
    }

    /**
     * Returns the currently logged in BE user
     *
     * @return BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * @param $key
     * @param array $arguments
     * @return NULL|string
     * @version 1.0.0
     * @throws \Exception
     */
    protected function translate($key, $arguments=[]) {
        $translation = LocalizationUtility::translate($key, $this->extensionName, $arguments);
        if($translation === null) {
            $extensionKey = GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName);
            $path = 'EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf';
            throw new \Exception('Translation ' . $key . ' not found in ' . $path);
        }
        return $translation;
    }

    /**
     * @param $key
     * @return Icon
     * @version 1.0.0
     */
    protected function getIcon($key) {
        return $this->iconFactory->getIcon($key, Icon::SIZE_SMALL);
    }

}
