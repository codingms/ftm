<?php

namespace CodingMs\Ftm\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Modules\Controller\BackendController;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

/**
 * Controller for handling and manage ftm-settings
 *
 * @package ftm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SettingsController extends BackendController
{

    /**
     * Settings service
     *
     * @var \CodingMs\Ftm\Service\SettingsService
     * @inject
     */
    protected $settingsService;

    /**
     * @var array Collect for saving
     */
    protected $saveSettings = [];

    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     * @throws \Exception
     */
    protected function initializeView(ViewInterface $view)
    {
        /** @var BackendTemplateView $view */
        parent::initializeView($view);
        if ($this->view->getModuleTemplate() !== null) {
            $this->createButtons();
        }
    }

    /**
     * Initialize any action
     */
    public function initializeAction()
    {
        // Load and merge the settings
        $loadedSettings = $this->settingsService->getSettings();
        $this->settings = array_replace_recursive($this->settings, $loadedSettings);
    }

    /**
     * Add menu buttons for specific actions
     *
     * @return void
     * @throws \Exception
     */
    protected function createButtons()
    {
        $moduleName = 'tools_FtmFtm';
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $buttons = [];
        switch ($this->request->getControllerActionName()) {
            case 'edit':
                // Back to theme overview
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'index',
                        'controller' => 'Overview'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_back_to_overview'))
                    ->setIcon($this->getIcon('actions-close'));
                break;
        }
        foreach ($buttons as $button) {
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        }
    }

    /**
     * Displays the settings for editing
     * @author Thomas Deuling <typo3@coding.ms>
     * @return void
     * @since 2.0.0
     */
    public function editAction()
    {
        // Set the settings!
        // Required because of adjusting values!
        $this->view->assign('settings', $this->settings);
    }

    /**
     * Saves the settings
     * @author Thomas Deuling <typo3@coding.ms>
     * @return void
     *
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function saveAction()
    {
        $requestSettings = $this->request->getArguments();
        $this->saveNodesRecursive($this->settings, $requestSettings);
        $this->settingsService->setSettings($this->settings);
        $translation = $this->translate('tx_ftm_message.ok_settings_successfully_saved');
        $this->addFlashMessage($translation, '', AbstractMessage::OK);
        if ($this->request->hasArgument('saveAndBack')) {
            $this->redirect('index', 'Overview');
        } else {
            $this->redirect('edit');
        }
    }

    /**
     * Saves settings nodes recursive
     * @param $settingsNode
     * @param $requestSettings
     * @param array $pathParts
     */
    protected function saveNodesRecursive($settingsNode, $requestSettings, $pathParts = [])
    {
        if (!empty($settingsNode)) {
            foreach ($settingsNode as $key => $value) {
                if (is_array($value)) {
                    array_push($pathParts, $key);
                    $this->saveNodesRecursive($value, $requestSettings, $pathParts);
                    array_pop($pathParts);
                } else {
                    $requestSettingsKey = implode('_', $pathParts) . '_' . $key;
                    if (isset($requestSettings[$requestSettingsKey])) {
                        if ($requestSettingsKey == 'pluginCloud_password') {
                            if ($requestSettings[$requestSettingsKey] != 'noPassword' && trim($requestSettings[$requestSettingsKey]) != '') {
                                $value = md5($requestSettings[$requestSettingsKey]);
                                $this->setSettingsValue($requestSettingsKey, $value);
                            }
                        } else {
                            $this->setSettingsValue($requestSettingsKey, $requestSettings[$requestSettingsKey]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Saves a value into settings
     * @param $key
     * @param $value
     */
    protected function setSettingsValue($key, $value)
    {
        $keys = explode('_', $key);
        if (count($keys) == 1) {
            $this->settings[$keys[0]] = $value;
        } else {
            if (count($keys) == 2) {
                $this->settings[$keys[0]][$keys[1]] = $value;
            } else {
                if (count($keys) == 3) {
                    $this->settings[$keys[0]][$keys[1]][$keys[2]] = $value;
                } else {
                    if (count($keys) == 4) {
                        $this->settings[$keys[0]][$keys[1]][$keys[2]][$keys[3]] = $value;
                    }
                }
            }
        }
    }

}
