<?php

namespace CodingMs\Ftm\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Messaging\AbstractMessage;
use \CodingMs\Ftm\Service\UpdatesService;
use CodingMs\Modules\Controller\BackendController;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Controller for Theme-Overview and working on files
 *
 * @package ftm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class OverviewController extends BackendController
{

    /**
     * Theme file data service
     *
     * @var \CodingMs\Ftm\Service\ThemeFileDataService
     * @inject
     */
    protected $themeFileDataService;

    /**
     * Theme service
     *
     * @var \CodingMs\Ftm\Service\ThemeService
     * @inject
     */
    protected $themeService;

    /**
     * Storage service
     *
     * @var \CodingMs\Ftm\Service\StorageService
     * @inject
     */
    protected $storageService;

    /**
     * Settings service
     *
     * @var \CodingMs\Ftm\Service\SettingsService
     * @inject
     */
    protected $settingsService;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    public $objectManager;

    /**
     * @var array File data array
     */
    protected $themeFileData = [];

    /**
     * @var array Available Themes in the system
     */
    protected $availableThemes = [];

    /**
     * @var string Name of the selected theme
     */
    protected $selectedTheme = '';

    /**
     * @var array Available Theme-Scopes in the theme
     */
    protected $themeScopes = [];

    /**
     * @var string Name of the selected theme scope
     */
    protected $selectedThemeScope = 'Theme';

    /**
     * @var boolean A theme selected!?
     */
    protected $themeSelected = false;

    /**
     * Set up the doc header properly here
     * @param ViewInterface $view
     * @throws \Exception
     */
    protected function initializeView(ViewInterface $view)
    {
        /** @var BackendTemplateView $view */
        parent::initializeView($view);
        if ($this->view->getModuleTemplate() !== null) {
            $this->createButtons();
        }
    }

    /**
     * Initialize any action
     * @throws \Exception
     */
    public function initializeAction()
    {
        // Create controller context for flash massages
        $this->controllerContext = $this->buildControllerContext();
        // Load and merge the settings
        $loadedSettings = $this->settingsService->getSettings();
        $this->settings = array_replace_recursive($this->settings, $loadedSettings);
        // Validate the selected theme
        $this->validateSelectedTheme();
    }

    /**
     * Add menu buttons for specific actions
     * @throws \Exception
     */
    protected function createButtons()
    {
        $moduleName = 'tools_FtmFtm';
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $buttons = [];
        switch ($this->request->getControllerActionName()) {
            case 'index':
                //
                // New theme button
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'newTheme',
                        'controller' => 'Theme'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_create_a_new_theme'))
                    ->setIcon($this->getIcon('actions-page-new'));
                //
                // Only if a theme is selected
                if (trim($this->settings['selectedTheme']) !== '') {
                    // Edit Meta data button
                    // Return url
                    $parameter = [
                        'id' => $this->pageUid,
                        'tx_ftm_' . strtolower($moduleName) => [
                            'action' => 'index',
                            'controller' => 'Overview'
                        ]
                    ];
                    $returnUrl = BackendUtility::getModuleUrl($moduleName, $parameter);
                    // Edit Meta data button
                    $themeYaml = $this->themeService->getThemeYamlObject($this->selectedTheme);
                    $parameter = [
                        'returnUrl' => $returnUrl,
                        'edit' => [
                            'tx_ftm_domain_model_theme' => [
                                $themeYaml->getUid() => 'edit'
                            ]
                        ]
                    ];
                    $buttons[] = $buttonBar->makeLinkButton()
                        ->setHref(BackendUtility::getModuleUrl('record_edit', $parameter))
                        ->setTitle($this->translate('tx_ftm_label.button_edit_theme_meta_data'))
                        ->setIcon($this->getIcon('actions-page-open'));
                    //
                    // Services button
                    $parameter = [
                        'id' => $this->pageUid,
                        'tx_ftm_' . strtolower($moduleName) => [
                            'action' => 'index',
                            'controller' => 'Services'
                        ]
                    ];
                    $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                    $buttons[] = $buttonBar->makeLinkButton()
                        ->setHref($uriCancel)
                        ->setTitle($this->translate('tx_ftm_label.button_services'))
                        ->setIcon($this->getIcon('actions-system-options-view'));
                }
                //
                // Edit settings button
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'edit',
                        'controller' => 'Settings'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_edit_settings'))
                    ->setIcon($this->getIcon('actions-system-extension-configure'));
                break;
            case 'newTheme':
                //
                // Back to theme overview
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'index',
                        'controller' => 'Overview'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_back_to_overview'))
                    ->setIcon($this->getIcon('actions-close'));
                break;
        }
        foreach ($buttons as $button) {
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        }
    }

    /**
     * List theme files from selected scope
     * @author Thomas Deuling <typo3@coding.ms>
     * @return void
     * @throws \Exception
     */
    public function indexAction()
    {
        //
        // Theme files needs to be rewritten?
        /** @var \CodingMs\Ftm\Domain\Model\Theme $themeYaml */
        $themeYaml = $this->themeService->getThemeYamlObject($this->selectedTheme);
        $refreshThemeFilesRequired = $themeYaml->getFileHash() === 'write-object-back-into-yaml-file';
        $this->view->assign('refreshThemeFilesRequired', $refreshThemeFilesRequired);
        //
        // Set the settings!
        // Required because of adjusting values!
        $this->view->assign('settings', $this->settings);
        $overviewColumnCount = $this->settingsService->getOverviewColumnCount($this->settings);
        $this->view->assign('overviewColSpan', ($overviewColumnCount + 1));
        $this->view->assign('debugRequests', $this->request->getArguments());
        //
        // Theme scope
        $this->view->assign('themeScopes', $this->themeScopes);
        $this->view->assign('selectedThemeScope', $this->selectedThemeScope);
        //
        // File data
        $this->themeFileData = $this->themeFileDataService->getThemeFileData(
            $this->selectedTheme,
            $this->selectedThemeScope,
            $this->settings
        );
        $this->view->assign('themeFileData', $this->themeFileData);
        $this->view->assign('selectedTheme', $this->selectedTheme);
        $this->view->assign('availableThemes', $this->availableThemes);
        $this->view->assign('themeSelected', $this->themeSelected);
        //
        // Readonly!?
        if (in_array($this->selectedTheme, $this->settings['readonlyThemes'])) {
            $translation = $this->translate('tx_ftm_message.warning_theme_is_readonly');
            $this->addFlashMessage($translation, '', AbstractMessage::WARNING);
            $this->view->assign('themeReadonly', true);
        } else {
            $this->view->assign('themeReadonly', false);
        }
        //
        // Update available?!
        global $TYPO3_CONF_VARS;
        $version = $TYPO3_CONF_VARS['EXTCONF']['ftm']['version'];
        $updateMessage = UpdatesService::check('ftm', explode('.', $version));
        if ($updateMessage) {
            $this->addFlashMessage($updateMessage, '', AbstractMessage::WARNING);
        }
    }

    /**
     * Validates the selected theme
     * @author Thomas Deuling <typo3@coding.ms>
     * @return void
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    protected function validateSelectedTheme()
    {
        //
        // Get all available themes
        $this->availableThemes = $this->themeService->getAvailableThemes();
        //
        // Check if a theme is passed by request param
        if ($this->request->hasArgument('theme')) {
            $validateTheme = $this->request->getArgument('theme');
            if ($this->themeService->isValidTheme($validateTheme)) {
                $this->selectedTheme = $validateTheme;
            }
        } // try to read from settings
        else {
            if (isset($this->settings['selectedTheme'])) {
                $validateTheme = $this->settings['selectedTheme'];
                if ($this->themeService->isValidTheme($validateTheme)) {
                    $this->selectedTheme = $validateTheme;
                }
            }
        }
        //
        // otherwise select the first available theme
        if ($this->selectedTheme === '') {
            $this->selectedTheme = $this->themeService->getFirstAvailableTheme();
        }
        if ($this->selectedTheme === '') {
            $translation = $this->translate('tx_ftm_exception.validating_theme_fails');
            $this->addFlashMessage($translation, '', AbstractMessage::INFO);
            $this->redirect('newTheme', 'Theme');
        } else {
            $this->settings['selectedTheme'] = $this->selectedTheme;
        }
        $this->themeSelected = true;
        //
        // Validate theme scope
        $this->themeScopes = $this->themeService->getThemeScopes($this->selectedTheme);
        if ($this->request->hasArgument('themeScope')) {
            $selectedThemeScopeForInArray = $this->request->getArgument('themeScope');
            if (array_key_exists($selectedThemeScopeForInArray, $this->themeScopes)) {
                $this->selectedThemeScope = $selectedThemeScopeForInArray;
            }
        } // try to read from settings
        else {
            if (isset($this->settings['selectedThemeScope'])) {
                $selectedThemeScopeForInArray = $this->settings['selectedThemeScope'];
                if (array_key_exists($selectedThemeScopeForInArray, $this->themeScopes)) {
                    $this->selectedThemeScope = $selectedThemeScopeForInArray;
                }
            }
        }
        $this->settings['selectedThemeScope'] = $this->selectedThemeScope;
        //
        // Validate the filter settings
        if (!empty($this->settings['overview']['filter'])) {
            foreach ($this->settings['overview']['filter'] as $filter => $value) {
                if ($this->request->hasArgument('overview_filter_' . $filter)) {
                    $this->settings['overview']['filter'][$filter] = $this->request->getArgument('overview_filter_' . $filter);
                }
            }
        }
        //
        // Save settings data
        $this->settingsService->setSettings($this->settings);
        //
        // Validate the file storage
        $storageUid = $this->storageService->storageExists($this->selectedTheme);
        if ($storageUid === 0) {
            $storageUid = $this->storageService->createStorage($this->selectedTheme);
            if ($storageUid === 0) {
                $translation = $this->translate('tx_ftm_message.error_create_storage_failed');
                $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
            } else {
                $translation = $this->translate('tx_ftm_message.info_file_storage_created');
                $this->addFlashMessage($translation, '', AbstractMessage::INFO);
            }
        }
    }

}
