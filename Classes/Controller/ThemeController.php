<?php

namespace CodingMs\Ftm\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Ftm\Utility\Tools;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \CodingMs\Ftm\Service\BackupService;
use \CodingMs\Ftm\Service\SnippetService;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use CodingMs\Modules\Controller\BackendController;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use \TYPO3\CMS\Core\Resource\ResourceStorage;
use \TYPO3\CMS\Core\Resource\File;


/**
 * Controller for creating, ... Themes
 *
 * @package ftm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ThemeController extends BackendController
{

    /**
     * BackendTemplateContainer
     *
     * @var BackendTemplateView
     */
    protected $view;

    /**
     * Backend Template Container
     *
     * @var BackendTemplateView
     */
    protected $defaultViewObjectName = BackendTemplateView::class;

    /**
     * Theme service
     *
     * @var \CodingMs\Ftm\Service\ThemeService
     * @inject
     */
    protected $themeService;

    /**
     * Settings service
     *
     * @var \CodingMs\Ftm\Service\SettingsService
     * @inject
     */
    protected $settingsService;

    /**
     * Theme service
     *
     * @var \CodingMs\Ftm\Service\StorageService
     * @inject
     */
    protected $storageService;

    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     * @param ViewInterface $view
     * @throws \Exception
     */
    protected function initializeView(ViewInterface $view)
    {
        /** @var BackendTemplateView $view */
        parent::initializeView($view);
        if ($this->view->getModuleTemplate() !== null) {
            $pageRenderer = $this->view->getModuleTemplate()->getPageRenderer();
            // Require Ace editor
            if ($this->request->getControllerActionName() === 'editFile') {
                $contribPath = 'Resources/Public/Contrib/';
                $contribPath = ExtensionManagementUtility::extPath('ftm', $contribPath);
                $pageRenderer->addRequireJsConfiguration([
                    'shim' => [],
                    'paths' => [
                        'ace' => PathUtility::getAbsoluteWebPath($contribPath) . 'Ace/ace'
                    ]
                ]);
                $pageRenderer->loadRequireJsModule('TYPO3/CMS/Ftm/AceEditor');
            }
            // Require FTM library
            if ($this->request->getControllerActionName() === 'newFile') {
                $pageRenderer->loadRequireJsModule('TYPO3/CMS/Ftm/BackendModule');
            }
            // Create menu and buttons
            $this->createButtons();
        }
    }

    /**
     * Initialize any action
     */
    public function initializeAction()
    {
    }

    /**
     * Add menu buttons for specific actions
     *
     * @return void
     * @throws \Exception
     */
    protected function createButtons()
    {
        $moduleName = 'tools_FtmFtm';
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $buttons = [];
        switch ($this->request->getControllerActionName()) {
            case 'newTheme':
                // Back to theme overview
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'index',
                        'controller' => 'Overview'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_back_to_overview'))
                    ->setIcon($this->getIcon('actions-close'));
                break;
            case 'newFile':
                // Back to theme overview
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'index',
                        'controller' => 'Overview'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_back_to_overview'))
                    ->setIcon($this->getIcon('actions-close'));
                break;
            case 'editFile':
                // Back to theme overview
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'index',
                        'controller' => 'Overview'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_back_to_overview'))
                    ->setClasses('editor-close')
                    ->setIcon($this->getIcon('actions-close'));
                // Save button
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_save'))
                    ->setClasses('editor-save')
                    ->setIcon($this->getIcon('actions-document-save'));
                // Save and close button
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_save_and_back_to_overview'))
                    ->setClasses('editor-save-and-close')
                    ->setIcon($this->getIcon('actions-document-save-close'));
                break;
        }
        foreach ($buttons as $button) {
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        }
    }

    /**
     * Validates the selected theme
     * @return string
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    protected function validateSelectedTheme()
    {
        // Get all available themes
        $this->availableThemes = $this->themeService->getAvailableThemes();
        // Check if a theme is passed by request param
        $selectedTheme = '';
        if ($this->request->hasArgument('theme')) {
            $validateTheme = $this->request->getArgument('theme');
            if ($this->themeService->isValidTheme($validateTheme)) {
                $selectedTheme = $validateTheme;
            }
        }
        return $selectedTheme;
    }

    /**
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function newFileAjaxAction()
    {

        // Default step is step 1
        $step = 1;

        // Message data
        $messages = [];
        $messages['form'] = [];
        $messages['error'] = [];
        $messages['success'] = [];
        $messages['request'] = $this->request->getArguments();
        $messages['settings'] = $this->request->getArguments();


        // Check the selected theme
        $selectedTheme = $this->validateSelectedTheme();
        if ($selectedTheme == '') {
            $translation = $this->translate('tx_ftm_exception.validating_theme_fails');
            throw new \Exception($translation);
        }

        // Theme scope
        $scopes = $this->themeService->getThemeScopes($selectedTheme);
        $selectedScope = 'Theme';
        $scopeFormSettings = 'Theme';
        if ($this->request->hasArgument('scope')) {
            $selectedScopeForInArray = $this->request->getArgument('scope');
            if (array_key_exists($selectedScopeForInArray, $scopes) && $selectedScopeForInArray != 'Theme') {
                $selectedScope = $selectedScopeForInArray;
                $scopeFormSettings = 'Extension';
            }
        }

        // File types
        $types = [];
        if (!empty($this->settings['files'][$scopeFormSettings])) {
            foreach ($this->settings['files'][$scopeFormSettings] as $type => $typeData) {
                $types[$type] = $type;
            }
            ksort($types);
        }

        // Selected file type
        $selectedType = '';
        if ($this->request->hasArgument('type')) {
            $selectedTypeForInArray = $this->request->getArgument('type');
            if (array_key_exists($selectedTypeForInArray, $types)) {
                $selectedType = $selectedTypeForInArray;
                $step = 2;
            }
        }
        $messages['form']['type'] = $selectedType;


        // File sub types
        $subTypes = [];
        if (!empty($this->settings['files'][$scopeFormSettings][$selectedType])) {
            foreach ($this->settings['files'][$scopeFormSettings][$selectedType] as $subType => $subTypeData) {
                $subTypes[$subType] = $subType;
            }
            ksort($subTypes);
        }
        $messages['form']['subTypes'] = $subTypes;

        // When the file type is valid
        if ($step == 2) {

            // Selected file subType
            $selectedSubType = '';
            if ($this->request->hasArgument('subType')) {
                $selectedSubTypeForInArray = $this->request->getArgument('subType');
                if (array_key_exists($selectedSubTypeForInArray, $subTypes)) {
                    $selectedSubType = $selectedSubTypeForInArray;
                    $step = 3;
                }
            }
            $messages['form']['subType'] = $selectedSubType;


            // Directories
            $directories = [];
            $directoriesArray = [];
            if (isset($this->settings['files'][$scopeFormSettings][$selectedType])) {
                $directoriesArray = $this->settings['files'][$scopeFormSettings][$selectedType][$selectedSubType]['Directories'];
                if (!empty($directoriesArray)) {
                    foreach ($directoriesArray as $directory => $directoryData) {
                        $directories[$directory] = str_replace('_', '/', $directory);
                    }
                }
                ksort($directories);
            }
            $messages['form']['directories'] = $directories;

            // When the directory is valid
            if ($step == 3) {
                // Directory
                $selectedDirectory = '';
                if ($this->request->hasArgument('directory')) {
                    $selectedDirectoryForInArray = $this->request->getArgument('directory');
                    if (array_key_exists($selectedDirectoryForInArray, $directoriesArray)) {
                        $selectedDirectory = $selectedDirectoryForInArray;
                        $step = 4;
                    }
                }

                // When the directory is valid
                if ($step == 4) {

                    // Load and add the snippets
                    $snippetParams = [];
                    $snippetParams['type'] = $selectedType;
                    $snippetParams['subType'] = $selectedSubType;
                    $snippetParams['scope'] = $selectedScope;
                    $snippets = SnippetService::searchSnippets($snippetParams, $this->settings);
                    if ($snippets == null) {
                        $translation = $this->translate('tx_ftm_message.error_snippet_loading_failed');
                        $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                        $snippets = [];
                    }
                    $messages['snippets'] = $snippets;


                    /*$this->view->assign('snippetParams', $snippetParams);
                    $this->view->assign('snippets', $snippets);
                    // Validate Snippet selection
                    $snippetUid = 0;
                    if ($this->request->hasArgument('snippetUid')) {
                        $snippetUid = (int)$this->request->getArgument('snippetUid');
                    }
                    $this->view->assign('snippetUid', $snippetUid);*/

                    // Validate the file extension
                    // Get the first entry as default
                    $extensions = $this->settings['files'][$scopeFormSettings][$selectedType][$selectedSubType]['Directories'][$selectedDirectory]['Extensions'];
                    $messages['form']['extensions'] = $extensions;

                }

            }
        }

        $this->returnJson($messages);
    }

    /**
     * PHP-Array to JSON conversion
     *
     * @param array $array
     * @return void
     */
    protected function returnJson(array $array = [])
    {
        $json = json_encode($array);
        header("Content-Type: application/json; charset=utf-8");
        header("Content-Transfer-Encoding: 8bit");
        header("Content-Length: " . strlen($json));
        echo $json;
        exit;
    }

    /**
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function newFileAction()
    {
        // Abort and back to overview
        if ($this->request->hasArgument('abortAndBack')) {
            $this->redirect('index', 'Overview');
        }
        // Default step is step 1
        $step = 1;
        // Check the selected theme
        $selectedTheme = $this->validateSelectedTheme();
        if ($selectedTheme == '') {
            $translation = $this->translate('tx_ftm_exception.validating_theme_fails');
            throw new \Exception($translation);
        }
        //
        // Theme scope
        $scopes = $this->themeService->getThemeScopes($selectedTheme);
        $selectedScope = 'Theme';
        $scopeFormSettings = 'Theme';
        if ($this->request->hasArgument('scope')) {
            $selectedScopeForInArray = $this->request->getArgument('scope');
            if (array_key_exists($selectedScopeForInArray, $scopes) && $selectedScopeForInArray != 'Theme') {
                $selectedScope = $selectedScopeForInArray;
                $scopeFormSettings = 'Extension';
            }
        }
        //
        // File types
        $types = [];
        if (!empty($this->settings['files'][$scopeFormSettings])) {
            foreach ($this->settings['files'][$scopeFormSettings] as $type => $typeData) {
                $types[$type] = $type;
            }
            ksort($types);
        }
        //
        // Selected file type
        $selectedType = '';
        if ($this->request->hasArgument('type')) {
            $selectedTypeForInArray = $this->request->getArgument('type');
            if (array_key_exists($selectedTypeForInArray, $types)) {
                $selectedType = $selectedTypeForInArray;
                $step = 2;
            }
        }
        //
        // When the file type is valid
        if ($step == 2) {
            //
            // File sub types
            $subTypes = [];
            if (!empty($this->settings['files'][$scopeFormSettings][$selectedType])) {
                foreach ($this->settings['files'][$scopeFormSettings][$selectedType] as $subType => $subTypeData) {
                    $subTypes[$subType] = $subType;
                }
                ksort($subTypes);
            }
            // Selected file subType
            $selectedSubType = '';
            if ($this->request->hasArgument('subType')) {
                $selectedSubTypeForInArray = $this->request->getArgument('subType');
                if (array_key_exists($selectedSubTypeForInArray, $subTypes)) {
                    $selectedSubType = $selectedSubTypeForInArray;
                    $step = 3;
                }
            }
            // When the directory is valid
            if ($step == 3) {
                //
                // Directories
                $directories = [];
                $directoriesArray = [];
                if (isset($this->settings['files'][$scopeFormSettings][$selectedType])) {
                    $directoriesArray = $this->settings['files'][$scopeFormSettings][$selectedType][$selectedSubType]['Directories'];
                    if (!empty($directoriesArray)) {
                        foreach ($directoriesArray as $directory => $directoryData) {
                            $directories[$directory] = str_replace('_', '/', $directory);
                        }
                    }
                    ksort($directories);
                }
                // Directory
                $selectedDirectory = '';
                if ($this->request->hasArgument('directory')) {
                    $selectedDirectoryForInArray = $this->request->getArgument('directory');
                    if (array_key_exists($selectedDirectoryForInArray, $directoriesArray)) {
                        $selectedDirectory = $selectedDirectoryForInArray;
                        $step = 4;
                    }
                }
                //
                // When the directory is valid
                if ($step == 4) {
                    //
                    // Load and add the snippets
                    $snippetParams = [];
                    $snippetParams['type'] = $selectedType;
                    $snippetParams['subType'] = $selectedSubType;
                    $snippetParams['scope'] = $selectedScope;
                    $snippets = SnippetService::searchSnippets($snippetParams, $this->settings);
                    if ($snippets == null) {
                        $translation = $this->translate('tx_ftm_message.error_snippet_loading_failed');
                        $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                        $snippets = [];
                    }
                    $this->view->assign('snippetParams', $snippetParams);
                    $this->view->assign('snippets', $snippets);
                    // Validate Snippet selection
                    $snippetUid = 0;
                    if ($this->request->hasArgument('snippetUid')) {
                        $snippetUid = (int)$this->request->getArgument('snippetUid');
                    }
                    $this->view->assign('snippetUid', $snippetUid);
                    //
                    // Validate the file extension
                    // Get the first entry as default
                    $extensions = $this->settings['files'][$scopeFormSettings][$selectedType][$selectedSubType]['Directories'][$selectedDirectory]['Extensions'];
                    reset($extensions);
                    $selectedExtension = key($extensions);
                    // Check if there was passed a file extension
                    if ($this->request->hasArgument('extension')) {
                        $selectedExtensionForInArray = $this->request->getArgument('extension');
                        if (array_key_exists($selectedExtensionForInArray, $extensions)) {
                            $selectedExtension = $selectedExtensionForInArray;
                        }
                    }
                    if (trim($selectedExtension) == '') {
                        $translation = $this->translate('tx_ftm_message.error_please_select_a_valid_file_extension');
                        $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                    }
                    //
                    // Validate the file name
                    if ($this->request->hasArgument('fileName') && trim($selectedExtension) != '') {
                        $fileName = $this->request->getArgument('fileName');
                        if (Tools::checkValidFilename($fileName) && trim($fileName) != '') {
                            //
                            // Load snippet content
                            $content = '';
                            if ($snippetUid > 0) {
                                $snippetParams = [];
                                $snippetParams['uid'] = $snippetUid;
                                $snippet = SnippetService::loadSnippet($snippetParams, $this->settings);
                                if ($snippet['status'] == 'success' && isset($snippet['snippet']['content'])) {
                                    $content = $snippet['snippet']['content'];
                                }
                            }
                            //
                            // Write the new file
                            if ($scopeFormSettings == 'Extension') {
                                $search = 'Extensions_Extension';
                                $replace = 'Extensions_' . $selectedScope;
                                $selectedDirectory = str_replace($search, $replace, $selectedDirectory);
                            }
                            $directory = str_replace('_', '/', $selectedDirectory);
                            $file = $fileName . '.' . $selectedExtension;
                            $relPath = "typo3conf/ext/" . $selectedTheme . '/' . $directory . '/';
                            if ($this->writeFile($relPath, $file, $content, true)) {
                                $step = 1;
                                $translation = $this->translate('tx_ftm_message.ok_theme_file_successfully_created');
                                // Create and back to overview
                                if ($this->request->hasArgument('createAndBack')) {
                                    $this->redirect('index', 'Overview');
                                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                                } else {
                                    if ($this->request->hasArgument('createAndEdit')) {
                                        $arguments = [];
                                        $arguments['name'] = $file;
                                        $arguments['relPath'] = $directory . '/';
                                        $arguments['theme'] = $selectedTheme;
                                        $arguments['fileExtension'] = $selectedExtension;
                                        $this->redirect('editFile', 'Theme', null, $arguments);
                                    } else {
                                        $this->addFlashMessage($translation, '', AbstractMessage::OK);
                                    }
                                }
                            } else {
                                $translation = $this->translate('tx_ftm_message.error_theme_file_creation_failed');
                                $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                            }
                        } else {
                            $this->view->assign('fileName', $fileName);
                            $translation = $this->translate('tx_ftm_message.error_please_enter_a_valid_file_name');
                            $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                        }
                    }
                    $this->view->assign('extensions', $extensions);
                    $this->view->assign('selectedExtension', $selectedExtension);
                    // End of step 4
                }
                $this->view->assign('directories', $directories);
                $this->view->assign('selectedDirectory', $selectedDirectory);
                // End of Step 3
            }
            $this->view->assign('subTypes', $subTypes);
            $this->view->assign('selectedSubType', $selectedSubType);
            // End of Step 2
        }
        $this->view->assign('step', $step);
        $this->view->assign('selectedTheme', $selectedTheme);
        $this->view->assign('selectedScope', $selectedScope);
        $this->view->assign('types', $types);
        $this->view->assign('selectedType', $selectedType);
    }

    /**
     * Delete a file
     * @throws \Exception
     * @throws \TYPO3\CMS\Core\Resource\Exception\FileOperationErrorException
     * @throws \TYPO3\CMS\Core\Resource\Exception\InsufficientFileAccessPermissionsException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function deleteFileAction()
    {
        $success = false;
        $selectedTheme = $this->validateSelectedTheme();
        if ($selectedTheme == '') {
            $translation = $this->translate('tx_ftm_exception.validating_theme_fails');
            throw new \Exception($translation);
        }
        /** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
        $storage = $this->storageService->getStorage($selectedTheme);
        if ($storage !== null) {
            // Rel-Path is valid!?
            $relPath = '';
            if ($this->request->hasArgument('relPath')) {
                $relPath = $this->request->getArgument('relPath');
            }
            // Name is valid!?
            $name = '';
            if ($this->request->hasArgument('name')) {
                $name = $this->request->getArgument('name');
            }
            // Get file and delete
            $file = $storage->getFile($relPath . $name);
            if (BackupService::backupFile(GeneralUtility::getFileAbsFileName($file->getPublicUrl()))) {
                $success = $storage->deleteFile($file);
            }
        }
        if ($success) {
            $translation = $this->translate('tx_ftm_message.ok_theme_file_successfully_deleted');
            $this->addFlashMessage($translation, '', AbstractMessage::OK);
        } else {
            $translation = $this->translate('tx_ftm_message.error_theme_file_deletion_fails');
            $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
        }
        $this->redirect('index', 'Overview');
    }

    /**
     * Writes a file
     *
     * @param        $path
     * @param        $file
     * @param string $content
     * @return bool
     * @throws \Exception
     */
    protected function writeFile($path, $file, $content = '', $createFolder = false)
    {
        $absPath = GeneralUtility::getFileAbsFileName($path);
        // Create folder when it's not available
        if (!file_exists($absPath) && $createFolder) {
            GeneralUtility::mkdir_deep($absPath);
        }

        if (file_exists($absPath)) {
            if (!file_exists($absPath . $file)) {
                return GeneralUtility::writeFile($absPath . $file, $content, true);
            } else {
                $translation = $this->translate('tx_ftm_message.warning_file_already_exists');
                $this->addFlashMessage($translation, '', AbstractMessage::WARNING);
            }
        } else {
            $translation = $this->translate('tx_ftm_message.error_directory_not_found');
            $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
            return false;
        }
        return false;
    }

    /**
     * Shows the create new Theme view
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function newThemeAction()
    {
        //
        $parentThemes = [];
        $parentThemes['none'] = $this->translate('tx_ftm_label.select_box_no_parent_theme');
        $cloneThemes = [];
        $cloneThemes['none'] = $this->translate('tx_ftm_label.select_box_no_clone');
        //
        // Get all available themes
        $availableThemes = $this->themeService->getAvailableThemes();
        /** @var \KayStrobach\Themes\Domain\Model\Theme $theme */
        foreach ($availableThemes as $theme) {
            $parentThemes[$theme->getExtensionName()] = $theme->getTitle() . ' (' . $theme->getExtensionName() . ')';
            $cloneThemes[$theme->getExtensionName()] = $theme->getTitle() . ' (' . $theme->getExtensionName() . ')';
        }
        //
        // Set theme_bootstrap4 as default (if available)
        $parentTheme = '';
        if (array_key_exists('theme_bootstrap4', $parentThemes)) {
            $parentTheme = 'theme_bootstrap4';
        }
        //
        // Check if parent theme is passed by parameter
        if ($this->request->hasArgument('parentTheme')) {
            $parentTheme = $this->request->getArgument('parentTheme');
        }
        $this->view->assign('parentTheme', $parentTheme);
        $this->view->assign('parentThemes', $parentThemes);
        $this->view->assign('cloneThemes', $cloneThemes);
    }

    /**
     * Generate a new theme
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function createThemeAction()
    {
        $extensionKey = '';
        if ($this->request->hasArgument('extensionKey')) {
            $extensionKey = $this->request->getArgument('extensionKey');
        }
        $title = '';
        if ($this->request->hasArgument('title')) {
            $title = $this->request->getArgument('title');
        }
        $parentTheme = '';
        if ($this->request->hasArgument('parentTheme')) {
            $parentTheme = $this->request->getArgument('parentTheme');
        }
        $cloneTheme = '';
        if ($this->request->hasArgument('cloneTheme')) {
            $cloneTheme = $this->request->getArgument('cloneTheme');
        }
        $installTheme = '';
        if ($this->request->hasArgument('installTheme')) {
            $installTheme = $this->request->getArgument('installTheme') == 'install' ? true : false;
        }
        //
        $redirectAction = 'newTheme';
        $redirectController = 'Theme';
        $redirectArguments = $this->request->getArguments();
        try {
            // When the Theme was successfully created
            if ($this->themeService->createNewTheme($extensionKey, $title, $parentTheme, $this->settings)) {
                /**
                 * @todo Clone files into the new Theme
                 */
                // Install the new Theme!?
                if ($installTheme) {
                    $this->themeService->installTheme($extensionKey);
                    $translation = $this->translate('tx_ftm_message.ok_theme_created_and_installed');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                    // Redirect actions
                    $redirectAction = 'index';
                    $redirectController = 'Overview';
                    $redirectArguments = ['theme' => $extensionKey];
                } else {
                    $translation = $this->translate('tx_ftm_message.ok_theme_created_and_not_installed');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                    // Redirect actions
                    $redirectAction = 'index';
                    $redirectController = 'Overview';
                    $redirectArguments = [];
                }
            } else {
                $translation = $this->translate('tx_ftm_message.error_theme_creation_failed');
                $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
            }
        } /** @var \Exception $exception */
        catch (\Exception $exception) {
            $this->addFlashMessage($exception->getMessage(), '', AbstractMessage::ERROR);
        }
        //
        // In case of errors
        // redirect back to new theme form
        // otherwise redirect to overview
        // If the theme-extensions was installed, select it directly
        $this->redirect($redirectAction, $redirectController, 'Ftm', $redirectArguments);
    }

    /**
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function saveSnippetAction()
    {

        $validParams = true;

        $selectedTheme = $this->validateSelectedTheme();
        if ($selectedTheme == '') {
            $translation = $this->translate('tx_ftm_exception.validating_theme_fails');
            throw new \Exception($translation);
        }
        $this->view->assign('selectedTheme', $selectedTheme);

        $relPath = '';
        if ($this->request->hasArgument('relPath')) {
            $relPath = $this->request->getArgument('relPath');
        }
        $this->view->assign('relPath', $relPath);

        $filename = '';
        if ($this->request->hasArgument('filename')) {
            $filename = $this->request->getArgument('filename');
        }
        $this->view->assign('filename', $filename);

        /** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
        $content = '';
        if ($this->request->hasArgument('content')) {
            $content = $this->request->getArgument('content');
        }
        $storage = $this->storageService->getStorage($selectedTheme);
        if ($storage !== null && $content == '') {

            // Get file content
            $file = $storage->getFile($relPath . $filename);
            $content = $file->getContents();
        }
        $this->view->assign('content', $content);

        $title = '';
        if ($this->request->hasArgument('title')) {
            $title = $this->request->getArgument('title');
        }
        $this->view->assign('title', $title);

        $type = '';
        if ($this->request->hasArgument('type')) {
            $type = $this->request->getArgument('type');
        }
        $this->view->assign('type', $type);

        $subType = '';
        if ($this->request->hasArgument('subType')) {
            $subType = $this->request->getArgument('subType');
        }
        $this->view->assign('subType', $subType);

        $scope = '';
        if ($this->request->hasArgument('scope')) {
            $scope = $this->request->getArgument('scope');
        }
        $this->view->assign('scope', GeneralUtility::underscoredToUpperCamelCase($scope));

        $description = '';
        if ($this->request->hasArgument('description')) {
            $description = $this->request->getArgument('description');
        }
        $this->view->assign('description', $description);

        $publicReadable = '';
        if ($this->request->hasArgument('publicReadable')) {
            $publicReadable = $this->request->getArgument('publicReadable');
        }
        $this->view->assign('publicReadable', $publicReadable);

        $publicWritable = '';
        if ($this->request->hasArgument('publicWritable')) {
            $publicWritable = $this->request->getArgument('publicWritable');
        }
        $this->view->assign('publicWritable', $publicWritable);


        if ($this->request->hasArgument('saveAndBack')) {
            if (trim($title) == '') {
                $validParams = false;
                $translation = $this->translate('tx_ftm_message.error_snippet_saving_failed_please_enter_a_title');
                $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
            }
            if (trim($description) == '') {
                $validParams = false;
                $translation = $this->translate('tx_ftm_message.error_snippet_saving_failed_please_enter_a_description');
                $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
            }

            if ($validParams) {
                if (Tools::checkValidFilename($filename) && trim($filename) != '') {
                    $snippetParams = [];
                    $snippetParams['type'] = $type;
                    $snippetParams['subType'] = $subType;
                    $snippetParams['title'] = $title;
                    $snippetParams['content'] = $content;
                    $snippetParams['description'] = $description;
                    $snippetParams['scope'] = $scope;
                    $snippetParams['filename'] = $filename;
                    $snippetParams['publicReadable'] = $publicReadable ? '1' : '0';
                    $snippetParams['publicWritable'] = $publicWritable ? '1' : '0';

                    $snippet = SnippetService::saveSnippet($snippetParams, $this->settings);
                    if ($snippet !== null) {
                        /**
                         * @todo: Translate Messages from Service?!
                         */
                        if ($snippet['status'] == 'success') {
                            $this->addFlashMessage($snippet['message'], '', AbstractMessage::OK);
                            $this->redirect('index', 'Overview');
                        } else {
                            $this->addFlashMessage($snippet['message'], '', AbstractMessage::ERROR);
                        }
                    } else {
                        $translation = $this->translate('tx_ftm_message.error_snippet_saving_failed');
                        $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                    }
                } else {
                    $translation = $this->translate('tx_ftm_message.error_please_enter_a_valid_file_name');
                    $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                }
            }
        }
        $this->view->assign('arguments', $this->request->getArguments());
    }

    /**
     * Open a file in editor
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function editFileAction()
    {
        $relPath = '';
        $name = '';
        $fileContent = '';
        // Is there a valid theme selected?!
        $selectedTheme = $this->validateSelectedTheme();
        if ($selectedTheme == '') {
            $translation = $this->translate('tx_ftm_exception.validating_theme_fails');
            throw new \Exception($translation);
        }
        /** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
        $storage = $this->storageService->getStorage($selectedTheme);
        if ($storage !== null) {
            //
            // Rel-Path is valid!?
            $relPath = '';
            if ($this->request->hasArgument('relPath')) {
                $relPath = $this->request->getArgument('relPath');
            }
            //
            // Name is valid!?
            $name = '';
            if ($this->request->hasArgument('name')) {
                $name = $this->request->getArgument('name');
            }
            //
            // Get file and its content
            /** @var File $file */
            $file = $storage->getFile($relPath . $name);
            $fileContent = $file->getContents();
            //
            // Set file path in top bar
            $metaInformation = [
                'combined_identifier' => $file->getCombinedIdentifier()
            ];
            $this->view->getModuleTemplate()->getDocHeaderComponent()->setMetaInformation($metaInformation);
        }

        //
        // Validate file extension
        $fileExtension = 'unknown';
        $scriptMode = 'ace/mode/text';
        $snippetFile = 'ace/snippets/text';
        $snippetScope = 'text';
        $snippets = '';

        if ($this->request->hasArgument('fileExtension')) {
            $fileExtension = strtolower($this->request->getArgument('fileExtension'));
        }


        switch ($fileExtension) {
            case 'js':
                $scriptMode = 'ace/mode/javascript';
                $snippetFile = 'ace/snippets/javascript';
                $snippetScope = 'javascript';
                break;
            case 'css':
                $scriptMode = 'ace/mode/css';
                $snippetFile = 'ace/snippets/css';
                $snippetScope = 'css';
                break;
            case 'less':
                $scriptMode = 'ace/mode/less';
                $snippetFile = 'ace/snippets/less';
                $snippetScope = 'less';
                $snippets = $this->settings['editor']['snippets']['less'];
                break;
            case 'scss':
                $scriptMode = 'ace/mode/scss';
                $snippetFile = 'ace/snippets/scss';
                $snippetScope = 'scss';
                $snippets = $this->settings['editor']['snippets']['scss'];
                break;
            case 'html':
                $scriptMode = 'ace/mode/twig';
                $snippetFile = 'ace/snippets/twig';
                $snippetScope = 'twig';
                $snippets = $this->settings['editor']['snippets']['fluid'];
                break;
        }
        /**
         * @todo Pass snippets to editor
         */
        $this->view->assign('wrapper', 'ace');
        $this->view->assign('fileContent', $fileContent);
        $this->view->assign('fileExtension', $fileExtension);
        $this->view->assign('scriptMode', $scriptMode);
        $this->view->assign('snippetFile', $snippetFile);
        $this->view->assign('snippetScope', $snippetScope);
        $this->view->assign('relPath', $relPath);
        $this->view->assign('fileName', $name);
        $this->view->assign('selectedTheme', $selectedTheme);
        $this->view->assign('settings', $this->settings);
    }

    /**
     * Save a file
     * @return string
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function saveFileAction()
    {
        $response = [];
        $selectedTheme = $this->validateSelectedTheme();
        if ($selectedTheme == '') {
            $translation = $this->translate('tx_ftm_exception.validating_theme_fails');
            throw new \Exception($translation);
        }
        /** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
        $storage = $this->storageService->getStorage($selectedTheme);
        if ($storage instanceof ResourceStorage) {
            //
            // Rel-Path is valid!?
            $relPath = '';
            if ($this->request->hasArgument('relPath')) {
                $relPath = $this->request->getArgument('relPath');
            }
            //
            // Name is valid!?
            $name = '';
            if ($this->request->hasArgument('name')) {
                $name = $this->request->getArgument('name');
            }
            //
            // Get file and save new content
            $file = $storage->getFile($relPath . $name);
            if ($file instanceof File) {
                if ($this->request->hasArgument('content')) {
                    if (BackupService::backupFile(GeneralUtility::getFileAbsFileName($file->getPublicUrl()))) {
                        $file->setContents($this->request->getArgument('content'));
                        $response['success'] = $file->getSize() . ' written!!';
                    } else {
                        $response['error'] = 'File backup failed, file not saved';
                    }
                }
            } else {
                $response['error'] = 'File not found';
            }
        } else {
            $response['error'] = 'Storage not found';
        }
        /**
         * @todo: add translations
         */
        return json_encode($response);
    }

    /**
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function rewriteThemeFilesAction() {
        $selectedTheme = $this->validateSelectedTheme();
        //
        // Refresh theme yaml object
        // Compare the hash of the theme.yaml->array, with hash within DB
        $themeYaml = $this->themeService->getMetaDataFromThemeYamlFile($selectedTheme);
        $changed = $this->themeService->loadThemeYaml($selectedTheme, $themeYaml, $this->settings);
        if ($changed == 'yaml-object-refreshed') {
            $translation = $this->translate('tx_ftm_message.info_yaml_object_refreshed');
            $this->addFlashMessage($translation, '', AbstractMessage::INFO);
        } else {
            if ($changed == 'yaml-file-refreshed') {
                $translation = $this->translate('tx_ftm_message.info_yaml_file_refreshed');
                $this->addFlashMessage($translation, '', AbstractMessage::INFO);
            }
        }
        //
        // Refresh ext_emconf.php, constants.txt and setup.txt
        $parentTheme = $themeYaml['parentTheme']['name'];
        $extensions = $themeYaml['supportedExtensions'];
        // But don't work on theme base packages
        if (!in_array($selectedTheme, $this->settings['readonlyThemes'])) {
            $filesSuccessful = $this->themeService->refreshThemeFiles($selectedTheme, $parentTheme, $extensions);
            if (!empty($filesSuccessful)) {
                $filesSuccessful = implode(', ', $filesSuccessful);
                $translation = $this->translate('tx_ftm_message.info_theme_typoscript_files_refreshed',
                    [$filesSuccessful]);
                $this->addFlashMessage($translation, '', AbstractMessage::INFO);
            }
        }
        $this->redirect('index', 'Overview', 'Ftm');
    }

}
