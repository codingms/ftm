<?php
namespace CodingMs\Ftm\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Utility\ArrayUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use \TYPO3\CMS\Extbase\Object\ObjectManager;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \CodingMs\Ftm\Service\ThemeService;
use \CodingMs\Ftm\Domain\Repository\ThemeRepository;

/**
 * Controller for handling and manage meta/theme.yaml
 *
 * @package ftm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class MetaController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * Theme service
	 *
	 * @var \CodingMs\Ftm\Service\ThemeService
	 * @inject
	 */
	protected $themeService;

	/**
	 * @var \CodingMs\Ftm\Domain\Repository\ThemeRepository
	 * @inject
	 */
	protected $themeYamlRepository;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;

	/**
	 * @var array Meta data of the YAML file
	 */
	protected $metaData = array();

	/**
	 * @var string Name of the selected theme
	 */
	protected $selectedTheme = '';

	/**
	 * @var boolean A theme selected!?
	 */
	protected $themeSelected = FALSE;

	/**
	 * Settings service
	 *
	 * @var \CodingMs\Ftm\Service\SettingsService
	 * @inject
	 */
	protected $settingsService;

    /**
     * Initialize actions
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
	public function initializeAction() {

		// Load and merge the settings
		$loadedSettings = $this->settingsService->getSettings();
		$this->settings = ArrayUtility::arrayMergeRecursiveOverrule($this->settings, $loadedSettings);

		// Validate the selected theme
		$this->validateSelectedTheme();

		// Read the meta data (theme.yaml)
		$this->metaData = $this->themeService->getMetaDataFromThemeYamlFile($this->selectedTheme);

		// Sync Theme.yaml
		$this->themeService->loadThemeYaml($this->selectedTheme, $this->metaData, $this->settings);
	}

    /**
     * Validates the selected theme
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
	protected function validateSelectedTheme() {
		// Get all available themes
		$this->availableThemes = $this->themeService->getAvailableThemes();
		// Check if a theme is passed by request param
		if($this->request->hasArgument('theme')) {
			$validateTheme = $this->request->getArgument('theme');
			if($this->themeService->isValidTheme($validateTheme)) {
				$this->selectedTheme = $validateTheme;
			}
		}
		// otherwise select the first available theme
		else {
			$this->selectedTheme = $this->themeService->getFirstAvailableTheme();
		}
		if($this->selectedTheme==='') {
			$translation = LocalizationUtility::translate('tx_ftm_exception.validating_theme_fails', 'Ftm');
			throw new \Exception($translation);
		}
		$this->themeSelected = true;
	}

    /**
     * @param string $command The command which has been sent to processDatamap
     * @param string $table The table we're dealing with
     * @param mixed $id Either the record UID or a string if a new record has been created
     * @param array $field The record row how it has been inserted into the database
     * @param \TYPO3\CMS\Core\DataHandling\DataHandler $reference A reference to the TCEmain instance
     * @return void
     *
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
	public function processDatamap_afterDatabaseOperations($command, $table, $id, &$fieldArray, \TYPO3\CMS\Core\DataHandling\DataHandler &$reference) {
		// Write the theme yaml object back
		// into the theme.yaml file
		if ($command == 'update' && $table == 'tx_ftm_domain_model_theme') {
			$this->prepareControllerForHookProcess();
            //
			// Get the theme yaml object
			/** @var \CodingMs\Ftm\Domain\Model\Theme $themeYaml */
			$themeYaml = $this->themeYamlRepository->findByIdentifier($id);
            //
			// Mark the file hash in order to rewrite the theme.yaml
			$themeYaml->setFileHash('write-object-back-into-yaml-file');
			$this->themeYamlRepository->update($themeYaml);
			$this->persistenceManager->persistAll();
		}
	}

    /**
     * Prepares the controller for executing by a hook
     */
	protected function prepareControllerForHookProcess() {
		// Create Objects in case of Hook executing
		if(!($this->objectManager instanceof ObjectManager)) {
			$this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
		}
		if(!($this->themeService instanceof ThemeService)) {
			$this->themeService = $this->objectManager->get(ThemeService::class);
		}
		if(!($this->themeYamlRepository instanceof ThemeRepository)) {
			$this->themeYamlRepository = $this->objectManager->get(ThemeRepository::class);
		}
		if(!($this->persistenceManager instanceof PersistenceManager)) {
			$this->persistenceManager = $this->objectManager->get(PersistenceManager::class);
		}
	}

    /**
     * Get a translation
     * @param $key
     * @param array $arguments
     * @return NULL|string
     */
    protected function translate($key, $arguments=[]) {
        return LocalizationUtility::translate($key, 'Ftm', $arguments);
    }

}
