<?php

namespace CodingMs\Ftm\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Ftm\Service\RealurlService;
use CodingMs\Ftm\Service\SettingsService;
use CodingMs\Ftm\Service\ThemeService;
use CodingMs\Ftm\Service\DocumentationService;
use CodingMs\Modules\Controller\BackendController;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use CodingMs\Ftm\Service\ExtensionService;
use CodingMs\Ftm\Service\BackupService;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Backend\View\BackendTemplateView;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

/**
 * Services controller
 *
 * @package ftm
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ServicesController extends BackendController
{

    /**
     * Settings service
     *
     * @var \CodingMs\Ftm\Service\SettingsService
     * @inject
     */
    protected $settingsService;

    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     * @throws \Exception
     */
    protected function initializeView(ViewInterface $view)
    {
        /** @var BackendTemplateView $view */
        parent::initializeView($view);
        if ($this->view->getModuleTemplate() !== null) {
            $this->createButtons();
        }
    }

    /**
     * Initialize any action
     */
    public function initializeAction()
    {
        // Load and merge the settings
        $loadedSettings = $this->settingsService->getSettings();
        $this->settings = array_replace_recursive($this->settings, $loadedSettings);
    }

    /**
     * Add menu buttons for specific actions
     *
     * @return void
     * @throws \Exception
     */
    protected function createButtons()
    {
        $moduleName = 'tools_FtmFtm';
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        $buttons = [];
        switch ($this->request->getControllerActionName()) {
            case 'index':
                // Back to theme overview
                $parameter = [
                    'id' => $this->pageUid,
                    'tx_ftm_' . strtolower($moduleName) => [
                        'action' => 'index',
                        'controller' => 'Overview'
                    ]
                ];
                $uriCancel = $this->uriBuilder->setArguments($parameter)->buildBackendUri();
                $buttons[] = $buttonBar->makeLinkButton()
                    ->setHref($uriCancel)
                    ->setTitle($this->translate('tx_ftm_label.button_back_to_overview'))
                    ->setIcon($this->getIcon('actions-close'));
                break;
        }
        foreach ($buttons as $button) {
            $buttonBar->addButton($button, ButtonBar::BUTTON_POSITION_LEFT);
        }
    }

    /**
     * Perform services actions
     * @throws \Exception
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    public function indexAction()
    {
        $service = '';
        if ($this->request->hasArgument('service')) {
            $service = $this->request->getArgument('service');
        }
        // Execute a service
        switch ($service) {
            case 'realurlClearCache':
                // Delete realurl cache
                if (RealurlService::clearCache()) {
                    $translation = $this->translate('tx_ftm_message.ok_clearing_realurl_cache_successful');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                } else {
                    $translation = $this->translate('tx_ftm_message.error_clearing_realurl_cache_failed');
                    $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                }
                break;
            case 'realurlDeleteAutoConfiguration':
                // Delete realurl auto configuration
                if (RealurlService::deleteAutoConfiguration()) {
                    $translation = $this->translate('tx_ftm_message.ok_delete_realurl_auto_configuration_successful');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                } else {
                    $translation = $this->translate('tx_ftm_message.error_delete_realurl_auto_configuration_failed');
                    $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                }
                break;
            case 'resetFtmSettings';
                // Reset FTM settings
                if (SettingsService::resetSettings()) {
                    $translation = $this->translate('tx_ftm_message.ok_reset_ftm_settings_successful');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                } else {
                    $translation = $this->translate('tx_ftm_message.error_reset_ftm_settings_failed');
                    $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                }
                break;
            case 'fixPermissions':
                // Fix file permissions on current selected theme
                if (ThemeService::fixPermissions($this->settings['selectedTheme'])) {
                    $translation = $this->translate('tx_ftm_message.ok_fix_file_permissions_successful');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                } else {
                    $translation = $this->translate('tx_ftm_message.error_fix_file_permissions_failed');
                    $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                }
                break;
            case 'generateDocumentation':
                // Generate documentation for current theme
                if (DocumentationService::generateDocumentation($this->settings['selectedTheme'])) {
                    $translation = $this->translate('tx_ftm_message.ok_fix_file_permissions_successful');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                } else {
                    $translation = $this->translate('tx_ftm_message.error_fix_file_permissions_failed');
                    $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                }
                break;
            case 'extensionsDeleteRepositoryCache':
                // Deletes all extensions in extension repository
                if (ExtensionService::deleteRepositoryCache()) {
                    $translation = $this->translate('tx_ftm_message.ok_delete_extension_repository_cache_successful');
                    $this->addFlashMessage($translation, '', AbstractMessage::OK);
                } else {
                    $translation = $this->translate('tx_ftm_message.error_delete_extension_repository_cache_failed');
                    $this->addFlashMessage($translation, '', AbstractMessage::ERROR);
                }
                break;
            case 'systemBackupFullDatabase':
                // Backup the whole database
                BackupService::backupFullDatabase();
                break;
        }
        /**
         * @todo Implement a service which show the size of backupped file. Additionally a service which clears all
         * backupped files.
         */
        //
        // Set the settings!
        // Required because of adjusting values!
        $this->view->assign('settings', $this->settings);
    }

}
