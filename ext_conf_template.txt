# cat=basic; type=string; label=LLL:EXT:ftm/Resources/Private/Language/locallang.ext_conf_template.xlf:tx_ftm_extconftemplate.plugin_cloud_host
pluginCloudHost = http://api.fluid-template-manager.de

# cat=basic; type=boolean; label=LLL:EXT:ftm/Resources/Private/Language/locallang.ext_conf_template.xlf:tx_ftm_extconftemplate.allow_log
allowLog = 0

# cat=basic; type=boolean; label=LLL:EXT:ftm/Resources/Private/Language/locallang.ext_conf_template.xlf:tx_ftm_extconftemplate.disable_backend_module
disableBackendModule = 0
