<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'FTM',
	'description' => 'FTM (Documentation: http://www.fluid-template-manager.de/) - More information in ChangeLog.md',
	'category' => 'Backend Modules',
	'author' => 'Thomas Deuling',
	'author_email' => 'typo3@coding.ms',
	'author_company' => 'coding.ms',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'version' => '4.1.0.DEV',
	'constraints' => array(
		'depends' => array(
			'typo3' => '8.7.0-8.7.99',
			'themes' => '8.7.0-8.7.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
			'developer' => '0.1.0-9.99.99',
		),
	),
);
