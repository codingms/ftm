<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

if (TYPO3_MODE == 'BE') {

	// Register hooks

	// $TYPO3_CONF_VARS['SC_OPTIONS']['typo3/file_edit.php']['preOutputProcessingHook'][]
	// = 'CodingMs\\FWtm\\Hook\\FileEditHook->preOutputProcessingHook';
	$TYPO3_CONF_VARS['SC_OPTIONS']['typo3/file_edit.php']['postOutputProcessingHook'][]
		= 'CodingMs\\Ftm\\Hook\\FileEditHook->postOutputProcessingHook';

	// Write theme.yaml when Theme-Object was saved
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][]
		= 'EXT:ftm/Classes/Controller/MetaController.php:CodingMs\Ftm\Controller\MetaController';

	// Always integrate Theme colored css
	//$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/template.php']['preHeaderRenderHook'][]
	//	= 'EXT:ftm/Classes/Hook/PreHeaderRenderHook.php:Tx_DyncssTest_Be_PreHeaderRenderHook->main';

	// Unroll save buttons
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Template\Components\Buttons\SplitButton::class] = [
		'className' => \CodingMs\Ftm\Xclass\UnrolledSplitButton::class
	];
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/tsconfig.txt">');

// Make the extension version number available to the extension scripts
require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'ext_emconf.php';
$TYPO3_CONF_VARS['EXTCONF'][$_EXTKEY]['version'] = $EM_CONF[$_EXTKEY]['version'];

if(!class_exists('\Ifsnop\Mysqldump\Mysqldump')) {
	include_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ftm') . '/Resources/Private/Php/Mysqldump/Mysqldump.php');
}
