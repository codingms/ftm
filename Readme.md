## Fluid-Template-Manager

### Backend-Module

* Generating Themes based on the TYPO3-Themes project
* 

### Plugins

* Plugin for Markdown rendering and displaying

### View-Helpers

* RenderExternal ViewHelper allows you to render Templates and Partials from another extension.
* FlashMessages ViewHelper displays Flash-Messages in Bootstrap style